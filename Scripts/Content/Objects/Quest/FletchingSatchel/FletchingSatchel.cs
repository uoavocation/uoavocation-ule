using System;
using System.Collections.Generic;
using System.Text;
using Server;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines.MLQuests.Items
{
    public class FletchingSatchel : BaseCraftmansSatchel
    {
        [Constructable]
        public FletchingSatchel()
        {
            AddBaseLoot(Loot.MLRangedWeaponTypes, Loot.JewelryTypes, m_TalismanType);

            if (Utility.RandomDouble() < 0.50)
                AddRecipe(DefBowFletching.CraftSystem);

            // TODO: runic fletching kit
        }

        public FletchingSatchel(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}