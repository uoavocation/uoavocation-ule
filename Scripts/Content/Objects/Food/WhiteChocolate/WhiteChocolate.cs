using System;
using Server;

namespace Server.Items
{
    public class WhiteChocolate : CandyCane
    {
        public override int LabelNumber { get { return 1079996; } } // White chocolate
        public override double DefaultWeight { get { return 1.0; } }

        [Constructable]
        public WhiteChocolate()
            : base(0xF11)
        {
            Hue = 0x47E;
            LootType = LootType.Regular;
        }

        public WhiteChocolate(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}
