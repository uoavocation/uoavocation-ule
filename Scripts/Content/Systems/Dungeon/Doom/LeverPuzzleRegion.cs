using System;
using Server;
using Server.Mobiles;
using Server.Regions;

namespace Server.Engines.Doom
{
    public class LeverPuzzleRegion : BaseRegion
    {
        private LeverPuzzleController Controller;
        public Mobile m_Occupant;

        [CommandProperty(AccessLevel.GameMaster)]
        public Mobile Occupant
        {
            get
            {
                if (m_Occupant != null && m_Occupant.Alive)
                    return m_Occupant;
                return null;
            }
        }

        public LeverPuzzleRegion(LeverPuzzleController controller, int[] loc)
            : base(null, Map.Malas, Region.Find(LeverPuzzleController.lr_Enter, Map.Malas), new Rectangle2D(loc[0], loc[1], 1, 1))
        {
            Controller = controller;
            Register();
        }

        public override void OnEnter(Mobile m)
        {
            if (m != null && m_Occupant == null && m is PlayerMobile && m.Alive)
                m_Occupant = m;
        }

        public override void OnExit(Mobile m)
        {
            if (m != null && m == m_Occupant)
                m_Occupant = null;
        }
    }
}