using System;
using Server;

namespace Server.Items
{
	public class FlyingShadow : Item
	{
		[Constructable]
		public FlyingShadow( int itemID, double Spd ) : base( itemID )
		{
			Movable = false;

			Hue = 962;

			double Rate = Spd;

			new InternalTimer( this, Rate ).Start();
		}

		public FlyingShadow( Serial serial ) : base( serial )
		{
			new InternalTimer( this, 0.0 ).Start();
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		private class InternalTimer : Timer
		{
			private Item m_FlyingShadow;
			private double Rate;

			public InternalTimer( Item FlyingShadow, double rate ) : base( TimeSpan.FromSeconds( rate ) )
			{
				m_FlyingShadow = FlyingShadow;
				Rate = rate;
			}

			protected override void OnTick()
			{
				m_FlyingShadow.Delete();
			}
		}
	}
}