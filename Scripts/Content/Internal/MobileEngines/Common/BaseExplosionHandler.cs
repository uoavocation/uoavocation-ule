using System;
using System.Collections;
using System.IO;
using Server;
using Server.Items;
using Server.Mobiles;

namespace Server
{
	public class FlameWrath
	{

		public static void DoRandomWrath( Mobile source )
		{
			if ( source.Deleted )
				return;

			int randomrange = Utility.RandomMinMax(2, 7);
			int mindmg = Utility.Random(10);
			int maxdmg = mindmg += Utility.Random(10);

			DoWrath(source, mindmg, maxdmg, randomrange);
		}

		public static void DoWrath( Mobile source, int mindmg, int maxdmg )
		{
			if ( source.Deleted )
				return;

			int randomrange = Utility.RandomMinMax(1, 5);
			
			DoWrath(source, mindmg, maxdmg, randomrange);
		}

		public static void DoWrath( Mobile source, int mindmg, int maxdmg, int range )
		{
			if ( source.Deleted )
				return;

			Map map = source.Map;

			if ( map != null )
			{
				for ( int x = -range; x <= range; ++x )
				{
					for ( int y = -range; y <= range; ++y )
					{
						double dist = Math.Sqrt(x*x+y*y);

						if ( dist <= range )
						new WrathTimer(map, source.X + x, source.Y + y, mindmg, maxdmg, source ).Start();						
					}
				}
			}
		}
	}

	public class WrathTimer : Timer
	{
		private Map n_Map;
		private int n_X, n_Y;
		private int n_MinDamage;
		private int n_MaxDamage;
		private Mobile n_SourceMobile;

		public WrathTimer( Map map, int x, int y, int mindamage, int maxdamage, Mobile sourcemobile ) : base( TimeSpan.FromSeconds( Utility.RandomDouble() * 10.0 ) ) 
		{
			n_Map = map;
			n_X = x;
			n_Y = y;
			n_MinDamage = mindamage;
			n_MaxDamage = maxdamage;
			n_SourceMobile = sourcemobile;
		}

		protected override void OnTick()
		{
			int z = n_Map.GetAverageZ( n_X, n_Y );
			bool canFit = n_Map.CanFit( n_X, n_Y, z, 6, false, false );

			for ( int i = -3; !canFit && i <= 3; ++i )
			{
				canFit = n_Map.CanFit( n_X, n_Y, z + i, 6, false, false );

				if ( canFit )
					z += i;
			}

			if ( !canFit )
				return;

			Gold g = new Gold( 1 );
				
			g.MoveToWorld( new Point3D( n_X, n_Y, z ), n_Map );
			
			switch ( Utility.Random( 3 ) )
			{
				case 0: // Fire column
				{
					if (g.Deleted || g == null)
						break;

					Effects.SendLocationParticles( EffectItem.Create( g.Location, g.Map, EffectItem.DefaultDuration ), 0x3709, 10, 30, 5052 );
					Effects.PlaySound( g, g.Map, 0x208 );
					DoDamage(g, n_MinDamage, n_MaxDamage);

					break;
				}
				case 1: // Explosion
				{
					if (g.Deleted || g == null)
						break;

					Effects.SendLocationParticles( EffectItem.Create( g.Location, g.Map, EffectItem.DefaultDuration ), 0x36BD, 20, 10, 5044 );
					Effects.PlaySound( g, g.Map, 0x307 );
					DoDamage(g, n_MinDamage, n_MaxDamage);

					break;
				}
				case 2: // Ball of fire
				{
					if (g.Deleted || g == null)
						break;

					Effects.SendLocationParticles( EffectItem.Create( g.Location, g.Map, EffectItem.DefaultDuration ), 0x36FE, 10, 10, 5052 );
					DoDamage(g, n_MinDamage, n_MaxDamage);

					break;
				}
			}
		}

		public virtual void DoDamage(Item g, int mindmg, int maxdmg)
		{
			if (g.Deleted || g == null)
				return;

			foreach ( Mobile m in g.GetMobilesInRange( 2 ) )
			{
				if ( m != null && !m.Deleted && m.AccessLevel == AccessLevel.Player && m is PlayerMobile && m.Alive || m is BaseCreature && (((BaseCreature)m).Controlled || ((BaseCreature)m).Summoned ) )
				{
					m.DoHarmful( m );

					m.FixedParticles( 0x36BD, 50, 50, 5052, EffectLayer.Waist );

					int toStrike = Utility.RandomMinMax( mindmg, maxdmg );

					m.Damage( toStrike, m );

					if( Utility.RandomDouble() < 0.20)
						m.SendMessage( "You are caught in the powerful blast!" );
				}
			}
			g.Delete();
		}
	}
}