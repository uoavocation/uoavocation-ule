using System;
using System.Collections.Generic;
using System.Text;
using Server;
using Server.Items;

namespace Server.Engines.MLQuests.Items
{
    public class SmallBagOfTrinkets : Bag
    {
        [Constructable]
        public SmallBagOfTrinkets()
        {
            RewardBag.Fill(this, 1, 0.0);
        }

        public SmallBagOfTrinkets(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}