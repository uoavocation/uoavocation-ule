using Server;
using System;
using Server.Misc;
using Server.Mobiles;

namespace Server.Items
{
    public class HonorableSwords : Item
    {
        private static string[] m_Names = new string[]
			{
				"Akira",
				"Avaniaga",
				"Aya",
				"Chie",
				"Emiko",
				"Fumiyo",
				"Gennai",
				"Gennosuke", 
				"Genjo",
				"Hamato",
				"Harumi",
				"Ikuyo",
				"Juri",
				"Kaori",
				"Kaoru",
				"Kiyomori",
				"Mayako",
				"Motoki",
				"Musashi",
				"Nami",
				"Nobukazu",
                "Otim Pyre",
				"Roku",
				"Romi",
				"Ryo",
				"Sanzo",
				"Sakamae",
				"Satoshi",
				"Takamori",
				"Takuro",
                "Ted",
				"Teruyo",
				"Toshiro",
                "Xavian",
				"Yago",
				"Yeijiro",
				"Yoshi",
				"Zeshin"
			};

        public static string[] Names { get { return m_Names; } }

        private string m_SwordsName;

        [CommandProperty(AccessLevel.GameMaster)]
        public string SwordsName
        {
            get { return m_SwordsName; }
            set { m_SwordsName = value; }
        }

        public override int LabelNumber { get { return 1071015; } } // Honorable Swords

        [Constructable]
        public HonorableSwords(string swordsName)
            : base(0x2853)
        {
            m_SwordsName = swordsName;

            Weight = 5.0;
        }

        [Constructable]
        public HonorableSwords()
            : this(AncientUrn.Names[Utility.Random(AncientUrn.Names.Length)])
        {
        }

        public HonorableSwords(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0);
            writer.Write(m_SwordsName);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
            m_SwordsName = reader.ReadString();

            Utility.Intern(ref m_SwordsName);
        }

        public override void AddNameProperty(ObjectPropertyList list)
        {
            list.Add(1070936, m_SwordsName); // Honorable Swords of ~1_name~
        }

        public override void OnSingleClick(Mobile from)
        {
            LabelTo(from, 1070936, m_SwordsName); // Honorable Swords of ~1_name~
        }
    }
}