using System;
using System.Collections;
using System.Collections.Generic;
using Server;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
    public class MerchandiseCrate : Item
    {
        private DateTime m_DecayTime;
        private Timer m_Timer;
        private Type m_GoodsType;
        private int m_Quantity;
        private int m_Price;
        private GoodsChestItem m_Manifest;
        private Item m_Goods;

        private void SetChestAppearance()
        {
            bool UseFirstItemId = Utility.RandomBool();

            switch (Utility.RandomList(0, 1, 2))
            {
                case 0:// Large Crate
                    this.ItemID = (UseFirstItemId ? 0xe3c : 0xe3d);
                    break;

                case 1:// Medium Crate
                    this.ItemID = (UseFirstItemId ? 0xe3e : 0xe3f);
                    break;

                case 2:// Small Crate
                    this.ItemID = (UseFirstItemId ? 0x9a9 : 0xe7e);
                    break;
            }
        }

        private void SelectManifest()
        {
            m_Manifest = GoodsItems[Utility.Random(GoodsItems.Length)];
            m_Quantity = m_Manifest.BaseQuantity * Utility.Random(1,10);
            double m_Sell = ((double)m_Manifest.ItemValue * (double)m_Quantity) * 0.8 ;
            m_GoodsType = m_Manifest.Type;
            m_Price = (int)m_Sell;
        }

        [Constructable]
        public MerchandiseCrate()
            : base(0xE41)
        {
            this.SetChestAppearance();
            this.SelectManifest();
            Movable = false;
            m_DecayTime = DateTime.Now + TimeSpan.FromMinutes(Utility.Random(10, 15));
                m_Timer = new InternalTimer(this, m_DecayTime);
                m_Timer.Start();
        }

        public override void GetProperties(ObjectPropertyList list)
        {
            base.GetProperties(list);
            if (m_Quantity != 0)
            {
                list.Add("Merchant goods {0} {1} ", m_Quantity, m_Manifest.Type.Name);
                list.Add("Price {0}gp", m_Price);
            }
            else
            {
                list.Add("Sold Out");
            }

        }

        private class InternalTimer : Timer
        {
            private Item m_Item;

            public InternalTimer(Item item, DateTime end)
                : base(end - DateTime.Now)
            {
                m_Item = item;
            }

            protected override void OnTick()
            {
                m_Item.Delete();
            }
        }

        
        private static readonly GoodsChestItem[] GoodsItems = new GoodsChestItem[]
			{
				//Boards
                new GoodsChestItem( typeof( Board ),3,500 ),
				new GoodsChestItem( typeof( OakBoard ),4,500 ),
				new GoodsChestItem( typeof( AshBoard ),5,500 ),
				new GoodsChestItem( typeof( YewBoard ),8,500 ),
				new GoodsChestItem( typeof( HeartwoodBoard ),12,500 ),
				new GoodsChestItem( typeof( BloodwoodBoard ),20,500 ),
				new GoodsChestItem( typeof( FrostwoodBoard ),45,500 ),
                //Reagents
                new GoodsChestItem( typeof( SulfurousAsh ),3,500 ),
				new GoodsChestItem( typeof( Bloodmoss ),3,500 ),
				new GoodsChestItem( typeof( Garlic ),3,500 ),
				new GoodsChestItem( typeof( Ginseng ),3,500 ),
				new GoodsChestItem( typeof( Nightshade ),3,500 ),
				new GoodsChestItem( typeof( BlackPearl ),5,500 ),
				new GoodsChestItem( typeof( SpidersSilk ),3,500 ),
				new GoodsChestItem( typeof( MandrakeRoot ),5,500 ),
                //Necro Reagents
				new GoodsChestItem( typeof( PigIron ),5,500 ),
				new GoodsChestItem( typeof( GraveDust ),3,500 ),
				new GoodsChestItem( typeof( NoxCrystal ),6,500 ),
				new GoodsChestItem( typeof( DaemonBlood ),6,500 ),
				new GoodsChestItem( typeof( BatWing ),3,500 ),
                //Archery
				new GoodsChestItem( typeof( Feather ),1,500 ),
				new GoodsChestItem( typeof( Bolt ),5,500 ),
				new GoodsChestItem( typeof( Arrow ),2,500 ),
                //Ingots
				new GoodsChestItem( typeof( IronIngot ),3,500 ),
				new GoodsChestItem( typeof( DullCopperIngot ),4,500 ),
				new GoodsChestItem( typeof( ShadowIronIngot ),5,500 ),
				new GoodsChestItem( typeof( CopperIngot ),7,500 ),
				new GoodsChestItem( typeof( BronzeIngot ),9,500 ),
				new GoodsChestItem( typeof( GoldIngot ),12,500 ),
				new GoodsChestItem( typeof( AgapiteIngot ),16,300 ),
				new GoodsChestItem( typeof( VeriteIngot ),25,300 ),
				new GoodsChestItem( typeof( ValoriteIngot ),35,300 ),
                //Scales
				new GoodsChestItem( typeof( RedScales ),20,100 ),
				new GoodsChestItem( typeof( YellowScales ),20,100 ),
				new GoodsChestItem( typeof( BlackScales ),20,100 ),
				new GoodsChestItem( typeof( GreenScales ),20,100 ),
				new GoodsChestItem( typeof( WhiteScales ),20,100 ),
				new GoodsChestItem( typeof( BlueScales ),20,100 ),
                //Tailoring
				new GoodsChestItem( typeof( BoltOfCloth),100,50 ),
				new GoodsChestItem( typeof( Leather ),5,500 ),
				new GoodsChestItem( typeof( SpinedLeather ),10,500 ),
				new GoodsChestItem( typeof( BarbedLeather ),20,300 ),
				new GoodsChestItem( typeof( HornedLeather ),40,200 ),
			};

        public override void OnDoubleClick(Mobile from)
        {
            BankCheck m_Check = null;
            Container pack = from.Backpack;
            if (m_Quantity !=0 )
            {
                bool m_hascheck = false;
                Item[] items = pack.FindItemsByType(typeof(BankCheck));

                for (int i = 0; i < items.Length; ++i)
                {
                    if (items.Length != 0)
                    {
                        if (((BankCheck)items[i]).Worth == m_Price)
                        {
                            m_Check = (BankCheck)items[i];
                            m_hascheck = true;
                            break;
                        }
                    }

                }

            if (pack != null && m_hascheck)
            {
                Type m_type = m_Manifest.Type;
                m_Goods = (Item)Activator.CreateInstance(m_Manifest.Type);
                m_Goods.Amount = m_Quantity;
                Item m_deed = new CommodityDeed(m_Goods);
                from.AddToBackpack(m_deed);
                m_Quantity = 0;
                m_Price = 0;
                InvalidateProperties();
                m_Check.Delete();
            }
            else if (pack != null && pack.ConsumeTotal(typeof(Gold), m_Price))
            {
                Type m_type = m_Manifest.Type;
                m_Goods = (Item)Activator.CreateInstance(m_Manifest.Type);
                m_Goods.Amount = m_Quantity;
                Item m_deed = new CommodityDeed(m_Goods);
                from.AddToBackpack(m_deed);
                m_Quantity = 0;
                m_Price = 0;
                InvalidateProperties();
                
                }
            else
            {
                from.SendMessage(0x22, "Sorry, the merchant only deals in gold or exact bankchecks upfront, go get the {0} gold or check from your bank to purchase the {1} {2}", m_Price, m_Quantity, m_Manifest.Type.Name);
            }
            }
        }

        public MerchandiseCrate(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)1); // version
            writer.WriteDeltaTime(m_DecayTime);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
            switch (version)
            {
                case 1:
                    {

                            m_DecayTime = reader.ReadDeltaTime();

                            m_Timer = new InternalTimer(this, m_DecayTime);
                            m_Timer.Start();
                            break;
                    }
            }

        }
    }
    public class GoodsChestItem
    {
        private Type m_Type;
        private int m_ItemValue;
        private int m_Qty;

        public Type Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }

        public int ItemValue
        {
            get { return m_ItemValue; }
            set { m_ItemValue = value; }
        }

        public int BaseQuantity
        {
            get { return m_Qty; }
            set { m_Qty = value; }
        }
        public GoodsChestItem(Type type, int itemvalue, int basequantity)
        {
            m_Type = type;
            m_ItemValue = itemvalue;
            m_Qty = basequantity;
        }
    }
}