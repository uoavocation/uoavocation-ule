using System;
using System.Collections;
using System.Collections.Generic;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;

namespace Server.Mobiles
{
    [CorpseName("a gatekeeper's corpse")]
    public class TheGateKeeper : BaseHealer
    {
        public override bool ClickTitle { get { return false; } }

        [Constructable]
        public TheGateKeeper()
        {
            Name = "a gate keeper";
            Body = ((this.Female = Utility.RandomBool()) ? Body = 0x191 : Body = 0x190);

            Item hair = new Item(Utility.RandomList(0x203B, 0x2049, 0x2048, 0x204A));
            hair.Hue = Utility.RandomNondyedHue();
            hair.Layer = Layer.Hair;
            hair.Movable = false;
            AddItem(hair);

            if (Utility.RandomBool() && !this.Female)
            {
                Item beard = new Item(Utility.RandomList(0x203E, 0x203F, 0x2040, 0x2041, 0x204B, 0x204C, 0x204D));

                beard.Hue = hair.Hue;
                beard.Layer = Layer.FacialHair;
                beard.Movable = false;

                AddItem(beard);
            }

            Hue = Utility.RandomSkinHue();

            SpeechHue = Utility.RandomDyedHue();

            SetStr(326, 375);
            SetDex(31, 45);
            SetInt(101, 110);

            SetHits(301, 400);
            SetMana(101, 110);

            SetDamage(17, 25);

            SetDamageType(ResistanceType.Physical, 90);

            SetResistance(ResistanceType.Physical, 55, 75);
            SetResistance(ResistanceType.Fire, 40, 60);
            SetResistance(ResistanceType.Cold, 35, 55);
            SetResistance(ResistanceType.Poison, 50, 70);
            SetResistance(ResistanceType.Energy, 55, 75);
         
            SetSkill(SkillName.Wrestling, 92.6, 107.5);
            SetSkill(SkillName.Forensics, 80.0, 100.0 );
			SetSkill(SkillName.SpiritSpeak, 80.0, 100.0 );
            SetSkill(SkillName.Anatomy, 110.1, 125.0);
            SetSkill(SkillName.MagicResist, 42.6, 57.5);

            Fame = 8500;
            Karma = -8500;
        }   

        #region Loot Players Can Take From Pack

        public override void OnDeath( Container c )
 		{
			base.OnDeath( c );
	 		c.DropItem( new MonksRobe() );
 		}

        public override void GenerateLoot()
        {
            AddLoot(LootPack.FilthyRich);
            AddLoot(LootPack.Rich);
            AddLoot(LootPack.Gems, 2);
        }

        #endregion

        #region Drop An Item And Get Teleported

        public override bool OnDragDrop(Mobile from, Item dropped)
        {
            if (dropped is MonksRobe)
            {
                Point3D loc = new Point3D(6001, 1505, 0);
                Map map = Map.Trammel;

                Effects.SendLocationParticles(EffectItem.Create(loc, map, EffectItem.DefaultDuration), 0x3728, 10, 10, 0, 0, 2023, 0);
                Effects.PlaySound(loc, map, 0x1FE);

                BaseCreature.TeleportPets(from, loc, map);

                from.MoveToWorld(loc, map);
            
                dropped.Delete();
                return true;
            }
            else 
            {
                Say(String.Format("I appreciate the offer, but your passage cannot be secured with that!", from.Name));            
                return false;
            }
        }

        #endregion

        #region Will Ressurect All Dead Players

        public bool CheckResurrect(Mobile m)
        {
            if (m.Criminal)
            {
                Say(String.Format("Thou art a criminal, but I will grant you a reprieve!", m.Name));
                return true;
            }
            else if (m.Kills >= 5)
            {
                Say(String.Format("Thou art murderous scum, you shall get no mercy here!", m.Name));
                return false;
            }
            else if (m.Karma < 0)
            {
                Say(String.Format("Thou art troubled?! Allow me to save yer soul my son!", m.Name));
            }
            return true;
        }

        #endregion

        public TheGateKeeper(Serial serial): base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }
        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}
