using System;
using Server.Network;

namespace Server.Items
{
    public class GreenBeaker : Item
    {
        [Constructable]
        public GreenBeaker()
            : base(0x1831)
        {
            Weight = 1.0;
            Movable = true;
        }

        public GreenBeaker(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}