using System;
using System.Collections;
using Server.Items;
using Server.Targeting;
using Server.Misc;

namespace Server.Mobiles
{
    [CorpseName("a gorilla corpse")]
    public class SickGorilla : BaseCreature
    {
        private DateTime NextAbilityTime;
        private const int maxDist = 10;

        [Constructable]
        public SickGorilla()
            : base(AIType.AI_Animal, FightMode.Aggressor, 10, 1, 0.2, 0.4)
        {
            Name = "a gorilla";
            Body = 0x1D;
            BaseSoundID = 0x9E;

            SetStr(53, 95);
            SetDex(36, 55);
            SetInt(36, 60);

            SetHits(38, 51);
            SetMana(0);

            SetDamage(4, 10);

            SetDamageType(ResistanceType.Physical, 100);

            SetResistance(ResistanceType.Physical, 20, 25);
            SetResistance(ResistanceType.Fire, 5, 10);
            SetResistance(ResistanceType.Cold, 10, 15);

            SetSkill(SkillName.MagicResist, 45.1, 60.0);
            SetSkill(SkillName.Tactics, 43.3, 58.0);
            SetSkill(SkillName.Wrestling, 43.3, 58.0);

            Fame = 450;
            Karma = 0;

            VirtualArmor = 20;

            Tamable = true;
            ControlSlots = 1;
            MinTameSkill = -18.9;
        }

        public override int Meat { get { return 1; } }
        public override int Hides { get { return 6; } }
        public override FoodType FavoriteFood { get { return FoodType.FruitsAndVegies | FoodType.GrainsAndHay; } }
        public override bool CanRummageCorpses { get { return true; } }

        public SickGorilla(Serial serial)
            : base(serial)
        {
        }

        public void GetTarget()
        {
            if (Combatant != null) return;

            foreach (Mobile m in this.GetMobilesInRange(maxDist))
            {
                if (m.Player && !m.Deleted && m.Alive && !m.Hidden && m.AccessLevel == AccessLevel.Player)
                {
                    if (m.FindItemOnLayer(Layer.Helm) is OrcishKinMask)
                        return;

                    Combatant = m;
                    FocusMob = m;

                    return;
                }
            }
        }

        public bool UseDung()
        {
            if (Combatant == null) return false;

            Mobile to = (Mobile)Combatant;

            if (!to.Mounted)
            {
                return false;
            }

            if (Core.AOS)
            {
                new Dung().MoveToWorld(to.Location, to.Map);

                if (0.03 > Utility.RandomDouble())
                {
                    Dung temp = new Dung();

                    temp.Location = to.Location;
                    temp.Map = to.Map;
                }
            }

            to.Damage(1, this);

            IMount mt = to.Mount;

            if (mt != null) mt.Rider = null;

            to.BeginAction(typeof(BaseMount));
            to.PlaySound(0x145);
            //to.Animate( 9, 1, 1, true, false, 0 );
            to.SendMessage("The gorilla has just hit you with a steamy wet pile of dung!");
            //to.Animate( 0x36E4, 7, 0, false, true, 0x44, 0 ); 
            //to.SendLocalizedMessage( 1040023 ); // You have been knocked off of your mount!
            to.EndAction(typeof(BaseMount));
            //Effects.SendMovingEffect( to, Rider, 0x36E4, 7, 0, false, true, 0x44, 0 ); 
            return true;
        }

        public override void OnThink()
        {
            GetTarget();

            if (DateTime.Now >= NextAbilityTime)
            {
                if (!UseDung()) return;

                NextAbilityTime = DateTime.Now + TimeSpan.FromSeconds(Utility.RandomMinMax(22, 25));
            }
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}