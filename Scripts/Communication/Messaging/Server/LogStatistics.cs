using System;
using Server.Network;
using Server.Gumps;

namespace Server.Misc
{
    public class LoginStats
    {
        public static void Initialize()
        {
            // Register our event handler
            EventSink.Login += new LoginEventHandler(EventSink_Login);
        }

        private static void EventSink_Login(LoginEventArgs args)
        {
            int userCount = NetState.Instances.Count;
            int itemCount = World.Items.Count;
            int mobileCount = World.Mobiles.Count;

            Mobile m = args.Mobile;

            m.SendMessage("Welcome, {0}! There {1} currently {2} user{3} online, with {4} item{5} and {6} mobile{7} in the world.",
                args.Mobile.Name,
                userCount == 1 ? "is" : "are",
                userCount, userCount == 1 ? "" : "s",
                itemCount, itemCount == 1 ? "" : "s",
                mobileCount, mobileCount == 1 ? "" : "s");

            World.Broadcast(0x35, true, "{0} has connected to the server. There {1} now currently {2} user{3} online.", args.Mobile.Name,
                userCount == 1 ? "is" : "are",
                userCount, userCount == 1 ? "" : "s");

            #region UOAvocation - Unique Name System [01-01]

            if (m.Name == CharacterCreation.GENERIC_NAME || !CharacterCreation.CheckDupe(m, m.Name))
            {
                m.CantWalk = true;
                m.SendGump(new NameChangeGump(m));
            }

            #endregion Edited By: A.A.S.R
        }
    }

    public class LogoutStats
    {
        public static void Initialize()
        {
            // Register event handler 
            EventSink.Disconnected += new DisconnectedEventHandler(EventSink_Disconnected);
        }
        private static void EventSink_Disconnected(DisconnectedEventArgs args)
        {
            Mobile m = args.Mobile;
            int userCount = NetState.Instances.Count;
            {
                World.Broadcast(0x35, true, "{0} has disconnected from the server. There {1} now currently {2} user{3} online.", args.Mobile.Name,
                userCount - 1 == 1 ? "is" : "are",
                userCount - 1, userCount == 1 ? "" : "s");
            }
        }
    }
}







