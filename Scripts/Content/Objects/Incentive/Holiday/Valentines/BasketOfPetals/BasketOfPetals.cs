using System; 
using Server; 

namespace Server.Items 
{ 

   public class BasketOfPetals : Item 
   { 

      [Constructable] 
      public BasketOfPetals() 
      { 

         Weight= 1.0;
	 Name = "a basket of rose petals";
	 ItemID = 2476;
       
       
      }

	public override void OnDoubleClick( Mobile from )
	{

        	RosePetals rosepetals = new RosePetals();
        	rosepetals.Location = from.Location;
        	rosepetals.Map = from.Map;
        	World.AddItem( rosepetals );
               	from.SendMessage( "You drop some rose petals on the floor." );

	}

      public BasketOfPetals( Serial serial ) : base( serial ) 
      { 
      } 

      public override void Serialize( GenericWriter writer ) 
      { 
         base.Serialize( writer ); 

         writer.Write( (int) 0 ); 
      } 
       
      public override void Deserialize(GenericReader reader) 
      { 
         base.Deserialize( reader ); 

         int version = reader.ReadInt(); 
      } 
   }     
} 