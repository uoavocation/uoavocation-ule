using System;
using Server.Network;

namespace Server.Items
{
    [FlipableAttribute(0x183B, 0x183C, 0x183D)]
    public class LargeFlask : Item
    {
        [Constructable]
        public LargeFlask()
            : base(0x183B)
        {
            Weight = 1.0;
            Movable = true;
        }

        public LargeFlask(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}