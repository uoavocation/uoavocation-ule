﻿//   ___|========================|___
//   \  |  Written by Felladrin  |  /   This script was released on RunUO Forums under the GPL licensing terms.
//    > |      August 2013       | <
//   /__|========================|__\   [Auto Ignite Lights] - Current version: 1.0 (August 18, 2013)

namespace Server.Items
{
    public class AutoIgniteLights
    {
        public static class Settings
        {
            public static bool Enabled = true;    // System Toggle: true | false
            public static int IgniteHour = 19;    // Lights Ignite At 7pm Every Night (24 Hour Time Frame: 13 = 1pm, 14 = 2pm, 15 = 3pm | 0 = 12am, 1 = 1am, 2 = 2am, 3 = 3am)
            public static int DouseHour = 7;      // Lights Go Out At 7am Every Day
            public static int CheckInterval = 5;  // How Often, In Minutes, Should We Check The Lights?
        }

        public static void Initialize()
        {
            if (Settings.Enabled)
                CheckLights();
        }

        public static void CheckLights()
        {
            foreach (Item item in World.Items.Values)
            {
                if (item is BaseLight && item.Parent == null)
                {
                    BaseLight light = item as BaseLight;

                    int currentHour, currentMinute;

                    Clock.GetTime(light.Map, light.X, light.Y, out currentHour, out currentMinute);

                    if (currentHour > Settings.IgniteHour || currentHour < Settings.DouseHour)
                    {
                        light.Ignite();
                    }
                    else if (currentHour > Settings.DouseHour || currentHour < Settings.IgniteHour)
                    {
                        light.Douse();
                    }
                }
            }

            Timer.DelayCall(System.TimeSpan.FromMinutes(Settings.CheckInterval), delegate { CheckLights(); });
        }
    }
}