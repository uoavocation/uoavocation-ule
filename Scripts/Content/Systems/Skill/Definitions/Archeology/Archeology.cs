/* Created by Hammerhand*/

using System;
using Server;
using Server.Items;
using Server.Targeting;
using Server.Network;
using Server.Commands;
using Server.Engines.Harvest;
using Server.Mobiles;
using Server.Regions;
using Server.Gumps;

namespace Server.Engines.Harvest
{
    public class Archeology : HarvestSystem
    {
        private static Archeology m_System;

        public static Archeology System
        {
            get
            {
                if (m_System == null)
                    m_System = new Archeology();

                return m_System;
            }
        }

        private HarvestDefinition m_Archeological;

        public HarvestDefinition Archeological
        {
            get { return m_Archeological; }
        }

        private Archeology()
        {
            HarvestResource[] res;
            HarvestVein[] veins;

            #region Archeological
            HarvestDefinition Archeological = m_Archeological = new HarvestDefinition();

            // Resource banks are every 6x6 tiles
            Archeological.BankWidth = 6;
            Archeological.BankHeight = 6;

            // Every bank holds from 10 to 15 rock
            Archeological.MinTotal = 1;
            Archeological.MaxTotal = 3;

            // A resource bank will respawn its content every 10 to 20 minutes
            Archeological.MinRespawn = TimeSpan.FromMinutes(10.0);
            Archeological.MaxRespawn = TimeSpan.FromMinutes(20.0);

            // Skill checking is done on the Mining skill
            Archeological.Skill = SkillName.Mining;

            // Set the list of harvestable tiles
            Archeological.Tiles = m_SandTiles;

            // Players must be within 2 tiles to harvest
            Archeological.MaxRange = 2;

            // One ore per harvest action
            Archeological.ConsumedPerHarvest = 1;
            Archeological.ConsumedPerFeluccaHarvest = 1;

            // The digging effect
            Archeological.EffectActions = new int[] { 11 };
            Archeological.EffectSounds = new int[] { 0x125, 0x126 };
            Archeological.EffectCounts = new int[] { 1 };
            Archeological.EffectDelay = TimeSpan.FromSeconds(1.6);
            Archeological.EffectSoundDelay = TimeSpan.FromSeconds(0.9);

            Archeological.NoResourcesMessage = "There are no artifacts here to mine."; // There is no metal here to mine.
            Archeological.DoubleHarvestMessage = "There are no artifacts here to mine."; // Someone has gotten to the metal before you.
            Archeological.TimedOutOfRangeMessage = 503041; // You have moved too far away to continue mining.
            Archeological.OutOfRangeMessage = 500446; // That is too far away.
            Archeological.FailMessage = "You dig for a while but fail to find anything."; // You loosen some rocks but fail to find any useable ore.
            Archeological.PackFullMessage = "Your backpack can't hold the artifact, and it is lost!"; // Your backpack is full, so the ore you mined is lost.
            Archeological.ToolBrokeMessage = 1044038; // You have worn out your tool!

            res = new HarvestResource[]
				{
                    new HarvestResource( 98.0, 65.0, 175.0, "You find an item and put it in your backpack", typeof( BonePile ), typeof( Bone ),  typeof( RibCage ),
                typeof( TelekinesisableDice ), typeof( SextantParts ), typeof( FishingPole ), typeof( FlourSifter ), typeof(Plate), typeof(Cards), typeof(PewterMug),
                typeof( BarrelHoops ), typeof( Scissors ), typeof(Fork), typeof(Knife), typeof(Chessmen), typeof(Bottle), typeof(BarrelTap), typeof( Spoon )),
                    new HarvestResource( 100.0, 70.0, 200.0, "You find an artifact and put it in your backpack", typeof (Archeological) )
				};

            veins = new HarvestVein[]
				{
                    new HarvestVein(55.0, 0.0, res[0], null),
              		new HarvestVein( 45.0, 0.5, res[1], res[0] )
				};

            Archeological.Resources = res;
            Archeological.Veins = veins;

            Definitions.Add(Archeological);
            #endregion
        }

        public override bool CheckHarvest(Mobile from, Item tool)
        {
            if (!base.CheckHarvest(from, tool))
                return false;

            if (from.Mounted)
            {
                from.SendLocalizedMessage(501864); // You can't dig while riding.
                return false;
            }
            else if (from.IsBodyMod && !from.Body.IsHuman)
            {
                from.SendLocalizedMessage(501865); // You can't dig while polymorphed.
                return false;
            }
            return true;
        }
        public override bool CheckHarvest(Mobile from, Item tool, HarvestDefinition def, object toHarvest)
        {
            if (!base.CheckHarvest(from, tool, def, toHarvest))
                return false;

            if (def == m_Archeological && !(from is PlayerMobile && from.Skills[SkillName.Mining].Base >= 100.0 && ((PlayerMobile)from).Archeology))
            {
                OnBadHarvestTarget(from, tool, toHarvest);
                return false;
            }
            else if (from.Mounted)
            {
                from.SendLocalizedMessage(501864); // You can't mine while riding.
                return false;
            }
            else if (from.IsBodyMod && !from.Body.IsHuman)
            {
                from.SendLocalizedMessage(501865); // You can't mine while polymorphed.
                return false;
            }
            return true;
        }
        public override void FinishHarvesting(Mobile from, Item tool, HarvestDefinition def, object toHarvest, object locked)
        {
            IUsesRemaining usesTool = tool as IUsesRemaining;

            int iUses = 0;
            if (usesTool != null)
                iUses = usesTool.UsesRemaining;
            base.FinishHarvesting(from, tool, def, toHarvest, locked);
            if (usesTool != null && !tool.Deleted)
            {
                usesTool.UsesRemaining = iUses - 1;
                if (usesTool.UsesRemaining <= 0)
                    tool.Delete();
                if (tool.Deleted)
                    def.SendMessageTo(from, def.ToolBrokeMessage);

            }
        }

        public override void OnHarvestFinished(Mobile from, Item tool, HarvestDefinition def, HarvestVein vein, HarvestBank bank, HarvestResource resource, object harvested)
        {
            HarvestResource res = vein.PrimaryResource;
            if (res == resource && res.Types.Length >= 3)
            {
                try
                {
                    Map map = from.Map;

                    if (map == null)
                        return;
                }
                catch
                {
                }
            }
        }
        public override bool BeginHarvesting(Mobile from, Item tool)
        {
            if (!base.BeginHarvesting(from, tool))
                return false;

            from.SendLocalizedMessage(503033); // Where do you wish to dig?
            return true;
        }
        public override void OnHarvestStarted(Mobile from, Item tool, HarvestDefinition def, object toHarvest)
        {
            base.OnHarvestStarted(from, tool, def, toHarvest);
        }
        public override void OnBadHarvestTarget(Mobile from, Item tool, object toHarvest)
        {
            if (toHarvest is LandTarget)
                from.SendMessage("You cant dig there"); // You can't mine there.
            else
                from.SendLocalizedMessage(501863); // You can't mine that.
        }
        public static void Initialize()
        {
            Array.Sort(m_SandTiles);
        }

        #region Harvestable Veins To Retrieve Items From

        private static int[] m_SandTiles = new int[]
			{
				22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
				32, 33, 34, 35, 36, 37, 38, 39, 40, 41,
				42, 43, 44, 45, 46, 47, 48, 49, 50, 51,
				52, 53, 54, 55, 56, 57, 58, 59, 60, 61,
				62, 68, 69, 70, 71, 72, 73, 74, 75,

				286, 287, 288, 289, 290, 291, 292, 293, 294, 295,
				296, 297, 298, 299, 300, 301, 402, 424, 425, 426,
				427, 441, 442, 443, 444, 445, 446, 447, 448, 449,
				450, 451, 452, 453, 454, 455, 456, 457, 458, 459,
				460, 461, 462, 463, 464, 465, 642, 643, 644, 645,
				650, 651, 652, 653, 654, 655, 656, 657, 821, 822,
				823, 824, 825, 826, 827, 828, 833, 834, 835, 836,
				845, 846, 847, 848, 849, 850, 851, 852, 857, 858,
				859, 860, 951, 952, 953, 954, 955, 956, 957, 958,
				967, 968, 969, 970,

				1447, 1448, 1449, 1450, 1451, 1452, 1453, 1454, 1455,
				1456, 1457, 1458, 1611, 1612, 1613, 1614, 1615, 1616,
				1617, 1618, 1623, 1624, 1625, 1626, 1635, 1636, 1637,
				1638, 1639, 1640, 1641, 1642, 1647, 1648, 1649, 1650
            };

        #endregion
    }
}

namespace Server.Items
{
    public class Archeological : Item
    {
        public override bool ForceShowProperties { get { return ObjectPropertyList.Enabled; } }

        public static int[] m_ArcheologicalID = new int[]
		{
            #region Harvestable Items That Can Be Retrieved

            1065,          //Gruesome Standard
            1056,          //Gruesome Standard
            2518, 
            0x1026, 
            4135, 
            3652, 
            3654, 
            3655, 
            4024, 
            3660, 
            4022, 
            6205, 
            6226, 
            4092, 
            7863, 
            7868, 
            7861, 
            4324, 
            4325, 
            0x13BB, 
            0x13BC, 
            0x13BD, 
            0x13BE, 
            0x13BF, 
            0x13C0, 
            0x13C1, 
            0x13C2, 
            0x13C3, 
            0x13C4, 
            0x13FA, 
            0x13FB, 
            0x097F, 
            0x09ED, 
            0x09F3, 
            0x0B45, 
            0x0B46,
            0x0B47, 
            0x0B48, 
            0x12D8, 
            0x12D9, 
            0x139A, 
            0x139B, 
            0x139C, 
            0x139D, 
            0x1BC6, 
            0x1BC7, 
            5384,
            5394, 
            5402, 
            5404

            #endregion
		};

        public Archeological()
            : base(1)
        {
            ItemID = m_ArcheologicalID[Utility.Random(m_ArcheologicalID.Length)];
            Weight = 10;
        }

        public override void AddNameProperties(ObjectPropertyList list)
        {
            base.AddNameProperties(list);
            list.Add(1049644, "Recovered from an Archeological site");
        }

        public Archeological(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}