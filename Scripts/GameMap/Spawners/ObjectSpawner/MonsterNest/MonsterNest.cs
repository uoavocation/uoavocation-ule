using System;
using System.Collections;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
//RewardOnlyForAttackers
using System.Collections.Generic;
//

namespace Server.Items
{
    public class MonsterNest : Item
    {
        private Type m_NestSpawnType;
        private int m_MaxCount;
        private TimeSpan m_RespawnTime;
        private ArrayList m_Spawn;
        private int m_HitsMax;
        private int m_Hits;
        private int m_RangeHome;
        private DateTime m_Attackable;
        private int m_LootLevel;

        [CommandProperty(AccessLevel.GameMaster)]
        public Type NestSpawnType
        {
            get { return m_NestSpawnType; }
            set { m_NestSpawnType = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxCount
        {
            get { return m_MaxCount; }
            set { m_MaxCount = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public TimeSpan RespawnTime
        {
            get { return m_RespawnTime; }
            set { m_RespawnTime = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int HitsMax
        {
            get { return m_HitsMax; }
            set { m_HitsMax = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int Hits
        {
            get { return m_Hits; }
            set { m_Hits = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int RangeHome
        {
            get { return m_RangeHome; }
            set { m_RangeHome = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int LootLevel
        {
            get { return m_LootLevel; }
            set { m_LootLevel = value; }
        }

        //RewardOnlyForAttackers
        private List<Mobile> m_Attackers = new List<Mobile>();
        public List<Mobile> Attackers
        {
            get { return m_Attackers; }
        }
        //

        [Constructable]
        public MonsterNest()
            : base(4962)
        {
            m_NestSpawnType = null;
            Name = "Monster Nest";
            m_Spawn = new ArrayList();
            Weight = 0.1;
            Hue = 1818;
            HitsMax = 300;
            Hits = 300;
            RangeHome = 10;
            RespawnTime = TimeSpan.FromSeconds(15.0);
            new InternalTimer(this).Start();
            new RegenTimer(this).Start();
            Movable = false;
        }

        public override void OnDelete()
        {
            base.OnDelete();

            if (m_Spawn != null && m_Spawn.Count > 0)
            {
                for (int i = 0; i < this.m_Spawn.Count; i++)
                {
                    Mobile m = (Mobile)this.m_Spawn[i];
                    m.Delete();
                }
            }
        }

        public override void OnDoubleClick(Mobile from)
        {
            BaseWeapon weapon = from.FindItemOnLayer(Layer.OneHanded) as BaseWeapon;
            if (weapon == null)
                weapon = from.FindItemOnLayer(Layer.TwoHanded) as BaseWeapon;
            if (weapon != null && DateTime.Now > this.m_Attackable)
            {
                if (from.InRange(this.Location, weapon.MaxRange))
                {
                    //RewardOnlyForAttackers
                    if (!m_Attackers.Contains(from)) m_Attackers.Add(from);
                    //

                    if (weapon is BaseRanged)
                    {
                        BaseRanged r = weapon as BaseRanged;
                        Cat cat = new Cat();
                        cat.CantWalk = true;
                        cat.Hidden = true;
                        cat.MoveToWorld(this.Location, this.Map);
                        if (r.OnFired(from, cat))
                        { cat.Delete(); }
                        else
                            return;
                    }
                    int damage = (Utility.Random(weapon.MinDamage, weapon.MaxDamage) * from.Str) / 75;
                    this.Hits -= damage;
                    this.PublicOverheadMessage(MessageType.Regular, 0x22, false, damage.ToString());
                    weapon.PlaySwingAnimation(from);
                    this.m_Attackable = DateTime.Now + TimeSpan.FromSeconds(2.0);
                    if (this.Hits <= 0)
                        this.Destroy();
                }
                else
                    from.SendMessage(0, "You must get closer to attack that.");
            }
            else if (DateTime.Now < this.m_Attackable)
                from.SendMessage(0, "You must wait to attack that again.");
            else
                from.SendMessage(0, "You must equip a weapon to attack that.");
        }

        public virtual void AddLoot()
        {
            //RewardOnlyForAttackers
            //MonsterNestLoot loot = new MonsterNestLoot( 6585, this.Hue, this.m_LootLevel, "Monster Nest remains" );
            MonsterNestLoot loot = new MonsterNestLoot(6585, this.Hue, this.m_LootLevel, "Monster Nest remains", m_Attackers);
            //

            loot.MoveToWorld(this.Location, this.Map);
        }

        public void Destroy()
        {
            AddLoot();
            if (m_Spawn != null && m_Spawn.Count > 0)
            {
                for (int i = 0; i < this.m_Spawn.Count; i++)
                {
                    Mobile m = (Mobile)this.m_Spawn[i];
                    m.Kill();
                }
            }
            this.Delete();
        }

        public int Count()
        {
            int c = 0;
            if (this.m_Spawn != null && this.m_Spawn.Count > 0)
            {
                for (int i = 0; i < this.m_Spawn.Count; i++)
                {
                    Mobile m = (Mobile)this.m_Spawn[i];
                    if (m.Alive)
                        c += 1;
                }
            }
            return c;
        }

        public void DoSpawn()
        {
            if (this.NestSpawnType != null && this.m_Spawn != null && this.Count() < this.m_MaxCount)
            {

                try
                {
                    object o = Activator.CreateInstance(this.NestSpawnType);
                    if (o != null && o is Mobile)
                    {
                        Mobile c = o as Mobile;
                        if (c is BaseCreature)
                        {
                            BaseCreature m = o as BaseCreature;
                            this.m_Spawn.Add(m);
                            m.OnBeforeSpawn(this.Location, this.Map);
                            m.MoveToWorld(this.Location, this.Map);
                            m.Home = this.Location;
                            m.RangeHome = this.m_RangeHome;
                        }
                    }
                }
                catch
                {
                    this.NestSpawnType = null;
                }
            }
        }

        public MonsterNest(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0);
            writer.WriteMobileList(m_Spawn);
            writer.Write((int)m_MaxCount);
            writer.Write((TimeSpan)m_RespawnTime);
            writer.Write((int)m_HitsMax);
            writer.Write((int)m_Hits);
            writer.Write((int)m_RangeHome);
            writer.Write((int)m_LootLevel);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
            m_Spawn = reader.ReadMobileList();
            m_MaxCount = reader.ReadInt();
            m_RespawnTime = reader.ReadTimeSpan();
            m_HitsMax = reader.ReadInt();
            m_Hits = reader.ReadInt();
            m_RangeHome = reader.ReadInt();
            m_LootLevel = reader.ReadInt();
        }

        private class RegenTimer : Timer
        {
            private MonsterNest nest;
            public RegenTimer(MonsterNest n)
                : base(TimeSpan.FromMinutes(1.0))
            {
                nest = n;
            }
            protected override void OnTick()
            {
                if (nest != null && !nest.Deleted)
                {
                    nest.Hits += nest.HitsMax / 10;
                    if (nest.Hits > nest.HitsMax)
                    {
                        nest.Hits = nest.HitsMax;
                        //RewardOnlyForAttackers
                        nest.m_Attackers.Clear();
                        //
                    }
                    new RegenTimer(nest).Start();
                }
            }
        }

        private class InternalTimer : Timer
        {
            private MonsterNest nest;
            public InternalTimer(MonsterNest n)
                : base(n.RespawnTime)
            {
                nest = n;
            }
            protected override void OnTick()
            {
                if (nest != null && !nest.Deleted)
                {
                    nest.DoSpawn();
                    new InternalTimer(nest).Start();
                }
            }
        }
    }

    public class MonsterNestLoot : Item
    {
        private int m_LootLevel;

        //RewardOnlyForAttackers
        private List<Mobile> m_attackers = new List<Mobile>();
        //

        [Constructable]
        //RewardOnlyForAttackers
        //public MonsterNestLoot(int itemid, int hue, int lootlevel, string name) : base()
        public MonsterNestLoot(int itemid, int hue, int lootlevel, string name, List<Mobile> attackers)
            : base()
        //
        {
            Name = name + " (double click to loot)".ToString();
            Hue = hue;
            ItemID = itemid;
            Movable = false;
            m_LootLevel = lootlevel;
            //RewardOnlyForAttackers
            m_attackers = attackers;
            //
        }

        public override void OnDoubleClick(Mobile from)
        {
            ArrayList alist = new ArrayList();

            IPooledEnumerable eable = this.Map.GetMobilesInRange(this.Location, 20);
            foreach (Mobile m in eable)
            {
                //RewardOnlyForAttackers
                if (m_attackers.Contains(m))
                    //
                    alist.Add(m);
            }
            eable.Free();

            if (alist.Count > 0)
            {
                for (int i = 0; i < alist.Count; i++)
                {
                    Mobile m = (Mobile)alist[i];
                    if (m is PlayerMobile)
                        AddLoot(m);
                }
            }

            this.Delete();
        }

        public void AddLoot(Mobile m)
        {
            int chance = Utility.Random(5, 20) * m_LootLevel;
            if (chance < 10)
                m.AddToBackpack(new Gold(Utility.Random(500, 1000)));
            else if (chance < 20)
                m.AddToBackpack(new Gold(Utility.Random(1000, 2000)));
            else if (chance < 30)
            {
                m.AddToBackpack(new BankCheck(Utility.Random(2000, 3000)));
            }
            else if (chance < 40)
            {
                m.AddToBackpack(new BankCheck(Utility.Random(3000, 4000)));
            }
            else if (chance < 50)
            {
                m.AddToBackpack(new BankCheck(Utility.Random(4000, 5000)));
            }
            else if (chance < 60)
            {
                m.AddToBackpack(new BankCheck(Utility.Random(5000, 6000)));
            }
            else if (chance < 70)
            {
                m.AddToBackpack(new BankCheck(Utility.Random(6000, 7000)));
            }
            else if (chance < 80)
            {
                m.AddToBackpack(new BankCheck(Utility.Random(7000, 8000)));
            }
            else if (chance < 90)
            {
                m.AddToBackpack(new BankCheck(Utility.Random(8000, 9000)));
            }
            else
            {
                m.AddToBackpack(new BankCheck(Utility.Random(7000, 10000)));
            }
        }

        public MonsterNestLoot(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0);
            writer.Write((int)m_LootLevel);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
            m_LootLevel = reader.ReadInt();
        }

        private class RegenTimer : Timer
        {
            private MonsterNest nest;
            public RegenTimer(MonsterNest n)
                : base(TimeSpan.FromSeconds(30.0))
            {
                nest = n;
            }
            protected override void OnTick()
            {
                if (nest != null && !nest.Deleted)
                {
                    if (nest.Hits < 0)
                    {
                        nest.Destroy();
                        return;
                    }
                    nest.Hits += nest.HitsMax / 10;
                    if (nest.Hits > nest.HitsMax)
                        nest.Hits = nest.HitsMax;
                    new RegenTimer(nest).Start();
                }
            }
        }

        private class SpawnTimer : Timer
        {
            private MonsterNest nest;
            public SpawnTimer(MonsterNest n)
                : base(n.RespawnTime)
            {
                nest = n;
            }
            protected override void OnTick()
            {
                if (nest != null && !nest.Deleted)
                {
                    nest.DoSpawn();
                    new SpawnTimer(nest).Start();
                }
            }
        }
    }
}