/* Created by Hammerhand*/

using System;
using Server;
using Server.Items;
using Server.Targeting;
using Server.Network;
using Server.Commands;
using Server.Engines.Harvest;
using Server.Mobiles;
using Server.Regions;
using Server.Gumps;

namespace Server.Items
{
	public class ArcheologyBook : Item
	{
		[Constructable]
		public ArcheologyBook() : base( 0xFF4 )
		{
            Name = "Learning Archeology";
			Weight = 1.0;
            Hue = 0;
		}

        public ArcheologyBook(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override void OnDoubleClick( Mobile from )
		{
			PlayerMobile pm = from as PlayerMobile;

			if ( !IsChildOf( from.Backpack ) )
			{
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
			}
			else if ( pm == null || from.Skills[SkillName.Mining].Base < 100.0 )
			{
				pm.SendMessage( "Only a Grandmaster Miner can learn from this book." );
			}
            else if (pm.Archeology)
			{
				pm.SendMessage( "You have already learned this information." );
			}
			else
			{
                pm.Archeology = true;
                pm.SendMessage("You have learned how to dig for ancient artifacts. Dig in archeological sites to search for artifacts.");
				Delete();
			}
		}
	}
}