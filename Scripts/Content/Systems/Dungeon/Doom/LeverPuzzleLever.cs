using System;
using System.Collections;
using Server;
using Server.Spells;
using Server.Mobiles;
using Server.Network;

namespace Server.Engines.Doom
{
    public class LeverPuzzleLever : Item
    {
        private UInt16 m_Code;
        private LeverPuzzleController m_Controller;

        [CommandProperty(AccessLevel.GameMaster)]
        public UInt16 Code
        {
            get { return m_Code; }
        }

        public LeverPuzzleLever(UInt16 code, LeverPuzzleController controller)
            : base(0x108E)
        {
            m_Controller = controller;
            m_Code = code;
            Hue = 0x66D;
            Movable = false;
        }

        public override void OnDoubleClick(Mobile m)
        {
            if (m != null && m_Controller.Enabled)
            {
                ItemID ^= 2;
                Effects.PlaySound(Location, Map, 0x3E8);
                m_Controller.LeverPulled(m_Code);
            }
            else
            {
                m.SendLocalizedMessage(1060001); // You throw the switch, but the mechanism cannot be engaged again so soon.
            }
        }

        public override void OnAfterDelete()
        {
            if (m_Controller != null && !m_Controller.Deleted)
                m_Controller.Delete();
        }

        public LeverPuzzleLever(Serial serial)
            : base(serial)
        {
        }
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
            writer.Write((ushort)m_Code);
            writer.Write(m_Controller);
        }
        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
            m_Code = reader.ReadUShort();
            m_Controller = reader.ReadItem() as LeverPuzzleController;
        }
    }
}