using System;
using Server.Network;

namespace Server.Items
{
    public class RedBeaker : Item
    {
        [Constructable]
        public RedBeaker()
            : base(0x182F)
        {
            Weight = 1.0;
            Movable = true;
        }

        public RedBeaker(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}