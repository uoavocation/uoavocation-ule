using System;
using System.Collections;
using System.Collections.Generic;
using Server.Targeting;
using Server.Network;
using Server.Items;
using Server.Regions;

namespace Server.Mobiles
{
	public class FlyingAI
	{
		public static bool debug = false;

		public static void CheckMethods( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;
		
			CheckMap( fbc );
			CheckLandTile( fbc );
			CheckStaticLandTile( fbc );
			CheckMapEdge( fbc );
			CheckStatic( fbc );
			CheckHouse( fbc );
			CheckDeepWater( fbc );
			FlyingRange( fbc );

			if ( debug == true )
				fbc.Say( "Check Methods" );
		}

		public static void AnimateFlying( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

               		fbc.PlaySound( 0x2D0 );
			fbc.Animate( 24, 5, 1, true, false, 0 );
		}

		public static void CheckMap( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Check Map" );

			if ( fbc.Map == Map.Felucca )
			{
				fbc.LeftSide = 5;
				fbc.RightSide = 5100;
				fbc.TopSide = 5;
				fbc.BottomSide = 4090;
				fbc.Ceiling = 80;
			}

			if ( fbc.Map == Map.Trammel )
			{
				fbc.LeftSide = 5;
				fbc.RightSide = 5100;
				fbc.TopSide = 5;
				fbc.BottomSide = 4090;
				fbc.Ceiling = 80;
			}

			if ( fbc.Map == Map.Ilshenar )
			{
				fbc.IsFlying = false;
				fbc.IsTakingOff = false;
				return;
			}

			if ( fbc.Map == Map.Malas )
			{
				fbc.LeftSide = 515;
				fbc.RightSide = 2555;
				fbc.TopSide = 5;
				fbc.BottomSide = 2045;
				fbc.Ceiling = 80;
			}

			if ( fbc.Map == Map.Tokuno )
			{
				fbc.LeftSide = 5;
				fbc.RightSide = 1445;
				fbc.TopSide = 5;
				fbc.BottomSide = 1445;
				fbc.Ceiling = 80;
			}
		}

		public static void RunFly( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Run Fly Check" );

			fbc.Direction |= Direction.Running;
		}

		public static void FlyingRange( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Flying Range Check" );

			int hx = fbc.Home.X;
			int hy = fbc.Home.Y;
			int hz = fbc.Home.Z;

			if ( (hx + hy + hz) == 0 )
				return;

			int mx = fbc.X;
			int my = fbc.Y;
			int mz = fbc.Z;

			int range = fbc.RangeHome;
			
			if ( hx > mx )
			{
				if ( (hx - mx) >= range )
					ChangeDirection( fbc );
			}
			if ( hx < mx )
			{
				if ( (mx - hx) >= range )
					ChangeDirection( fbc );
			}
			if ( hy > my )
			{
				if ( (hy - my) >= range )
					ChangeDirection( fbc );
			}
			if ( hy < my )
			{
				if ( (my - hy) >= range )
					ChangeDirection( fbc );
			}
			if ( hz > mz )
			{
				if ( (hz - mz) >= range )
				{
					if ( fbc.FlyingDown == true )
					{
						fbc.FlyingDown = false;
						fbc.FlyingUp = true;
						return;
					}
					fbc.FlyingUp = true;
					return;
				}	
			}
			if ( hz < mz )
			{
				if ( (mz - hz) >= range )
				{
					if ( fbc.FlyingUp == true )
					{
						fbc.FlyingUp = false;
						fbc.FlyingDown = true;
						return;
					}
					fbc.FlyingDown = true;
					return;
				}	
			}
		}

		public static void TakeOff( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "I'll Take Off" );

			if ( fbc.Ground == fbc.Z )
			{
				fbc.FlyingUp = true;
				RandomChangeDirection( fbc );
				MaskedDirection( fbc );
				AnimateFlying( fbc );
				fbc.Z++;   
				return;
			}
		
			if ( fbc.Ground < fbc.Z )
				fbc.Z++;   
		
			if ( fbc.Z > (fbc.Ground + 20) )
			{ 
				fbc.IsTakingOff = false;    
			}
		}

		public static void Flying( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "I'm Flying" );

			if ( CheckAbove( fbc ))
			{

				if ( debug == true )
					fbc.Say( "Check Above" );

				fbc.IsFlying = false;
				fbc.IsTakingOff = false;
				return;
			}

			Timer f_timer = new FlyingTimer( fbc );

			CheckMethods( fbc );

			if ( fbc.CanFlying == false )
				return;

			if ( fbc.FlyStam <= 50 )
			{
				fbc.FlyingUp = false;
				fbc.IsLanding = true;
				fbc.FlyingDown = true;

				if ( debug == true )
					fbc.Say( "Fly Stam Low" );
			}

			if ( (fbc.Z + 20) >= fbc.Ceiling )
			{
				fbc.FlyingUp = false;
				fbc.FlyingDown = true;
				fbc.Z--;

				if ( debug == true )
					fbc.Say( "Ceiling Hit" );
			}

			if ( fbc.FlyingUp == true )
				fbc.Z++;
			if ( fbc.FlyingDown == true )
				fbc.Z--;

			CheckMethods( fbc );

			int direct = Utility.Random( 1, 20 );
			int altitude = Utility.Random( 1, 3 );

			if ( fbc.IsTakingOff == true )
			{
				if ( debug == true )
					fbc.Say( "Is Taking Off" );

				TakeOff( fbc );
 
				if ( direct == 7 )
					RandomChangeDirection( fbc ); 
				MaskedDirection( fbc );

				CheckMethods( fbc );

    				f_timer.Start();

				if ( fbc.FlyAnim == 2 )
    					AnimateFlying( fbc );

				if ( fbc.FlyStam > 10 )
					fbc.FlyStam--;
				return;
			}

			if ( fbc.IsLanding == true )
			{
				if ( debug == true )
					fbc.Say( "Is Landing" );

				Landing( fbc );

				if ( fbc.IsLanding == false )
					return;

				if ( direct == 7 )
					RandomChangeDirection( fbc );
				MaskedDirection( fbc );

				CheckMethods( fbc );

				f_timer.Start();

				if ( fbc.FlyAnim == 2 )
    					AnimateFlying( fbc );	
				return;
			}

			if ( (fbc.Z - 20) <= fbc.Ground )
			{
				if ( debug == true )
					fbc.Say( "Check Ground" );

				fbc.FlyingUp = true;
				fbc.FlyingDown = false;
				fbc.Z++;
			}
			
			if ( direct == 7 )
				RandomChangeDirection( fbc ); 
			if ( altitude == 2 )
				RandomAltitudeDirection( fbc ); 
			MaskedDirection( fbc );

			CheckMethods( fbc );
                         
			f_timer.Start();

			if ( fbc.FlyAnim == 2 )
    				AnimateFlying( fbc );	
		}

		public static void Landing( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "I'm Landing" );

			if ( fbc.Ground > (fbc.Z - 1) )
			{
				fbc.Z = fbc.Ground + 1;
				fbc.IsLanding = false;
				fbc.IsFlying = false;
				fbc.FlyingUp = false;
				fbc.FlyingDown = false;
				return;
			}
			if ( fbc.Ground == (fbc.Z - 1) )
			{
				fbc.IsLanding = false;
				fbc.IsFlying = false;
				fbc.FlyingUp = false;
				fbc.FlyingDown = false;
				CheckStaticLanding( fbc );
				return;
			}
			if ( fbc.Z > (fbc.Ground + 1) )
				fbc.Z--;
		}

		public static void CheckStatic( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Check Static" );

			const int a = -1;
			const int b = 1;
			const int c = 0;

			bool Obs = false;

			int xx = 0;
			int yy = 0;

			if ( fbc.CDirection == 1 )
			{
				xx = c;
				yy = a;
			}

			if ( fbc.CDirection == 2 )
			{
				xx = b;
				yy = a;
			}

			if ( fbc.CDirection == 3 )
			{
				xx = b;
				yy = c;
			}

			if ( fbc.CDirection == 4 )
			{
				xx = b;
				yy = b;
			}

			if ( fbc.CDirection == 5 )
			{
				xx = c;
				yy = b;
			}

			if ( fbc.CDirection == 6 )
			{
				xx = a;
				yy = b;
			}

			if ( fbc.CDirection == 7 )
			{
				xx = a;
				yy = c;
			}

			if ( fbc.CDirection == 8 )
			{
				xx = a;
				yy = a;
			}

                      	StaticTile[] tiles = fbc.Map.Tiles.GetStaticTiles( ( fbc.X + xx ), ( fbc.Y + yy ), true );

			if ( tiles == null )
				return;

			for ( int i = 0; i < tiles.Length; ++i )
		        {
		           	StaticTile tile = tiles[i];

				if ( tile.Z >= fbc.Z )
					Obs = true;
               		} 
			if ( Obs == true )
				ChangeDirection( fbc );
		}

		public static void CheckHouse( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Check House" );

		      	if ( fbc.Region.Name == "HouseRegion" )
				ChangeDirection( fbc );
		}

		public static void CheckLandTile( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Check Land" );

		      	LandTile landTile = fbc.Map.Tiles.GetLandTile( fbc.X, fbc.Y );

			fbc.Ground = landTile.Z;

			LandData lD = TileData.LandTable[ landTile.ID & 0x3FFF ];

			if ( lD.Name == "rock" )
				ChangeDirection( fbc );
		}

		public static void CheckStaticLandTile( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Check Static Land" );

                      	StaticTile[] tiles = fbc.Map.Tiles.GetStaticTiles( ( fbc.X ), ( fbc.Y ), true );

			if ( tiles == null )
				return;

			for ( int i = 0; i < tiles.Length; ++i )
		        {
		           	StaticTile tile = tiles[i];

                           	if ( tile.Z > fbc.Ground )
					fbc.Ground = tile.Z;
               		} 
		}

		public static void CheckStaticLanding( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Check Static Landing" );

		      	LandTile landTile = fbc.Map.Tiles.GetLandTile( fbc.X, fbc.Y );

			fbc.Ground = landTile.Z;

                      	StaticTile[] tiles = fbc.Map.Tiles.GetStaticTiles( ( fbc.X ), ( fbc.Y ), true );

			if ( tiles == null )
				return;

			for ( int i = 0; i < tiles.Length; ++i )
		        {
		           	StaticTile tile = tiles[i];

				if ( tile.Z <= fbc.Ground )
					return;

                           	if ( tile.Z == fbc.Z )
					fbc.Z++;

                           	if ( tile.Z == ( fbc.Z -1 ))
				{
					fbc.FlyStam = fbc.FlyStamMax;
				}
               		} 
		}

		public static bool CheckAbove( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return true;

			if ( debug == true )
				fbc.Say( "Check Above" );

		      	LandTile landTile = fbc.Map.Tiles.GetLandTile( fbc.X, fbc.Y );

			fbc.Ground = landTile.Z;

			if ( (fbc.Ground - 10) > fbc.Z )
				return true;

                      	StaticTile[] tiles = fbc.Map.Tiles.GetStaticTiles( ( fbc.X ), ( fbc.Y ), true );

			if ( tiles == null )
				return false;

			for ( int i = 0; i < tiles.Length; ++i )
		        {
		           	StaticTile tile = tiles[i];

				if ( (tile.Z - 10) > fbc.Z )
					return true;
               		} 
			return false;
		}

		public static void CheckMapEdge( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Check Map Edge" );

			if ( fbc.X < fbc.LeftSide )
				ResetNoFly( fbc );
			if ( fbc.X > fbc.RightSide )
				ResetNoFly( fbc );
			if ( fbc.Y < fbc.TopSide )
				ResetNoFly( fbc );
			if ( fbc.Y > fbc.BottomSide )
				ResetNoFly( fbc );

			if ( fbc.X - 1 <= fbc.LeftSide )
				ChangeDirection( fbc );
			if ( fbc.X + 1 >= fbc.RightSide )
				ChangeDirection( fbc );
			if ( fbc.Y - 1 <= fbc.TopSide )
				ChangeDirection( fbc );
			if ( fbc.Y + 1 >= fbc.BottomSide )
				ChangeDirection( fbc );
		}

 		public static void CheckDeepWater( FlyingCreature fbc )
        	{
            		if( NullCheck( fbc ) )
                		return;

			if ( debug == true )
				fbc.Say( "Check Deep Water" );

            		bool deepWater = SpecialFishingNet.FullValidation( fbc.Map, fbc.Location.X, fbc.Location.Y );

            		if( deepWater )
                		ChangeDirection( fbc );
        	}

		public static void ChangeDirection( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Change Direction" );

			if ( fbc.CDirection == 1 )
			{
				fbc.Direction = Direction.South;
				fbc.CDirection = 5;
				MaskedDirection( fbc );
				return;
			}
			if ( fbc.CDirection == 2 )
			{
				fbc.Direction = Direction.Left;
				fbc.CDirection = 6;
				MaskedDirection( fbc );
				return;
			}
			if ( fbc.CDirection == 3 )
			{
				fbc.Direction = Direction.West;
				fbc.CDirection = 7;

				MaskedDirection( fbc );
				return;
			}
			if ( fbc.CDirection == 4 )
			{
				fbc.Direction = Direction.Up;
				fbc.CDirection = 8;
				MaskedDirection( fbc );
				return;
			}
			if ( fbc.CDirection == 5 )
			{
				fbc.Direction = Direction.North;
				fbc.CDirection = 1;
				MaskedDirection( fbc );
				return;
			}
			if ( fbc.CDirection == 6 )
			{
				fbc.Direction = Direction.Right;
				fbc.CDirection = 2;
				MaskedDirection( fbc );
				return;
			}
			if ( fbc.CDirection == 7 )
			{
				fbc.Direction = Direction.East;
				fbc.CDirection = 3;
				MaskedDirection( fbc );
				return;
			}
			if ( fbc.CDirection == 8 )
			{
				fbc.Direction = Direction.Down;
				fbc.CDirection = 4;
				MaskedDirection( fbc );
				return;
			}
		}

		public static void RandomChangeDirection( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Random Change Direction" );

			fbc.CDirection = Utility.Random( 1, 8 );

			if ( fbc.CDirection == 1 )
				fbc.Direction = Direction.North;
			if ( fbc.CDirection == 2 )
				fbc.Direction = Direction.Right;
			if ( fbc.CDirection == 3 )
				fbc.Direction = Direction.East;
			if ( fbc.CDirection == 4 )
				fbc.Direction = Direction.Down;
			if ( fbc.CDirection == 5 )
				fbc.Direction = Direction.South;
			if ( fbc.CDirection == 6 )
				fbc.Direction = Direction.Left;
			if ( fbc.CDirection == 7 )
				fbc.Direction = Direction.West;
			if ( fbc.CDirection == 8 )
				fbc.Direction = Direction.Up;

			RunFly( fbc );
		}

		public static void MaskedDirection( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Mask Direction" );

			if ( fbc.CDirection == 1 )
				fbc.Direction = Direction.North;
			if ( fbc.CDirection == 2 )
				fbc.Direction = Direction.Right;
			if ( fbc.CDirection == 3 )
				fbc.Direction = Direction.East;
			if ( fbc.CDirection == 4 )
				fbc.Direction = Direction.Down;
			if ( fbc.CDirection == 5 )
				fbc.Direction = Direction.South;
			if ( fbc.CDirection == 6 )
				fbc.Direction = Direction.Left;
			if ( fbc.CDirection == 7 )
				fbc.Direction = Direction.West;
			if ( fbc.CDirection == 8 )
				fbc.Direction = Direction.Up;

			RunFly( fbc );
		}

		public static void RandomAltitudeDirection( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Random Alt Direction Change" );

			int updown = Utility.Random( 1, 10 );

			if ( updown <= 1 )
			{
				if ( fbc.Ground <= fbc.Z - 21 )
					fbc.Z--;
				else fbc.Z++;
			}
			if ( updown >= 2 )
			{
				if ( fbc.Ceiling >= fbc.Z + 21 )
					fbc.Z++;
				else fbc.Z--;
			}

			if ( fbc.FlyStam > 10 )
				fbc.FlyStam--;
		}

		public static void ResetNoFly( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			if ( debug == true )
				fbc.Say( "Reset No Fly" );
		
			if ( fbc.CanFlying == true )
				fbc.CanFlying = false;
		}

		public static bool NullCheck( FlyingCreature fbc )
		{
			if ( fbc == null )
				return true;
			else if ( !fbc.Alive )
                		return true;
			else if ( fbc.Deleted )
                		return true;
			else
				return false;
		}

		public static void SpawnShadow( FlyingCreature fbc )
		{
			if ( NullCheck( fbc ))
				return;

			int bld = 0;

			Map map = fbc.Map;

			if ( map == null )
				return;

			if ( fbc.CDirection == 1 )
				bld +=7425;
			if ( fbc.CDirection == 2 )
				bld +=7418;
			if ( fbc.CDirection == 3 )
				bld +=7411;
			if ( fbc.CDirection == 4 )
				bld +=7414;
			if ( fbc.CDirection == 5 )
				bld +=7416;
			if ( fbc.CDirection == 6 )
				bld +=7428;
			if ( fbc.CDirection == 7 )
				bld +=7421;
			if ( fbc.CDirection == 8 )
				bld +=7422;
			if ( bld == 0 )
				bld +=7428;

			double Spd = fbc.FlySpd;

			FlyingShadow shadow = new FlyingShadow( bld, Spd );

			Point3D loc = fbc.Location;
			
			int x = fbc.X;
			int y = fbc.Y;
			int z = fbc.Ground+1;

			loc = new Point3D( x, y, z );
			
			shadow.MoveToWorld( loc, map );
		
			if ( shadow.Z > fbc.Z )
				fbc.Z = shadow.Z + 2;
		}

		public class FlyingTimer : Timer 
                { 
                	private Mobile m_Mobile; 

                	//public FlyingTimer( FlyingCreature m ) : base( TimeSpan.FromSeconds( fbc.FlySpd ) )
                	public FlyingTimer( FlyingCreature fbc ) : base( TimeSpan.FromSeconds( fbc.FlySpd ) )
                	{
                		m_Mobile = fbc;
                	} 

                	protected override void OnTick() 
                	{	
				FlyingCreature fbc = m_Mobile as FlyingCreature;

				if ( debug == true )
					fbc.Say( "{0}", fbc.Z );

				if ( fbc == null )
                		{
					Stop();
					return;
                		}

				if ( !fbc.Alive )
                		{
					Stop();
					return;
                		}

				if ( fbc.Deleted )
                		{
					Stop();
					return;
                		}

				if ( fbc.FlyAnim >= 2 )
    					fbc.FlyAnim = 0;
				fbc.FlyAnim++;

				if ( fbc.IsFlying == true )
				{
					if ( fbc.CDirection == 1 )
					{
						RunFly( fbc );
						fbc.Y--;

						if ( fbc.FlyStam > 10 )
							fbc.FlyStam--;
						SpawnShadow( fbc );
					}

					if ( fbc.CDirection == 2 )
					{
						RunFly( fbc );
						fbc.X++;
						fbc.Y--;

						if ( fbc.FlyStam > 10 )
							fbc.FlyStam--;
						SpawnShadow( fbc );
					}

					if ( fbc.CDirection == 3 )
					{
						RunFly( fbc );
						fbc.X++;

						if ( fbc.FlyStam > 10 )
							fbc.FlyStam--;
						SpawnShadow( fbc );
					}

					if ( fbc.CDirection == 4 )
					{
						RunFly( fbc );
						fbc.X++;
						fbc.Y++;

						if ( fbc.FlyStam > 10 )
							fbc.FlyStam--;
						SpawnShadow( fbc );
					}

					if ( fbc.CDirection == 5 )
					{
						RunFly( fbc );
						fbc.Y++;

						if ( fbc.FlyStam > 10 )
							fbc.FlyStam--;
						SpawnShadow( fbc );
					}

					if ( fbc.CDirection == 6 )
					{
						RunFly( fbc );
						fbc.X--;
						fbc.Y++;

						if ( fbc.FlyStam > 10 )
							fbc.FlyStam--;
						SpawnShadow( fbc );
					}

					if ( fbc.CDirection == 7 )
					{
						RunFly( fbc );
						fbc.X--;

						if ( fbc.FlyStam > 10 )
							fbc.FlyStam--;
						SpawnShadow( fbc );
					}

					if ( fbc.CDirection == 8 )
					{
						RunFly( fbc );
						fbc.X--;
						fbc.Y--;

						if ( fbc.FlyStam > 10 )
							fbc.FlyStam--;
						SpawnShadow( fbc );
					}
					Flying( fbc );
					Stop();
				}
				Stop();
			} 
		}
	}
}