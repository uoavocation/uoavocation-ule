using System;
using System.Threading;

using Server;
using Server.Commands;

namespace Server.Engines.WatchUO
{
    public abstract class Core
    {
        // Configuration
        public static bool Enabled = true;
        public static bool OpenOnStartup = true;

        public static string Version = "2.0.0";

        private static Thread m_Thread = null;
        public static Thread Thread
        {
            get { return m_Thread; }
        }

        private static MainForm m_MainForm = null;
        public static MainForm MainForm
        {
            get { return m_MainForm; }
        }

        private static PluginLoader m_PluginLoader = null;
        public static PluginLoader PluginLoader
        {
            get { return m_PluginLoader; }
        }

        [CallPriority(5)]
        public static void Initialize()
        {
            Server.Commands.CommandSystem.Register("Management", AccessLevel.Administrator, new CommandEventHandler(Management_OnCommand));
            Server.Commands.CommandSystem.Register("HideManagement", AccessLevel.Administrator, new CommandEventHandler(HideManagement_OnCommand));
            
            if (Enabled && OpenOnStartup)
            {
                CreateMainForm();
            }
        }

        public static void RunApp()
        {
            System.Windows.Forms.Application.Run(m_MainForm);
        }

        [Usage("Management")]
        [Description("Shows the Management GUI if enabled")]
        public static void Management_OnCommand(CommandEventArgs args)
        {
            if (!Enabled)
            {
                args.Mobile.SendMessage(0x21, "Management GUI is not active");
                return;
            }
            if (m_MainForm != null && !m_MainForm.Disposing)
            {
                try
                {
                    m_MainForm.Visible = true;
                }
                catch
                { //(ObjectDisposedException ode) {
                    CreateMainForm();
                }
            }
            else
            {
                CreateMainForm();
            }
        }

        [Usage("HideManagement")]
        [Description("Hides the Management GUI if enabled")]
        public static void HideManagement_OnCommand(CommandEventArgs args)
        {
            if (!Enabled)
            {
                args.Mobile.SendMessage(0x21, "Management GUI is not enabled");
                return;
            }

            if (m_MainForm != null)
            {
                m_MainForm.Visible = false;
            }
        }

        public static void CreateMainForm()
        {
            Console.Write("Initializing Server Management GUI... ");
            m_MainForm = new MainForm();

            m_PluginLoader = new PluginLoader("Server.Engines.WatchUO.Plugins");
            m_PluginLoader.LoadPlugins();
            m_PluginLoader.AppendPluginsTo(m_MainForm);

            m_Thread = new Thread(new ThreadStart(RunApp));
            m_Thread.Priority = ThreadPriority.BelowNormal;
            m_Thread.Start();
            Console.WriteLine(" Done.");
        }
    }
}