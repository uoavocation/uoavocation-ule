using System;
using Server;

namespace Server.Items
{
    public class Basket5WestArtifact : BaseDecorationContainerArtifact
    {
        public override int ArtifactRarity { get { return 2; } }

        [Constructable]
        public Basket5WestArtifact()
            : base(0x24DC)
        {
        }

        public Basket5WestArtifact(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.WriteEncodedInt(0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadEncodedInt();
        }
    }

    public class Basket5NorthArtifact : BaseDecorationContainerArtifact
    {
        public override int ArtifactRarity { get { return 2; } }

        [Constructable]
        public Basket5NorthArtifact()
            : base(0x24DB)
        {
        }

        public Basket5NorthArtifact(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.WriteEncodedInt(0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadEncodedInt();
        }
    }
}
