using System;
using System.Collections;
using Server;
using Server.Spells;
using Server.Mobiles;
using Server.Network;

namespace Server.Engines.Doom
{
    [TypeAlias("Server.Engines.Doom.LampRoomTelePorter")]
    public class LampRoomTeleporter : Item
    {
        public LampRoomTeleporter(int[] dat)
        {
            Hue = dat[1];
            ItemID = dat[0];
            Movable = false;
        }

        public override bool HandlesOnMovement { get { return true; } }
        public override bool OnMoveOver(Mobile m)
        {
            if (m != null && m is PlayerMobile)
            {
                if (SpellHelper.CheckCombat(m))
                {
                    m.SendLocalizedMessage(1005564, "", 0x22); // Wouldst thou flee during the heat of battle??				
                }
                else
                {
                    Server.Mobiles.BaseCreature.TeleportPets(m, LeverPuzzleController.lr_Exit, Map.Malas);
                    m.MoveToWorld(LeverPuzzleController.lr_Exit, Map.Malas);
                    return false;
                }
            }
            return true;
        }

        public LampRoomTeleporter(Serial serial)
            : base(serial)
        {
        }
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }
        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}