using System;
using Server;

namespace Server.Misc
{
	public static class TileDataOverride
	{
		public static void Initialize()
        {
            #region TileDataOverride Description
            /*
                Just a file overrides the TileData flags in the TileData.mul File
             
                This allows server owners the ability to keep TileData flags for
                custom artwork script-side which makes them private; thus people
                cannot use stolen artwork in-game as easily; especially if they
                do not know the TileData flags associated with the custom artwork
            */
            #endregion Edited By: A.A.S.R

            #region TileData Fix - Distro Client
            
            //NAME: Marble Wall | ID: 672
            //FLAG: NoShoot
            TileData.ItemTable[0x2A0].Flags |= TileFlag.NoShoot; 
            
            //NAME: Stone Wall | ID: 992
            //FLAG: NoShoot
			TileData.ItemTable[0x3E0].Flags |= TileFlag.NoShoot; 
			
			//NAME: Stone Wall | ID: 993
			//FLAG: NoShoot
			TileData.ItemTable[0x3E1].Flags |= TileFlag.NoShoot;
						
			//NAME: Water | ID: 13522
			//FLAG: Height
			TileData.ItemTable[0x34D2].Height = 0;
		
            #endregion Edited By: A.A.S.R

            #region TileData Update - Custom Art

            #endregion Edited By: A.A.S.R
        }
	}
}
