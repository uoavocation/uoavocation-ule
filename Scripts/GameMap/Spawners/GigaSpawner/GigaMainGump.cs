using System;
using System.Collections;
using Server;
using Server.Items;
using Server.Network;
using Server.Targeting;
using Server.GigaSpawners;

namespace Server.Gumps
{
	public class GigaMainGump : Gump
	{
		private GigaSpawner m_spawner;
		private AccessLevel l_al, g_al;
		private ArrayList All_List = new ArrayList(GSMain.GSHash.Values);
		
		public GigaMainGump( GigaSpawner gs, Mobile from ) : this(gs, AccessLevel.Player,  AccessLevel.Player, from)
		{
		}
		
		public GigaMainGump( GigaSpawner gs, AccessLevel local, AccessLevel global, Mobile from ) : base(0, 0)
		{
			if( gs == null )
				return;
			m_spawner = gs;
			if( local == AccessLevel.Player )
				l_al = gs.Accesslevel;
			else
				l_al = local;
			if( global == AccessLevel.Player )
				g_al = gs.GAccessLevel;
			else
				g_al = global;
			AddBackground(50, 50, 480, 370, 3000);
			AddBackground(60, 80, 460, 150, 3500);
			AddBackground(60, 235, 460, 110, 3500);
			AddHtml ( 50, 60, 480, 20, String.Format( "<center>GigaSpawner Main - {0}</center>", gs.Name ), false, false );
			AddLabel(115, 100, 0, "Spawn Entry Menu");
			AddLabel(290, 100, 0, "Options:");
			AddLabel(115, 145, 0, "Modified By List");
			AddLabel(330, 145, 0, "Goto Spawner");
			AddLabel(115, 190, 0, (gs.ShowSpawnRange ? "Hide" : "Show") + " Borders" );
			AddLabel(115, 260, 0, "List");
			AddLabel(255, 260, 0, "Load/Save");
			AddLabel(410, 260, 0, "Toggle Vis");
			AddLabel(445, 345, gs.Active ? 67 : 32,  gs.Active ? " Active" : "Inactive");
			AddButton( 80, 100, 4005, 4006,  1, GumpButtonType.Reply, 0);	// Spawn Entry
			AddButton(345, 100, 2225, 2225,  4, GumpButtonType.Reply, 0);	// Options 1
			AddButton(370, 100, 2226, 2226, 17, GumpButtonType.Reply, 0);	// Options 2
			AddButton( 80, 145, 4005, 4006,  3, GumpButtonType.Reply, 0);	// Mod by
			AddButton(290, 145, 4005, 4006,  8, GumpButtonType.Reply, 0);	// Goto
			AddButton( 80, 190, 4005, 4006, 12, GumpButtonType.Reply, 0);	// Toggle Borders
			AddButton( 80, 260, 4005, 4006,  6, GumpButtonType.Reply, 0);	// List
			AddButton(220, 260, 4005, 4006,  2, GumpButtonType.Reply, 0);	// Load/Save
			AddButton(375, 260, 4005, 4006,  5, GumpButtonType.Reply, 0);	// Toggle Vis
			if ( gs.SpawnedAmount > 0 )
			{
				if( !gs.InContainer )
				{
					AddLabel(335, 370, 0, "Bring to Home");
					AddButton(300, 370,  4005,  4006,  7, GumpButtonType.Reply, 0);// Home
				}
				AddLabel(225, 370, 0, "De-Spawn");
				AddButton(190, 370,  4005,  4006,  9, GumpButtonType.Reply, 0);// De-Spawn
			}
			if ( gs.Active )
			{
				AddLabel(115, 370, 0, "Re-Spawn");
				AddButton( 80, 370,  4005,  4006, 10, GumpButtonType.Reply, 0);// Re-Spawn
			}
			AddButton(455, 370, gs.Active ? 10830 : 10850, gs.Active ? 10850 : 10830, 11, GumpButtonType.Reply, 0);// Active
			AddBackground(390, 180, 110, 30, 2620);
			AddButton(370, 185, 253, 252, 14, GumpButtonType.Reply, 0);// Local AccessLevel Down
			AddLabel(400, 185, 80, l_al.ToString());
			if( from.AccessLevel == AccessLevel.Administrator )
			{
				AddBackground(390, 295, 110, 30, 2620);
				AddButton(370, 300, 253, 252, 16, GumpButtonType.Reply, 0);// Global AccessLevel Down
				AddLabel(400, 300, 80, g_al.ToString());
			}
			AddButton(80, 300, 4009, 4010, 18, GumpButtonType.Reply, 0);//Activate all GigaSpawners
			AddLabel(115, 300, 0, "Activate");
			AddButton(220, 300, 4003, 4004, 19, GumpButtonType.Reply, 0);//Deactivate all GigaSpawners
			AddLabel(255, 300, 0, "Deactivate");

			AddBackground(395, 71, 90, 30, 9200);
			AddLabel(400, 76, 1153, "Local Options");
			AddBackground(395, 227, 95, 30, 9200);
			AddLabel(400, 232, 1153, "Global Options");
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			if ( m_spawner.Deleted )
				return;
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			
			int myIndex = 0;
			int gotopage = -1;
			
			switch( info.ButtonID )
			{
				case 1:
					{
						//spawn entry
						from.SendGump( new SpawnEntryMenu(m_spawner) );
						break;
					}
				case 2:
					{
						//save load
						from.SendGump( new GigaSaveGump(from) );
						break;
					}
				case 3:
					{
						//mod by
						from.SendGump( new GigaModByGump(m_spawner) );
						break;
					}
				case 4:
					{
						//ops1
						from.SendGump( new GigaOpsGump(m_spawner) );
						break;
					}
				case 17:
				{
						//ops2
					from.SendGump( new GigaOpsGump2(m_spawner) );
					break;
				}
				case 5:
					{
						//toggle vis
						if ( m_spawner.ItemID == 7955 )
							GSMain.GigaVis();
						else
							GSMain.GigaNoVis();
						
						from.SendGump( new GigaMainGump(m_spawner,from) );
						break;
					}
				case 6:
					{
						//List
						try
						{
							myIndex = All_List.IndexOf( m_spawner );
							gotopage = myIndex / 15;
						}
						catch ( Exception err )
						{
							Console.WriteLine( err.ToString() );
						}
						from.SendGump( new GigaListGump( from, All_List, gotopage >= 0 ? gotopage : 0 ) );
						break;
					}
				case 7:
					{
						//bring home
						m_spawner.BringToHome();
						from.SendGump( new GigaMainGump(m_spawner,from) );
						break;
					}
				case 8:
					{
						//goto
						from.MoveToWorld(m_spawner.Location,m_spawner.Map);
						from.SendGump( new GigaMainGump(m_spawner,from) );
						break;
					}
				case 9:
					{
						//despawn
						m_spawner.DeSpawn();
						from.SendGump( new GigaMainGump(m_spawner,from) );
						break;
					}
				case 10:
					{
						//respawn
						m_spawner.ReSpawn();
						from.SendGump( new GigaMainGump(m_spawner,from) );
						break;
					}
				case 11:
					{
						//activate/deactivate
						if ( !m_spawner.Active )
						{
							GSMain.LogWrite(String.Format("Account: {0} Character: {1} has activated spawner '{2}' at {3} Map: {4}",from.Account, from, m_spawner.Name, m_spawner.Location, m_spawner.Map.ToString()),true,2);
							m_spawner.Start();
						}
						else
						{
							GSMain.LogWrite(String.Format("Account: {0} Character: {1} has de-activated spawner '{2}' at {3} Map: {4}",from.Account, from, m_spawner.Name, m_spawner.Location, m_spawner.Map.ToString()),true,2);
							m_spawner.Stop();
						}
						m_spawner.AddModifiedBy(from);
						from.SendGump( new GigaMainGump(m_spawner,from) );
						break;
					}
				case 12:
					{
						//toggle borders
						if ( m_spawner.ShowSpawnRange )
						{
							m_spawner.ShowSpawnRange = false;
						}
						else
						{
							m_spawner.ShowSpawnRange = true;
						}
						from.SendGump( new GigaMainGump(m_spawner,from) );
						break;
					}
				case 14:
				{
						//Local AccessLevel
					if ( l_al == AccessLevel.Player )
					{
						l_al = from.AccessLevel;
					}
					else
					{
						l_al--;
					}
					m_spawner.Accesslevel = l_al;
					from.SendGump( new GigaMainGump(m_spawner, l_al, g_al, from));
					break;
				}
				case 16:
				{
						//Global AccessLevel
					if ( g_al == AccessLevel.Player )
					{
						g_al = from.AccessLevel;
					}
					else
					{
						g_al--;
					}
					m_spawner.GAccessLevel = g_al;
					from.SendGump( new GigaMainGump(m_spawner, l_al, g_al, from));
					break;
				}
				case 18:
				{
					 //activate all spawners
					GSMain.LogWrite(String.Format("Account: {0} Character: {1} has activated all spawner.",from.Account, from ),true,2);
					GSMain.ToggleActive( true );
					from.SendGump( new GigaMainGump(m_spawner,from) );
					break;
				}
				case 19:
				{
					//deactivate all spawners
					GSMain.LogWrite(String.Format("Account: {0} Character: {1} has de-activated all spawner.",from.Account, from ),true,2);
					GSMain.ToggleActive( false );
					from.SendGump( new GigaMainGump(m_spawner,from) );
					break;
				}
			}
			if( m_spawner.Modified && GSMain.Mods )
			{
				GSMain.LogWrite(String.Format("Account: {0} Character: {1} modified the accesslevels of spawner '{2}' at {3} Map: {4}",from.Account, from, m_spawner.Name, m_spawner.Location, m_spawner.Map.ToString()),true,2);
				GSMain.LogWrite(String.Format("New:"),false,2);
				GSMain.LogWrite(String.Format("Spawner AccessLevel: {0} Global AccessLevel: {1}", l_al, g_al),false,2);
			}
			m_spawner.AddModifiedBy(from);
		}
	}
		public class GigaOpsGump2 : Gump
	{
		private GigaSpawner m_spawner;
		private Rectangle2D m_area = new Rectangle2D(0,0,0,0);
		private WayPoint m_wp;
		
		public GigaOpsGump2( GigaSpawner gs ) : this( gs, new Rectangle2D(0,0,0,0), null )
		{
		}
		
		public GigaOpsGump2( GigaSpawner gs, WayPoint wp ) : this( gs, new Rectangle2D(0,0,0,0), wp )
		{
		}
		
		public GigaOpsGump2(GigaSpawner gs, Rectangle2D area, WayPoint wp) : base(0, 0)
		{
			m_wp = wp;
			if( area.Equals(m_area) )
				area = gs.SpawnArea;
			m_area = area;
			m_spawner = gs;
			AddBackground(50, 50, 460, 355, 3000);
			AddBackground(60, 80, 440, 100, 3500);
			AddBackground(60, 175, 440, 130, 3500);
			AddBackground(60, 300, 440, 70, 3500);
			AddBackground(105, 225, 50, 20, 9300);
			AddBackground(185, 225, 50, 20, 9300);
			AddBackground(300, 225, 50, 20, 9300);
			AddBackground(410, 225, 50, 20, 9300);
			AddBackground(195, 260, 50, 20, 9300);
			AddBackground(345, 260, 50, 20, 9300);
			AddHtml( 50, 60, 440, 20, String.Format( "<center>GigaSpawner Options 2 - {0}</center>", gs.Name ), false, false );
			AddLabel(110, 100, 0, "Regular");
			AddLabel(245, 100, 0, "Facet Spawn");
			AddLabel(380, 100, 0, "Global Spawn");
			AddLabel(110, 135, 0, "Exclude from facet and global spawn");
			AddLabel(110, 195, 0, "Spawn Area");
			AddLabel(110, 260, 0, "Spawn Range");
			AddLabel( 85, 225, 0, "X");
			AddLabel(165, 225, 0, "Y");
			AddLabel(254, 225, 0, "Width");
			AddLabel(360, 225, 0, "Height");
			AddLabel(305, 195, 0, "Target Area?");
			AddLabel(305, 260, 0, "Team");
			AddLabel( 85, 325, 0, "Waypoint:");
			AddLabel(455,  55, 0, "Ops:");
			AddButton(400, 195, 247, 248, 1, GumpButtonType.Reply, 0);// Target Area
			AddButton(165, 325, 2460, 2461, 2, GumpButtonType.Reply, 0);// Add Waypoint
			AddButton(244, 325, 2463, 2463, 3, GumpButtonType.Reply, 0);// Delete Waypoint
			AddButton(260, 370, 238, 240, 6, GumpButtonType.Reply, 0);// Apply
			AddButton(340, 370, 247, 248, 4, GumpButtonType.Reply, 0);// OK
			AddButton(420, 370, 242, 241, 5, GumpButtonType.Reply, 0);// Cancel
			AddButton(485,  55, 2225, 2225, 7, GumpButtonType.Reply, 0);// Options 1
			AddTextEntry(105, 225, 45, 20, 0, 0, area.X.ToString());
			AddTextEntry(185, 225, 45, 20, 0, 1, area.Y.ToString());
			AddTextEntry(300, 225, 45, 20, 0, 2, area.Width.ToString());
			AddTextEntry(410, 225, 45, 20, 0, 3, area.Height.ToString());
			AddTextEntry(200, 260, 40, 20, 0, 4, gs.SpawnRange.ToString());
			AddTextEntry(345, 260, 45, 20, 0, 5, gs.Team.ToString());
			AddGroup(1);
			AddRadio( 85, 100, 210, 211, (!gs.FacetSpawn && !gs.GlobalSpawn), 5);
			AddRadio(355, 100, 210, 211, gs.GlobalSpawn, 2);
			AddRadio(220, 100, 210, 211, gs.FacetSpawn, 3);
			AddCheck( 85, 135, 210, 211, gs.IsExcluded, 4);
			AddGroup(2);
			AddRadio(85, 195, 210, 211, !gs.RangeOrArea, 0);
			AddRadio(85, 260, 210, 211, gs.RangeOrArea, 1);
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			if ( m_spawner.Deleted )
				return;
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			switch ( info.ButtonID )
			{
				case 0:
					{
						from.SendGump( new GigaMainGump( m_spawner, from ) );
						break;
					}
				case 1:
					{
						from.Target = new InternalTarget( m_spawner, m_area, m_wp, true, true );
						from.SendMessage("Select the first corner of the spawn area");
						break;
					}
				case 2:
					{
						from.Target = new InternalTarget( m_spawner, m_area, m_wp, false, true );
						from.SendMessage("Select a waypoint.");
						if ( m_spawner.ShowSpawnRange == true )
						{
							m_spawner.ShowSpawnRange = false;
						}
						break;
					}
				case 3:
					{
						m_wp = null;
						break;
					}
				case 4:
				case 6:
					{
						string[] ordered = new string[info.TextEntries.Length];
						foreach( TextRelay tr in info.TextEntries )
							ordered[tr.EntryID] = tr.Text;
						string oldvals = String.Format("SpawnArea: {0} SpawnRange: {1} Team: {2} GlobalSpawn: {3} FacetSpawn: {4} IsExcluded: {5}",m_spawner.SpawnArea.ToString(),m_spawner.SpawnRange.ToString(),m_spawner.Team.ToString(),m_spawner.GlobalSpawn,m_spawner.FacetSpawn,m_spawner.IsExcluded);
						try
						{
							int x, y, w, h;
							x = y = w = h = 0;
							m_spawner.RangeOrArea = info.IsSwitched(1);
							if( !m_spawner.RangeOrArea )
							{
								x = Math.Abs(Utility.ToInt32( ordered[0] ));
								y = Math.Abs(Utility.ToInt32( ordered[1] ));
								w = Math.Abs(Utility.ToInt32( ordered[2] ));
								h = Math.Abs(Utility.ToInt32( ordered[3] ));
								m_spawner.SpawnArea = new Rectangle2D(x,y,w,h);
							}
							else
								m_spawner.SpawnRange = Math.Abs(Utility.ToInt32( ordered[4] ));
							
							m_spawner.Team = Utility.ToInt32( ordered[5] );
						}
						catch
						{
							from.SendGump( new GigaOpsGump2( m_spawner, m_area, m_wp ) );
							from.SendMessage("Invalid values found. Check value and try again.");
						}
						m_spawner.WayPoint = m_wp;
						m_spawner.GlobalSpawn = info.IsSwitched(2);
						m_spawner.FacetSpawn = info.IsSwitched(3);
						m_spawner.IsExcluded = info.IsSwitched(4);
						if( m_spawner.Modified && GSMain.Mods )
						{
							GSMain.LogWrite(String.Format("Account: {0} Character: {1} has modified the following options in '{2}' at {3} Map: {4}", from.Account,from,m_spawner.Name,m_spawner.Location,m_spawner.Map.ToString()), true, 2);
							GSMain.LogWrite("Old:",false,2);
							GSMain.LogWrite(oldvals,false,2);
							GSMain.LogWrite("",false,2);
							GSMain.LogWrite("New:",false,2);
							GSMain.LogWrite(String.Format("SpawnArea: {0} SpawnRange: {1} Team: {2} GlobalSpawn: {3} FacetSpawn: {4} IsExcluded: {5}",m_spawner.SpawnArea.ToString(),m_spawner.SpawnRange.ToString(),m_spawner.Team.ToString(),m_spawner.GlobalSpawn,m_spawner.FacetSpawn,m_spawner.IsExcluded),false,2);
						}
						m_spawner.AddModifiedBy(from);
						if( info.ButtonID == 4 )
						from.SendGump( new GigaMainGump( m_spawner, from ) );
							else
						from.SendGump( new GigaOpsGump2( m_spawner, m_area, m_wp ) );
						break;
					}
					
				case 5:
				{
					from.SendGump( new GigaMainGump( m_spawner, from ) );
					break;
				}
				case 7:
					{
						from.SendGump( new GigaOpsGump(m_spawner) );
						break;
					}
			}
		}
		
		private class InternalTarget : Target
		{
			private GigaSpawner m_gs;
			private Rectangle2D m_area;
			private WayPoint m_wp;
			private bool m_IsArea;
			private bool m_FirstCorner;
			
			public InternalTarget( GigaSpawner gs, Rectangle2D area, WayPoint wp, bool IsArea, bool FirstCorner ) : base( 15, true, TargetFlags.None )
			{
				m_gs = gs;
				m_area = area;
				m_wp = wp;
				m_IsArea = IsArea;
				m_FirstCorner = FirstCorner;
			}
			
			protected override void OnTarget( Mobile from, object targeted )
			{
				if( !m_IsArea && (targeted is WayPoint) )
				{
					m_wp = targeted as WayPoint;
					from.SendGump( new GigaOpsGump2( m_gs, m_area, m_wp ) );
					return;
				}
				else if( m_IsArea && (targeted is IPoint2D) && m_FirstCorner )
				{
					Point2D p = new Point2D( (IPoint2D)targeted );
					m_area.X = p.X;
					m_area.Y = p.Y;
					from.Target = new InternalTarget( m_gs, m_area, m_wp, m_IsArea, false );
					from.SendMessage("Select the second corner of the spawn area");
					return;
				}
				else if( m_IsArea && (targeted is IPoint2D) )
				{
					Point2D p = new Point2D( (IPoint2D)targeted );
					int x,y,x1,y1;
					x = y = x1 = y1 = 0;
					if ( p.X < m_area.X )
					{
						x = p.X;
						x1 = m_area.X;
					}
					else
					{
						x1 = p.X;
						x = m_area.X;
					}
					if( p.Y < m_area.Y )
					{
						y = p.Y;
						y1 = m_area.Y;
					}
					else
					{
						y1 = p.Y;
						y = m_area.Y;
					}
					m_area = new Rectangle2D( new Point2D(x,y), new Point2D(x1,y1) );
					from.SendGump( new GigaOpsGump2( m_gs, m_area, m_wp ) );
					return;
				}
				
				if ( !m_IsArea )
					from.SendMessage("You must select a waypoint.");
			}
		}
	}
	public class GigaOpsGump : Gump
	{
		private GigaSpawner m_spawner;
		public GigaOpsGump( GigaSpawner gs ) : base(0, 0)
		{
			if( gs == null )
				return;
			m_spawner = gs;
			AddBackground( 50,  50, 440, 410, 3000);
			AddBackground( 60,  80, 415, 105, 3500);
			AddBackground( 60, 180, 415, 130, 3500);
			AddBackground( 60, 305, 415,  60, 3500);
			AddBackground( 60, 365, 415,  60, 3500);
			AddBackground(130, 385, 150,  20, 9300);
			AddBackground(375, 385,  70,  20, 9300);
			AddBackground(145, 235, 300,  20, 9300);
			AddBackground(260, 200,  50,  20, 9300);
			AddBackground(260, 270,  50,  20, 9300);
			AddBackground(147, 107, 305,  50, 9300);
			AddHtml ( 50, 60, 440, 20, String.Format( "<center>GigaSpawner Options 1 - {0}</center>", gs.Name ), false, false );
			AddLabel( 77, 107, 0, "Min Delay:");
			AddLabel( 77, 137, 0, "Max Delay:");
			AddLabel(177, 107, 0, "Days");
			AddLabel(242, 107, 0, "Hours");
			AddLabel(317, 107, 0, "Minutes");
			AddLabel(402, 107, 0, "Seconds");
			AddLabel(177, 137, 0, "Days");
			AddLabel(242, 137, 0, "Hours");
			AddLabel(317, 137, 0, "Minutes");
			AddLabel(402, 137, 0, "Seconds");
			AddLabel(110, 200, 0, "Speech Spawn");
			AddLabel( 85, 235, 0, "Keyword:");
			AddLabel(215, 200, 0, "Range:");
			AddLabel(360, 200, 0, "Case Sensitive");
			AddLabel(110, 270, 0, "InRange Spawn");
			AddLabel(215, 270, 0, "Range:");
			AddLabel(105, 325, 0, "Group Spawn");
			AddLabel(220, 325, 0, "Linear Progression");
			AddLabel(355, 325, 0, "Activity Scaled");
			AddLabel(435,  55, 0, "Ops:");
			AddLabel( 85, 385, 0, "Name:");
			AddLabel( 295, 385, 0, "Home Range:");
			AddCheck( 85, 200, 210, 211, gs.SpeechTriggered, 1);
			AddCheck( 85, 270, 210, 211, gs.RangeTriggered, 2);
			AddCheck( 85, 325, 210, 211, gs.Group, 3);
			AddCheck(200, 325, 210, 211, gs.LinearProgression, 4);
			AddCheck(335, 325, 210, 211, gs.ActivityScaled, 6);
			AddCheck(335, 200, 210, 211, gs.CaseSensitive, 5);
			AddButton(465, 55, 2226, 2226, 1, GumpButtonType.Reply, 0);// Options 2
			AddButton(250, 425,  238,  240, 2, GumpButtonType.Reply, 0);// Apply
			AddButton(325, 425,  247,  248, 3, GumpButtonType.Reply, 0);// OK
			AddButton(400, 425,  242,  241, 4, GumpButtonType.Reply, 0);// Cancel
			AddTextEntry(147, 107,  30, 20, 0,  0, gs.MinDelay.Days.ToString());
			AddTextEntry(212, 107,  30, 20, 0,  1, gs.MinDelay.Hours.ToString());
			AddTextEntry(282, 107,  30, 20, 0,  2, gs.MinDelay.Minutes.ToString());
			AddTextEntry(372, 107,  30, 20, 0,  3, gs.MinDelay.Seconds.ToString());
			AddTextEntry(147, 137,  30, 20, 0,  4, gs.MaxDelay.Days.ToString());
			AddTextEntry(212, 137,  30, 20, 0,  5, gs.MaxDelay.Hours.ToString());
			AddTextEntry(282, 137,  30, 20, 0,  6, gs.MaxDelay.Minutes.ToString());
			AddTextEntry(372, 137,  30, 20, 0,  7, gs.MaxDelay.Seconds.ToString());
			AddTextEntry(145, 235, 295, 20, 0,  8, gs.Keyword );
			AddTextEntry(260, 200,  50, 20, 0,  9, gs.SPTriggerRange.ToString() );
			AddTextEntry(260, 270,  50, 20, 0, 10, gs.WalkTriggerRange.ToString() );
			AddTextEntry(130, 385, 150, 20, 0, 11, gs.Name );
			AddTextEntry(375, 385,  70, 20, 0, 12, gs.HomeRange.ToString() );
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			if ( m_spawner.Deleted )
				return;
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			if( info.ButtonID == 0 || info.ButtonID == 4 )
			{
				from.SendGump(new GigaMainGump( m_spawner, from ) );
				return;
			}
			if( info.ButtonID == 1 )
			{
				from.SendGump(new GigaOpsGump2(m_spawner) );
				return;
			}
			string[] order = new string[info.TextEntries.Length];
			for (int i=0; i < info.TextEntries.Length; i++)
				order[info.TextEntries[i].EntryID] = info.TextEntries[i].Text;
			string[] oldvals = new string[]
			{
				String.Format("MinDelay: {0} Days {1} Hours {2} Minutes {3} Seconds", m_spawner.MinDelay.Days, m_spawner.MinDelay.Hours, m_spawner.MinDelay.Minutes, m_spawner.MinDelay.Seconds),
				String.Format("MinDelay: {0} Days {1} Hours {2} Minutes {3} Seconds", m_spawner.MaxDelay.Days, m_spawner.MaxDelay.Hours, m_spawner.MaxDelay.Minutes, m_spawner.MaxDelay.Seconds),
				String.Format("Speech Triggered: {0} Keyword: {1} Range: {2} Case Sensitive: {3}", m_spawner.SpeechTriggered, m_spawner.Keyword, m_spawner.SPTriggerRange.ToString(), m_spawner.CaseSensitive ),
				String.Format("Walk Triggered: {0} Range: {1}", m_spawner.RangeTriggered, m_spawner.WalkTriggerRange.ToString() ),
				String.Format("Name: {0} Group: {1} Linear Progression: {2} Activity Scaled {3}", m_spawner.Name, m_spawner.Group, m_spawner.LinearProgression, m_spawner.ActivityScaled )
			};
			try
			{
				m_spawner.MinDelay = new TimeSpan( Math.Abs(Utility.ToInt32( order[0] )),Math.Abs(Utility.ToInt32( order[1] )),Math.Abs(Utility.ToInt32( order[2] )),Math.Abs(Utility.ToInt32( order[3] )));
				m_spawner.MaxDelay = new TimeSpan( Math.Abs(Utility.ToInt32( order[4] )),Math.Abs(Utility.ToInt32( order[5] )),Math.Abs(Utility.ToInt32( order[6] )),Math.Abs(Utility.ToInt32( order[7] )));
			}
			catch
			{
				from.SendMessage( "Invalid value in MinDelay or MaxDelay. Please verify values." );
			}
			
			m_spawner.Keyword = order[8];
			try
			{
				m_spawner.SPTriggerRange = Math.Abs(Utility.ToInt32( order[9] ));
			}
			catch
			{
				from.SendMessage("Ivalid speech trigger range value.");
			}
			try
			{
				m_spawner.WalkTriggerRange = Math.Abs(Utility.ToInt32( order[10] ));
			}
			catch
			{
				from.SendMessage("Ivalid walk trigger range value.");
			}
			m_spawner.Name = order[11];
			try
			{
				m_spawner.HomeRange = Utility.ToInt32( order[12] );
			}
			catch
			{
				from.SendMessage("Ivalid home range value.");
			}
			m_spawner.SpeechTriggered = info.IsSwitched(1);
			m_spawner.RangeTriggered = info.IsSwitched(2);
			m_spawner.Group = info.IsSwitched(3);
			m_spawner.LinearProgression = info.IsSwitched(4);
			m_spawner.CaseSensitive = info.IsSwitched(5);
			m_spawner.ActivityScaled = info.IsSwitched(6);
			if( m_spawner.Modified && GSMain.Mods )
			{
				GSMain.LogWrite(String.Format("Account: {0} Character: {1} has modified the following options in '{2}' at {3} Map: {4}", from.Account,from,m_spawner.Name,m_spawner.Location,m_spawner.Map.ToString()), true, 2);
				GSMain.LogWrite("Old:",false,2);
				foreach(string s in oldvals)
					GSMain.LogWrite(s,false,2);
				GSMain.LogWrite("",false,2);
				GSMain.LogWrite("New:",false,2);
				GSMain.LogWrite(String.Format("MinDelay: {0} Days {1} Hours {2} Minutes {3} Seconds", m_spawner.MinDelay.Days, m_spawner.MinDelay.Hours, m_spawner.MinDelay.Minutes, m_spawner.MinDelay.Seconds),false,2);
				GSMain.LogWrite(String.Format("MinDelay: {0} Days {1} Hours {2} Minutes {3} Seconds", m_spawner.MaxDelay.Days, m_spawner.MaxDelay.Hours, m_spawner.MaxDelay.Minutes, m_spawner.MaxDelay.Seconds),false,2);
				GSMain.LogWrite(String.Format("Speech Triggered: {0} Keyword: {1} Range: {2} Case Sensitive: {3}", m_spawner.SpeechTriggered, m_spawner.Keyword, m_spawner.SPTriggerRange.ToString(), m_spawner.CaseSensitive),false,2);
				GSMain.LogWrite(String.Format("Walk Triggered: {0} Range: {1}", m_spawner.RangeTriggered, m_spawner.WalkTriggerRange.ToString() ),false,2);
				GSMain.LogWrite(String.Format("Name: {0} Group: {1} Linear Progression: {2} Activity Scaled {3}", m_spawner.Name, m_spawner.Group, m_spawner.LinearProgression, m_spawner.ActivityScaled),false,2);
			}
			m_spawner.AddModifiedBy(from);
			if( info.ButtonID == 2 )
			{
				from.SendGump(new GigaOpsGump(m_spawner) );
				return;
			}
			
			from.SendGump(new GigaMainGump(m_spawner,from) );
		}
	}


}
