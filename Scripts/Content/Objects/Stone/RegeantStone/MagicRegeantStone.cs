using System;
using Server.Items;

namespace Server.Items
{
	public class RegStone_All : Item
	{
		public override string DefaultName
		{
			get { return "reagent stone"; }
		}

		[Constructable]
		public RegStone_All() : base( 0xED4 )
		{
			Movable = false;
			Hue = 0x2D1;
		}

		public override void OnDoubleClick( Mobile from )
		{
            BagOfAllReagents regBag = new BagOfAllReagents(50);

			if ( !from.AddToBackpack( regBag ) )
				regBag.Delete();
		}

		public RegStone_All( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}