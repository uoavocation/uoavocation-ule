using System;
using Server.Mobiles;

namespace Server.Items
{
	public class SolenDirtSpawn : BaseTrap
	{
		private bool m_IsBlackSolens;
		private int m_SpawnChance;
		private int m_AngerChance;
		private int m_MaxSpawn;
		private int m_MinSpawn;
		private int m_WarriorChance;
		private bool m_PlayerOnly;
		private TimeSpan m_ResetDelay;
		
		[CommandProperty( AccessLevel.GameMaster )]
		public bool IsBlackSolens {
			get { return m_IsBlackSolens; }
			set { m_IsBlackSolens = value; InvalidateProperties(); }
		}
		
		[CommandProperty( AccessLevel.GameMaster )]
		public int SpawnChance {
			get { return m_SpawnChance; }
			set {
				if(value < 0) value = 0;
				if(value > 100) value = 100;
				m_SpawnChance = value;
				InvalidateProperties();
			}
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int AngerChance {
			get { return m_AngerChance; }
			set {
				if(value < 0) value = 0;
				if(value > 100) value = 100;
				m_AngerChance = value;
				InvalidateProperties();
			}
		}
		
		[CommandProperty( AccessLevel.GameMaster )]
		public int MaxSpawn {
			get { return m_MaxSpawn; }
			set {
				if(value < m_MinSpawn) value = m_MinSpawn;
				m_MaxSpawn = value;	
				InvalidateProperties();
			}
		}
		
		[CommandProperty( AccessLevel.GameMaster )]
		public int MinSpawn {
			get { return m_MinSpawn; }
			set {
				if(value > m_MaxSpawn) value = m_MaxSpawn;
				m_MinSpawn = value;
				InvalidateProperties();
			}
		}
		
		[CommandProperty( AccessLevel.GameMaster )]
		public int WarriorChance {
			get { return m_WarriorChance; }
			set {
				if(value < 0) value = 0;
				if(value > 100) value = 100;
				m_WarriorChance = value;
				InvalidateProperties();
			}
		}
		
		[CommandProperty(AccessLevel.GameMaster)]
		public bool PlayerOnly {
			get {return m_PlayerOnly;}
			set { m_PlayerOnly = value; InvalidateProperties(); }
		}

		[CommandProperty(AccessLevel.GameMaster)]
		public TimeSpan DelayReset {
			get {return m_ResetDelay; }
			set { m_ResetDelay = value; InvalidateProperties();	}
		}
		
		[Constructable]
		public SolenDirtSpawn() : base( 0x914 ) {
			m_IsBlackSolens = true;
			m_SpawnChance = 50;
			m_AngerChance = 75;
			m_MaxSpawn = 4;
			m_MinSpawn = 2;
			m_WarriorChance = 10;
			m_PlayerOnly = true;
			m_ResetDelay = TimeSpan.Zero;
		}

		public override TimeSpan ResetDelay{ 
			get{ return m_ResetDelay; }
		}
			
		public override void OnTrigger( Mobile from )
		{
			int i, antCount;
			BaseCreature newAnt;
			
			if (m_PlayerOnly && (from.AccessLevel > AccessLevel.Player ))
				return;
			if ( from.Alive && CheckRange( from.Location, 0 ) && from is PlayerMobile ) {
				// Check chance of spawn
				if(Utility.Random(100) < m_SpawnChance) {
					from.SendMessage("You have angered the ants!!!");
					// Spawn min-max ants
					antCount = Utility.Random(m_MinSpawn, (m_MaxSpawn - m_MinSpawn));
					for(i=0;i<antCount;i++) {
						if(Utility.Random(100) < m_WarriorChance) {
							if(m_IsBlackSolens) {
								newAnt = new BlackSolenWarrior();
								newAnt.MoveToWorld(this.Location, from.Map);
							} else {
								newAnt = new RedSolenWarrior();
								newAnt.MoveToWorld(this.Location, from.Map);
							}
						} else {
							if(m_IsBlackSolens) {
								newAnt = new BlackSolenWorker();
								newAnt.MoveToWorld(this.Location, from.Map);
							} else {
								newAnt = new RedSolenWorker();
								newAnt.MoveToWorld(this.Location, from.Map);
							}
						}
						if(Utility.Random(100) < m_AngerChance) {
							//newAnt.ForceReaquire();
                            newAnt.AggressiveAction(from);
						}
					}
				}
			}
		}

		public SolenDirtSpawn( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write((int)  0 ); // version
			writer.Write((bool) m_IsBlackSolens);
			writer.Write((int)  m_SpawnChance);
			writer.Write((int)  m_AngerChance);
			writer.Write((int)  m_MaxSpawn);
			writer.Write((int)  m_MinSpawn);
			writer.Write((int)  m_WarriorChance);
			writer.Write((bool) m_PlayerOnly);
			writer.Write((TimeSpan) m_ResetDelay);
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
			m_IsBlackSolens = reader.ReadBool();
			m_SpawnChance = reader.ReadInt();
			m_AngerChance = reader.ReadInt();
			m_MaxSpawn = reader.ReadInt();
			m_MinSpawn = reader.ReadInt();
			m_WarriorChance = reader.ReadInt();
			m_PlayerOnly = reader.ReadBool();
			m_ResetDelay = reader.ReadTimeSpan();
		}
	}
}
