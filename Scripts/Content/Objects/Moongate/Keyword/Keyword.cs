using System; 
using Server; 

namespace Server.Items
{ 
	public class KeywordMoongate : Item
	{ 

		private string s_Pwd; // The passphrase itself
		private int i_Range;  // how far can you be to trigger the teleport
		private Point3D m_Target; // Teleport destination

		[CommandProperty( AccessLevel.GameMaster )]
		public Point3D Target
		{
			get
			{
				return m_Target;
			}
			set
			{
				m_Target = value;
			}
		}
	
		[CommandProperty( AccessLevel.GameMaster )]
		public string Password
		{
			get
			{
				return s_Pwd;
			}
			set
			{
				s_Pwd = value;
			}
		}
	
		[CommandProperty( AccessLevel.GameMaster )]
		public int Range
		{
			get
			{
				return i_Range;
			}
			set
			{
				i_Range = value;
			}
		}
	

		[Constructable]
		public KeywordMoongate() : base( 0xF6C ) 
		{
			Name = "A Moongate";
			Movable = false;
			s_Pwd = "password";
			i_Range = 0;
			Hue = 962;
		} 

		public KeywordMoongate( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 
			writer.Write(s_Pwd);
			writer.Write(i_Range);
			writer.Write( (int) 0 ); 
		} 
       
		public override void Deserialize(GenericReader reader) 
		{ 
			base.Deserialize( reader ); 
			s_Pwd = reader.ReadString();
			i_Range = reader.ReadInt();
			int version = reader.ReadInt(); 
		}

		public override bool HandlesOnSpeech
		{
			get{ return true;}
		}

		public override void OnSpeech( SpeechEventArgs e ) 
		{ 
			base.OnSpeech(e);
			Mobile from = e.Mobile;

			if (from.InRange( this, i_Range ) && (e.Speech==s_Pwd))
			{
				from.MoveToWorld( m_Target, Map.Felucca );
				from.PlaySound( 0x1FE );
			}
		}

	} 
}