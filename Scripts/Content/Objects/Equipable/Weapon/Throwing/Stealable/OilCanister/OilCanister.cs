using System;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Targeting;
using System.Collections;
using Server.Spells;

namespace OilCanister
{
    public class OilCanister : Item
    {
        private DateTime lastused = DateTime.Now;
        private TimeSpan delay = TimeSpan.FromSeconds(5);

        [Constructable]
        public OilCanister() : this(1)
        {
        }

        [Constructable]
        public OilCanister(int amount): base(0x1C18)
        {
            Hue = 0;
            Weight = 4;
            Stackable = true;
            Amount = amount;
            Name = "canister of oil"; InvalidateProperties();
        }

        public override void OnDoubleClick(Mobile from)
        {
            if (!CanThrow(from))
                return;

            if (lastused + delay > DateTime.Now)
            {
                from.SendMessage("You must wait a few seconds before doing that.");
                return;
            }
            else
            {
                lastused = DateTime.Now;
            }

            if (!IsChildOf(from.Backpack)) 
            {
                from.SendLocalizedMessage(1042001); // That must be in your pack for you to use it.
            }
            else
            {
                from.Target = new OilTarget(this); 
            }
        }

        public bool CanThrow(Mobile m)
        {
            if (m.Frozen)
                return false;
            if (m.Paralyzed)
                return false;

            return true;
        }

        public OilCanister( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
    }

    public class OilTarget : Target
    {
        private OilCanister oil;

        public OilTarget(OilCanister canister)
            : base(10, true, TargetFlags.None)
        {
            oil = canister;
        }

        protected override void OnTarget(Mobile from, object o)
        {
            if (!from.CanSee(o))
                return;

            IPoint3D loc = o as IPoint3D;

            SpellHelper.GetSurfaceTop(ref loc);

            Map map = from.Map;

            IEntity to;

            if (o is Mobile)
                to = (Mobile)o;
            else
                to = new Entity(Serial.Zero, new Point3D(loc), map);

            Effects.SendMovingEffect(from, to, oil.ItemID & 0x3FFF, 7, 0, false, false, oil.Hue, 0);

            OilCanisterMoveTimer tmr = new OilCanisterMoveTimer(from, oil, new Point3D(loc), map);
            tmr.Start();

            from.RevealingAction();

            from.SendMessage("You quickly spark and toss the canister.");
            from.Say("fire in the hole!");
        }
    }

    public class OilCanisterMoveTimer : Timer
    {
        private Mobile from;
        private OilCanister oil;
        private Point3D loc;
        private Map map;

        public OilCanisterMoveTimer(Mobile m, OilCanister canister, Point3D p, Map _map)
            : base(TimeSpan.FromSeconds(1.5))
        {
            from = m;
            oil = canister;
            loc = p;
            map = _map;
        }

        protected override void OnTick()
        {
            if (oil.Amount > 1)
            {
                oil.Amount -= 1;
                OilCanister oil2 = new OilCanister();
                oil2.MoveToWorld(new Point3D(loc), map);

                OilCanisterDetonateTimer tmr = new OilCanisterDetonateTimer(oil2, from, loc, 0);
                tmr.Start();
                this.Stop();
            }
            else
            {
                oil.MoveToWorld(new Point3D(loc), map);

                OilCanisterDetonateTimer tmr = new OilCanisterDetonateTimer(oil, from, loc, 0);
                tmr.Start();
                this.Stop();
            }
        }
    }

    public class OilCanisterDetonateTimer : Timer
    {
        private OilCanister oil;
        private Mobile from;
        private Point3D loc;

        private ArrayList targets;

        private int count;

        public OilCanisterDetonateTimer(OilCanister canister, Mobile f, Point3D p, int c)
            : base(TimeSpan.FromSeconds(1))
        {
            oil = canister;
            from = f;
            loc = p;
            count = c;
        }

        protected override void OnTick()
        {
            int seconds = count;
            seconds += 1;

            Map map = from.Map;

            oil.PublicOverheadMessage(0, 39, false, ""+seconds.ToString());

            IPooledEnumerable mobs = map.GetMobilesInRange(loc, 5);

            targets = new ArrayList();

            foreach (Mobile m in mobs)
            {
                if (SpellHelper.ValidIndirectTarget(from, m) && from.CanBeHarmful(m, false))
                {
                    targets.Add(m);
                }
            }

            if (seconds == 4)
            {
                TempItem it = new TempItem(0x1C1D);
                it.MoveToWorld(oil.Location, oil.Map);

                I_Delete tmr = new I_Delete(it, 0);
                tmr.Start();

                from.PlaySound(0x307);

                for (int i = 0; i < targets.Count; ++i)
                {
                    Mobile _m = (Mobile)targets[i];

                    int damage = Utility.Random(11, 22);
                    if (_m.Location == oil.Location)

//-----------------------------------------------------------------Shadows Edge-----------------------//

                        damage += 10;

//----------------------------------------------------------------------------------------------------//

                    if (damage >= _m.Hits)
                        _m.Kill();
                    else
                        _m.Hits -= damage;

                    from.DoHarmful(_m);

                    if(Core.AOS)
                        _m.PublicOverheadMessage(0, 39, false, "" + damage.ToString());

                    _m.PlaySound(0x307);
                }

                oil.Delete();

                this.Stop();
            }
            else
            {
                OilCanisterDetonateTimer tmr = new OilCanisterDetonateTimer(oil, from, loc, seconds);
                tmr.Start();
                this.Stop();
            }

        }
    }

    public class TempItem : Item
    {
        [Constructable]
        public TempItem(int itemid): base(itemid)
        {
        }

        public TempItem(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
    }

    public class I_Delete : Timer
    {
        private Item item;
        private int count;

        public I_Delete(Item i, int c)
            : base(TimeSpan.FromSeconds(0.1))
        {
            item = i;
            count = c;
        }

        protected override void OnTick()
        {
            if (item == null)
            {
                this.Stop();
                return;
            }

            int id = count;
            id += 1;

            if (id == 1)
            {
                TempItem i = new TempItem(0x1C1E);
                i.MoveToWorld(item.Location, item.Map);
                I_Delete tmr = new I_Delete(i, id);
                tmr.Start();
                this.Stop();
            }

            if (id == 2)
            {
                TempItem i = new TempItem(0x1C1F);
                i.MoveToWorld(item.Location, item.Map);
                I_Delete tmr = new I_Delete(i, id);
                tmr.Start();
                this.Stop();
            }

            item.Delete();
            this.Stop();
        }
    }
}