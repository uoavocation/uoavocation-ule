using System;
using System.Text;
using Server;
using Server.Network;
using Server.GigaSpawners;

namespace Server.Gumps
{
	public class SpawnEntryEditor : Gump
	{
		private GigaSpawner m_gs;
		private int m_entryID;
		public SpawnEntryEditor( GigaSpawner gs ) : this( gs, "", -1, null )
		{
		}
		
		public SpawnEntryEditor( GigaSpawner gs, string spawnentry, int entryID) : this( gs, spawnentry, entryID, null )
		{
		}
		
		public SpawnEntryEditor( GigaSpawner gs, string spawnentry, int entryID, string[] errmsgs ) : base( 0, 0 )
		{
			m_entryID = entryID;
			if( gs == null || gs.Deleted )
				return;
			m_gs = gs;
			if(errmsgs == null)
			{
				string oldentry = spawnentry;
				m_gs.VerifyEntries( ref spawnentry, m_entryID, m_gs.InContainer, out errmsgs );
				if( spawnentry != oldentry )
					spawnentry = oldentry;
			}
			AddBackground(50, 50, 550, 540, 3000);
			AddHtml ( 50, 60, 550, 20, String.Format( "<center>Spawn Entry Editor - {0}</center>", m_gs.Name ), false, false );
			AddLabel(70, 120, (errmsgs.Length > 0 ? (errmsgs[0] == "" || errmsgs[0] == null ? 0 : 32) : 0), "Entry 1 :");
			AddLabel(70, 145, (errmsgs.Length > 1 ? (errmsgs[1] == "" || errmsgs[1] == null ? 0 : 32) : 0), "Entry 2 :");
			AddLabel(70, 170, (errmsgs.Length > 2 ? (errmsgs[2] == "" || errmsgs[2] == null ? 0 : 32) : 0), "Entry 3 :");
			AddLabel(70, 195, (errmsgs.Length > 3 ? (errmsgs[3] == "" || errmsgs[3] == null ? 0 : 32) : 0), "Entry 4 :");
			AddLabel(70, 220, (errmsgs.Length > 4 ? (errmsgs[4] == "" || errmsgs[4] == null ? 0 : 32) : 0), "Entry 5 :");
			AddLabel(70, 245, (errmsgs.Length > 5 ? (errmsgs[5] == "" || errmsgs[5] == null ? 0 : 32) : 0), "Entry 6 :");
			AddLabel(70, 270, (errmsgs.Length > 6 ? (errmsgs[6] == "" || errmsgs[6] == null ? 0 : 32) : 0), "Entry 7 :");
			AddLabel(70, 295, (errmsgs.Length > 7 ? (errmsgs[7] == "" || errmsgs[7] == null ? 0 : 32) : 0), "Entry 8 :");
			AddLabel(70, 320, (errmsgs.Length > 8 ? (errmsgs[8] == "" || errmsgs[8] == null ? 0 : 32) : 0), "Entry 9 :");
			AddLabel(70, 345, (errmsgs.Length > 9 ? (errmsgs[9] == "" || errmsgs[9] == null ? 0 : 32) : 0), "Entry 10:");
			AddLabel(204, 90, 0, "Type");
			AddLabel(390, 90, 0, "Params");
			AddLabel(520, 90, 0, "Chance");
			string[] types, args, chance;
			double verch = 0;
			SplitEntry( spawnentry, out types, out args, out chance );
			for( int i = 0; i < 10; i++ )
			{
				AddBackground(130, 120 + 25 * i, 200, 20, 9300);
				AddBackground(355, 120 + 25 * i, 130, 20, 9300);
				AddBackground(510, 120 + 25 * i, 60, 20, 9300);
				if( types != null && i < types.Length)
				{
					AddTextEntry(130, 120 + 25 * i, 200, 20, 0, i, types[i]);
					AddTextEntry(355, 120 + 25 * i, 130, 20, 0, i+10, args[i]);
					AddTextEntry(510, 120 + 25 * i, 60, 20, 0, i+20, chance[i]);
					verch += Utility.ToDouble(chance[i]);
				}
				else
				{
					AddTextEntry(130, 120 + 25 * i, 200, 20, 0, i, "");
					AddTextEntry(355, 120 + 25 * i, 130, 20, 0, i+10, "");
					AddTextEntry(510, 120 + 25 * i, 60, 20, 0, i+20, "");
				}
			}
			StringBuilder sb = new StringBuilder();
			AddLabel(70, 380, 0, "For chance to work correctly it must add up to exactly 1.0.");
			if( verch > 0 && verch != 1.0 )
			{
				sb.Append(String.Format("Chance must add up to exactly 1.0. You are off by {0} \n",(1.0 - verch).ToString()));
				AddLabel(510, 380, 32, verch.ToString());
			}
			AddLabel(70, 400, 0, "Spawn Entry Line:");
			string se = spawnentry;
			if( spawnentry.Length > 62 )
				se = spawnentry.Substring(0,62);
			AddLabel(190, 400, 0, se);
			if( spawnentry.Length > 62 )
			{
				se = spawnentry.Substring(63);
				AddLabel(70, 415, 0, se);
			}
			foreach( string s in errmsgs )
			{
				if( s != null )
				{
					if ( s.Trim() != "" )
						sb.Append(s + "\n");
				}
			}
			AddHtml( 70, 435, 490, 100, sb.ToString().TrimEnd('\n'), true, true);
			AddButton(350, 555, 238, 240, 1, GumpButtonType.Reply, 0);// Apply
			AddButton(425, 555, 247, 248, 2, GumpButtonType.Reply, 0);// OK
			AddButton(500, 555, 242, 241, 3, GumpButtonType.Reply, 0);// Cancel
		}
		
		private void SplitEntry( string spawnentry, out string[] types, out string[] args, out string[] chance )
		{
			string[] temp, temp2;
			temp = null; temp2 = null; types = null; args = null; chance = null;
			string holder = "";
			if( spawnentry != "" )
			{
				temp = spawnentry.Split(';');
				types = new string[temp.Length];
				args = new string[temp.Length];
				chance = new string[temp.Length];
				for( int i = 0; i < temp.Length; i++ )
				{
					temp2 = temp[i].Split('?');
					holder = temp2[0];
					if( temp2.Length > 1 )
						chance[i] = temp2[1];
					else
						chance[i] = "";
					temp2 = holder.Split(':');
					holder = temp2[0];
					if( temp2.Length > 1 )
						args[i] = temp2[1];
					else
						args[i] = "";
					types[i] = holder;
				}
			}
		}
		
		private string BuildEntry( string[] te )
		{
			StringBuilder sb = new StringBuilder();
			string[] types,args,chance;
			types = new string[10];
			args = new string[10];
			chance = new string[10];
			for( int i=0; i < te.Length; i++ )
			{
				if( i < 10 )
					types[i] = te[i];
				if( 9 < i && i < 20 )
					args[i - 10] = te[i];
				if( 19 < i && i < 30 )
					chance[i - 20] = te[i];
			}
			for( int i = 0; i < types.Length; i++ )
			{
				if( types[i] != "" )
				{
					sb.Append( types[i] );
					if( args[i] != "" )
						sb.Append( ":" + args[i] );
					if( chance[i] != "" )
						sb.Append( "?" + chance[i] );
					sb.Append( ";" );
				}
			}
			return sb.ToString().TrimEnd(';').ToLower();
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			if( info.ButtonID == 3 || info.ButtonID == 0 )
			{
				from.SendGump( new SpawnEntryMenu( m_gs ) );
				return;
			}
			
			string[] ordered = new string[30];
			foreach( TextRelay tr in info.TextEntries )
				ordered[tr.EntryID] = tr.Text;
			string entry = BuildEntry(ordered);
			string[] ErrMsgs = new string[10];
			string oldentry = entry;
			int bid = 1;
			if( m_gs.VerifyEntries( ref entry, m_entryID, m_gs.InContainer, out ErrMsgs ))
			{
				bid = info.ButtonID;
				if ( (m_entryID == -1 || m_entryID >= m_gs.SpawnList.Length) && entry != "" )
				{
					m_gs.AddSpawnEntry( entry, 0 );
					oldentry = " ";
				}
				else if( entry != "" )
				{
					oldentry = m_gs.SpawnList[m_entryID];
					m_gs.SpawnList[m_entryID] = entry;
				}
				if( oldentry != entry )
					GSMain.LogWrite(String.Format("Account: {0} Character: {1} modified spawn entry in '{2}' at {3} Map: {4} from '{5}' to '{6}'",from.Account, from, m_gs.Name, m_gs.Location, m_gs.Map.ToString(), oldentry, entry),true,2);
				oldentry = entry;
				m_gs.AddModifiedBy(from);
			}
			if( bid == 1 )
			{
				from.SendGump( new SpawnEntryEditor( m_gs, oldentry, m_entryID, ErrMsgs ) );
				return;
			}
			if( bid == 2 )
			{
				from.SendGump( new SpawnEntryMenu( m_gs ) );
				return;
			}
		}
	}
}

