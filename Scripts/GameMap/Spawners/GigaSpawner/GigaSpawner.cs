using System;
using System.Text;
using System.Reflection;
using System.Collections;
using Server;
using Server.Commands;
using Server.Items;
using Server.Mobiles;
using Server.Gumps;

namespace Server.GigaSpawners
{
	[Flags]
	public enum SpawnerFlags
	{
		None				= 0x0000,
		FacetSpawn			= 0x0001,
		GlobalSpawn			= 0x0002,
		Active				= 0x0004,
		IsExcluded			= 0x0008,
		Modified			= 0x0010,
		LinearProgression	= 0x0020,
		SpeechTriggered		= 0x0040,
		RangeTriggered		= 0x0080,
		Group				= 0x0100,
		CaseSensitive		= 0x0200,
		ShowSpawnRange		= 0x0400,
		InvalidEntries		= 0x0800,
		OnlyOne				= 0x1000,
		ActivityScaled		= 0x2000,
		RangeOrArea			= 0x4000
	}
	
	public class GigaSpawner : Item
	{
		#region Spawn Groups
		// Please submit your spawngroups the more complete it is the better for all
		private static Hashtable MobileSpawnGroups = new Hashtable();
		private static Hashtable ItemSpawnGroups = new Hashtable();
		public static void Initialize()
		{
			// To add more groups just use the following format:
			// SpawnGroups.Add( "nameofgroup", "typename1;typename2;typename3...etc" )
			// NOTE* nameofgroup must be all lower case and with no spaces
			// To spawn them just type the groupname in the spawnentry line
			MobileSpawnGroups.Add( "townanimals", "dog;cat;rat;bird" );
			MobileSpawnGroups.Add( "farmanimals", "cow;bull;chicken;boar;pig;rabbit;sheep;goat" );
			MobileSpawnGroups.Add( "forestanimals", "brownbear;blackbear;cougar;greathart;greywolf;grizzlybear;hind;horse;jackrabbit;llama;mountaingoat;panther;eagle;bird;snake" );
			MobileSpawnGroups.Add( "swampanimals", "alligator;snake;giantserpent;TropicalBird");
			MobileSpawnGroups.Add( "undead1", "Bogle;Shade;Ghoul;Skeleton;Zombie;headlessone" );
			MobileSpawnGroups.Add( "undead2", "BoneMagi;Spectre;Wraith;BoneKnight;SkeletalKnight" );
			MobileSpawnGroups.Add( "undead3", "Lich;Mummy" );
			MobileSpawnGroups.Add( "undead4", "LichLord;ShadowKnight;RottingCorpse" );
			MobileSpawnGroups.Add( "undead5", "AncientLich;DarknightCreeper;SkeletalDragon" );
			MobileSpawnGroups.Add( "undeadall", "AncientLich;Bogle;BoneMagi;Lich;LichLord;Shade;Spectre;Wraith;BoneKnight;Ghoul;Mummy;SkeletalKnight;Skeleton;Zombie;ShadowKnight;DarknightCreeper;RottingCorpse;SkeletalDragon" );
			MobileSpawnGroups.Add( "orcs1", "Orc;OrcCaptain" );
			MobileSpawnGroups.Add( "orcs2", "OrcishMage;OrcBomber" );
			MobileSpawnGroups.Add( "orcs3", "OrcishLord;OrcBrute" );
			MobileSpawnGroups.Add( "orcsall", "Orc;OrcishMage;OrcishLord;OrcBrute;OrcBomber;OrcCaptain" );
			MobileSpawnGroups.Add( "ogres", "Ogre;OgreLord");
			MobileSpawnGroups.Add( "giants", "Troll;Cyclops;Titan" );
			MobileSpawnGroups.Add( "articanimals", "icesnake;walrus;polarbear;snowleopard;whitewolf");
			MobileSpawnGroups.Add( "artic1", "icesnake;frostooze;frostspider");
			MobileSpawnGroups.Add( "artic2", "iceserpent;frosttroll;snowelemental;iceelemental");
			MobileSpawnGroups.Add( "artic3", "ArcticOgreLord;iceelemental");
			MobileSpawnGroups.Add( "articall", "ArcticOgreLord;iceelemental;icefiend;iceserpent;icesnake;snowelemental;frostooze;frostspider;frosttroll");
			MobileSpawnGroups.Add( "ophidians", "ophidianarchmage;ophidianknight;ophidianmage;ophidianmatriarch;ophidianwarrior");
			MobileSpawnGroups.Add( "terathan", "terathanavenger;terathandrone;terathanmatriarch;terathanwarrior");
			MobileSpawnGroups.Add( "blacksolen", "blacksoleninfiltratorqueen;blacksoleninfiltratorwarrior;blacksolenqueen;blacksolenwarrior;blacksolenworker" );
			MobileSpawnGroups.Add( "redsolen", "redsoleninfiltratorqueen;redsoleninfiltratorwarrior;redsolenqueen;redsolenwarrior;redsolenworker" );
			MobileSpawnGroups.Add( "watermob", "waterelemental;seaserpent;dolphin" );
			MobileSpawnGroups.Add( "healers", "EvilHealer;EvilWanderingHealer;Healer;WanderingHealer" );
			MobileSpawnGroups.Add( "ratmen", "ratman;ratmanarcher;ratmanmage" );
			MobileSpawnGroups.Add( "savages", "savageshaman;savage;savagerider" );
			MobileSpawnGroups.Add( "meer", "meereternal;meercaptain;meerwarrior;meermage" );
			MobileSpawnGroups.Add( "juka", "jukalord;jukawarrior;jukamage" );
			MobileSpawnGroups.Add( "lavamobs", "lavalizard;lavasnake;lavaserpent" );
			
			#region Nerun Groups
			MobileSpawnGroups.Add( "bears", "brownbear;grizzlybear;blackbear" );
			MobileSpawnGroups.Add( "wolves", "direwolf;timberwolf;greywolf" );
			MobileSpawnGroups.Add( "cows", "bull;cow" );
			MobileSpawnGroups.Add( "roden", "rabbit;jackrabbit" );
			MobileSpawnGroups.Add( "felin", "cougar;panther" );
			MobileSpawnGroups.Add( "misc", "goat;mountaingoat;greathart;hind;sheep;boar;llama;snake;pig" );
			MobileSpawnGroups.Add( "birds", "bird;eagle" );
			MobileSpawnGroups.Add( "mount", "horse;ridablellama" );
			#endregion
			
			ItemSpawnGroups.Add( "regs", "BlackPearl;Bloodmoss;Garlic;Ginseng;MandrakeRoot;Nightshade;SulfurousAsh;SpidersSilk" );
			ItemSpawnGroups.Add( "necroregs", "BatWing;GraveDust;DaemonBlood;NoxCrystal;PigIron" );
			ItemSpawnGroups.Add( "allregs", "BlackPearl;Bloodmoss;Garlic;Ginseng;MandrakeRoot;Nightshade;SulfurousAsh;SpidersSilk;BatWing;GraveDust;DaemonBlood;NoxCrystal;PigIron" );
			ItemSpawnGroups.Add( "gems", "Amber;Amethyst;Citrine;Diamond;Emerald;Ruby;Sapphire;StarSapphire;Tourmaline" );
			ItemSpawnGroups.Add( "fruits", "banana;bananas;SplitCoconut;Lemon;Lime;Coconut;Dates;Grapes;Peach;Pear;Apple;Watermelon;Squash;Cantaloupe" );
			ItemSpawnGroups.Add( "vegetables", "Carrot;Cabbage;Onion;Lettuce;Pumpkin" );
			ItemSpawnGroups.Add( "platearmor", "platearms;platechest;plategloves;plategorget;platelegs;platehelm;heatershield" );
			ItemSpawnGroups.Add( "chainarmor", "chainchest;chainlegs" );
			ItemSpawnGroups.Add( "ringarmor", "ringmailarms;ringmailchest;ringmail;gloves;ringmaillegs" );
			ItemSpawnGroups.Add( "rangerarmor", "rangerarms;rangerchest;rangergloves;rangergorget;rangerlegs" );
		}
		#endregion
		
		#region Private vars
		private const int ActLimit = 5;// Sets the amount of ticks before activity scalar drops down by 1
		private string[] m_SpawnList, m_ItemsList;
		private Rectangle2D m_SpawnArea;
		private string m_Creator, m_Keyword, m_UID;
		private SpawnerFlags m_Flags;
		private Hashtable m_ModifiedBy = new Hashtable();
		private Hashtable m_SpawnedList = new Hashtable();
		private int m_HomeRange, m_Team, m_SPTriggerRange, m_WalkTriggerRange, m_SpawnRange, m_InActivityCount;
		private int m_spawnprogress = 0;
		private int[] m_SpawnAmount = new int[]{0};
		private int[] m_SpawnedAmount = new int[]{0};
		private Timer m_Timer;
		private TimeSpan m_MinDelay, m_MaxDelay;
		private AccessLevel m_Accesslevel;
		private static AccessLevel m_GAccessLevel;
		private DateTime m_End;
		private InternalTimer SpawnTimer;
		private WayPoint m_WayPoint;
		private ArrayList boundry = new ArrayList();
		#endregion
		
		#region Public vars
		public string[] ItemsList{ get{ return m_ItemsList; } set{ m_ItemsList = value; }}
		public string[] SpawnList{ get{ return m_SpawnList; } set{ m_SpawnList = value; }}
		
		public string UID{ get{ return m_UID; } set{ m_UID = value; } }
		
		public WayPoint WayPoint
		{
			get{ return m_WayPoint; }
			set
			{
				if ( m_WayPoint != value )
					Modified = true;
				m_WayPoint = value;
			}
		}
		
		public Rectangle2D SpawnArea
		{
			get{ return m_SpawnArea; }
			set
			{
				if( !m_SpawnArea.Equals(value) )
					Modified = true;
				m_SpawnArea = value;
				ResetBoundry();
			}
		}
		
		public int[] SpawnAmount{ get{ return m_SpawnAmount; } set{ m_SpawnAmount = value; }}
		
		public SpawnerFlags Flags
		{
			get{ return m_Flags; }
			set{ m_Flags = value; }
		}
		
		public TimeSpan NextSpawn
		{
			get
			{
				if ( Active )
				{
					TimeSpan ts = m_End - DateTime.Now;
					if( ts < TimeSpan.Zero )
						DoTimer();
					return ts;
				}
				else
					return TimeSpan.FromSeconds( 0 );
			}
		}
		
		public bool FacetSpawn
		{
			get{ return GetFlag( SpawnerFlags.FacetSpawn ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.FacetSpawn))
					Modified = true;
				SetFlag( SpawnerFlags.FacetSpawn, value );
				if (value)
					GlobalSpawn = false;
			}
		}
		
		public bool GlobalSpawn
		{
			get{ return GetFlag( SpawnerFlags.GlobalSpawn ); }
			set
			{
				if (value != GetFlag( SpawnerFlags.GlobalSpawn ))
					Modified = true;
				SetFlag( SpawnerFlags.GlobalSpawn, value );
				if (value)
					FacetSpawn = false;
			}
		}
		
		public bool IsExcluded
		{
			get{ return GetFlag( SpawnerFlags.IsExcluded ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.IsExcluded ))
					Modified = true;
				SetFlag( SpawnerFlags.IsExcluded, value );
			}
		}
		
		public bool LinearProgression
		{
			get{ return GetFlag( SpawnerFlags.LinearProgression ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.LinearProgression ))
					Modified = true;
				SetFlag( SpawnerFlags.LinearProgression, value );
			}
		}
		
		public bool ActivityScaled
		{
			get{ return GetFlag( SpawnerFlags.ActivityScaled ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.ActivityScaled ))
					Modified = true;
				SetFlag( SpawnerFlags.ActivityScaled, value );
			}
		}
		
		public bool Group
		{
			get{ return GetFlag( SpawnerFlags.Group ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.Group ))
					Modified = true;
				SetFlag( SpawnerFlags.Group, value );
			}
		}
		
		public bool RangeOrArea
		{
			get{ return GetFlag( SpawnerFlags.RangeOrArea ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.RangeOrArea ))
					Modified = true;
				SetFlag( SpawnerFlags.RangeOrArea, value );
			}
		}
		
		public bool OnlyOne
		{
			get{ return GetFlag( SpawnerFlags.OnlyOne ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.OnlyOne ))
					Modified = true;
				SetFlag( SpawnerFlags.OnlyOne, value );
			}
		}
		
		public bool SpeechTriggered
		{
			get{ return GetFlag( SpawnerFlags.SpeechTriggered ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.SpeechTriggered ))
					Modified = true;
				SetFlag( SpawnerFlags.SpeechTriggered, value );
			}
		}
		
		public bool CaseSensitive
		{
			get{ return GetFlag( SpawnerFlags.CaseSensitive ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.CaseSensitive))
					Modified = true;
				SetFlag( SpawnerFlags.CaseSensitive, value );
			}
		}
		
		public bool RangeTriggered
		{
			get{ return GetFlag( SpawnerFlags.RangeTriggered ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.RangeTriggered ))
					Modified = true;
				SetFlag( SpawnerFlags.RangeTriggered, value );
			}
		}
		
		public int SPTriggerRange
		{
			get{ return m_SPTriggerRange; }
			set
			{
				if (m_SPTriggerRange != value)
					Modified = true;
				m_SPTriggerRange = value;
				InvalidateProperties();
			}
		}
		
		public int WalkTriggerRange
		{
			get{ return m_WalkTriggerRange; }
			set
			{
				if (m_WalkTriggerRange != value)
					Modified = true;
				m_WalkTriggerRange = value;
				InvalidateProperties();
			}
		}
		
		public int Team
		{
			get{ return m_Team; }
			set
			{
				if ( m_Team != value )
					Modified = true;
				m_Team = value;
				InvalidateProperties();
			}
		}
		
		public string Keyword
		{
			get{ return m_Keyword; }
			set
			{
				if (m_Keyword != value)
					Modified = true;
				m_Keyword = value;
				InvalidateProperties();
			}
		}
		
		public Hashtable ModifiedBy{ get{ return m_ModifiedBy; } set{ m_ModifiedBy = value; }}
		
		public int HomeRange
		{
			get{ return m_HomeRange; }
			set
			{
				if (m_HomeRange != value)
					Modified = true;
				m_HomeRange = value;
				InvalidateProperties();
			}
		}
		
		public int SpawnRange
		{
			get{ return m_SpawnRange; }
			set
			{
				if (m_SpawnRange != value)
				{
					Modified = true;
				}
				m_SpawnRange = value;
				FixBoundries();
				InvalidateProperties();
			}
		}
		
		public AccessLevel Accesslevel
		{
			get{ return m_Accesslevel; }
			set
			{
				if (m_Accesslevel != value)
					Modified = true;
				m_Accesslevel = value;
			}
		}
		
		public AccessLevel GAccessLevel
		{
			get{ return m_GAccessLevel; }
			set
			{
				if (m_GAccessLevel != value)
					Modified = true;
				m_GAccessLevel = value;
			}
		}
		
		public TimeSpan MinDelay
		{
			get { return m_MinDelay; }
			set
			{
				if(m_MinDelay != value)
					Modified = true;
				m_MinDelay = value;
				InvalidateProperties();
			}
		}
		
		public TimeSpan MaxDelay
		{
			get { return m_MaxDelay; }
			set
			{
				if(m_MaxDelay != value)
					Modified = true;
				m_MaxDelay = value;
				InvalidateProperties();
			}
		}
		
		public bool Active
		{
			get{ return GetFlag( SpawnerFlags.Active ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.Active ))
					Modified = true;
				SetFlag( SpawnerFlags.Active, value );
				if ( value )
					Start();
				else
					Stop();
			}
		}
		
		public bool Modified
		{
			get{ return GetFlag( SpawnerFlags.Modified ); }
			set{ SetFlag( SpawnerFlags.Modified, value ); }
		}
		
		public bool InvalidEntries
		{
			get{ return GetFlag( SpawnerFlags.InvalidEntries ); }
			set{ SetFlag( SpawnerFlags.InvalidEntries, value ); }
		}
		
		public bool ShowSpawnRange
		{
			get{ return GetFlag( SpawnerFlags.ShowSpawnRange ); }
			set
			{
				if( value != GetFlag( SpawnerFlags.ShowSpawnRange))
					Modified = true;
				SetFlag( SpawnerFlags.ShowSpawnRange, value );
				ResetBoundry();
			}
		}
		#endregion
		
		#region Read-Only Properties
 
        public bool InContainer { get{ return Parent != null; } }
		public string Creator{ get{ return m_Creator; } }
		public int SpawnedAmount{ get{ return m_SpawnedList.Count; } }
		public int TotalAmount
		{
			get
			{
				int tot = 0;
				if ( LinearProgression || ActivityScaled )
					return m_SpawnAmount[m_spawnprogress];
				foreach( int am in m_SpawnAmount )
					tot += am;
				return tot;
			}
		}

        #region Unreachable Code Detected //Console Warning

        public override bool HandlesOnMovement 
        { 
            get 
            {
                InvalidateProperties(); 
                return RangeTriggered;             
            } 
        }
        public override bool HandlesOnSpeech 
        { 
            get 
            {
                InvalidateProperties();
                return SpeechTriggered; 
            } 
        }

        #endregion Unreachable Code Detected //Console Warning

		#endregion
		
		[Constructable]
		public GigaSpawner() : base ( 0x1f13 )
		{
			Name = "Giga Spawner";
			Visible = false;
			m_Accesslevel = AccessLevel.GameMaster;
			m_GAccessLevel = AccessLevel.GameMaster;
			InitSpawner();
		}
		
		public void InitSpawner()
		{
			Movable = false;
			RangeOrArea = true;//True = Range - False = Area
			m_HomeRange = 10;
			m_SpawnRange = 10;
			m_SPTriggerRange = 10;
			m_WalkTriggerRange = 5;
			m_Team = 0;
			m_SpawnList = new string[]{ "" };
			m_ItemsList = new string[]{ "" };
			m_MinDelay = TimeSpan.FromMinutes(5);
			m_MaxDelay = TimeSpan.FromMinutes(10);
			GSMain.Add(this);
			m_InActivityCount = 0;
		}
		
		public bool GetFlag( SpawnerFlags flag )
		{
			return ( (m_Flags & flag) != 0 );
		}
		
		public void SetFlag( SpawnerFlags flag, bool value )
		{
			if ( value )
				m_Flags |= flag;
			else
				m_Flags &= ~flag;
			InvalidateProperties();
		}
		
		private void FixAmount()
		{
			ArrayList toDelM = new ArrayList();
			ArrayList toDelI = new ArrayList();
			CleanHashTables(m_SpawnedList);
			foreach( Object o in m_SpawnedList.Keys )
			{
				if( o is Mobile )
				{
					Mobile mob = (Mobile)o;
					if( mob.Combatant == null && mob.HitsMax == mob.Hits )
						toDelM.Add(mob);
				}
				if( o is Item )
				{
					Item item = (Item)o;
					toDelI.Add(item);
				}
			}
			
			if( LinearProgression && ( toDelI.Count + toDelM.Count == SpawnedAmount ) )
			{
				ReSpawn();
				return;
			}
			else if( LinearProgression )
			{
				m_InActivityCount = ActLimit - 1;
				m_spawnprogress++;
				return;
			}
			int i = 0;
			while( i < toDelI.Count && SpawnedAmount > TotalAmount )
			{
				((Item)toDelI[i]).Delete();
				m_SpawnedList.Remove(toDelI[i]);
				i++;
			}
			i = 0;
			while( i < toDelM.Count && SpawnedAmount > TotalAmount )
			{
				((Mobile)toDelM[i]).Delete();
				m_SpawnedList.Remove(toDelM[i]);
				i++;
			}
			if( SpawnedAmount > TotalAmount )
			{
				m_InActivityCount = ActLimit - 1;//If we can't find things to delete try again next tick
				m_spawnprogress++;//reset progress since we couldn't clean up
			}
		}
		
		private void ReCalculateSA()
		{
			ArrayList toDel = new ArrayList();
			foreach( DictionaryEntry de in m_SpawnedList )
			{
				if( (int)de.Value >= m_SpawnedAmount.Length )
					toDel.Add(de.Key);
				else
					m_SpawnedAmount[(int)de.Value]++;
			}
			if( toDel.Count > 0 )
			{
				for( int i = 0; i < toDel.Count; i++ )
				{
					if( toDel[i] is Mobile )
					{
						Mobile del = (Mobile)toDel[i];
						m_SpawnedList.Remove(del);
						del.Delete();
					}
					else
					{
						Item del = (Item)toDel[i];
						m_SpawnedList.Remove(del);
						del.Delete();
					}
				}
			}
		}
		
		public bool IsFull()
		{
			CleanHashTables(m_SpawnedList);
			return ( TotalAmount <= m_SpawnedList.Count );
		}
		
		#region Boundry methods
		private void ResetBoundry()
		{
			DeleteBoundry();
			if ( ShowSpawnRange == true )
				ShowBoundry();
		}
		
		private void ShowBoundry()
		{
			if( Map == Map.Internal || Map == null )
				return;
			if( boundry == null )
				boundry = new ArrayList();
			else
				boundry.Clear();
			
			for( int x=1; x < m_SpawnArea.Width + 1; x++ )
			{
				BoundryWall item = new BoundryWall(GSMain.itemidx);
				item.Visible = false;
				item.Movable = false;
				item.Name = "Spawner Boundry";
				item.MoveToWorld( new Point3D( m_SpawnArea.X + x, m_SpawnArea.Y, Z), Map );
				boundry.Add(item);
				item = new BoundryWall(GSMain.itemidx);
				item.Visible = false;
				item.Movable = false;
				item.Name = "Spawner Boundry";
				item.MoveToWorld( new Point3D( m_SpawnArea.X + x, m_SpawnArea.Y + m_SpawnArea.Height, Z), Map );
				boundry.Add(item);
			}
			for( int y=1; y < m_SpawnArea.Height + 1; y++ )
			{
				BoundryWall item = new BoundryWall(GSMain.itemidy);
				item.Visible = false;
				item.Movable = false;
				item.Name = "Spawner Boundry";
				item.MoveToWorld( new Point3D( m_SpawnArea.X, m_SpawnArea.Y + y, Z), Map );
				boundry.Add(item);
				item = new BoundryWall(GSMain.itemidy);
				item.Visible = false;
				item.Movable = false;
				item.Name = "Spawner Boundry";
				item.MoveToWorld( new Point3D( m_SpawnArea.X + m_SpawnArea.Width, m_SpawnArea.Y + y, Z), Map );
				boundry.Add(item);
			}
		}
		
		private void DeleteBoundry()
		{
			if ( boundry == null )
				return;
			Item[] items = (Item[])boundry.ToArray(typeof(Item));
			for( int i=0; i < items.Length; i++)
				items[i].Delete();
			boundry.Clear();
		}
		
		private void FixBoundries()
		{
			if( RangeOrArea )
				SpawnArea = new Rectangle2D( new Point2D(X - m_SpawnRange, Y - m_SpawnRange), new Point2D( X + m_SpawnRange, Y + m_SpawnRange ) );
		}
		
		private class BoundryWall: Item
		{
			public BoundryWall(int itemid): base(itemid)
			{
			}
			public BoundryWall( Serial serial ) : base( serial )
			{
			}
			public override void Serialize( GenericWriter writer )
			{
			}
			public override void Deserialize( GenericReader reader )
			{
				Delete();
			}
		}
		#endregion
		
		#region Overrides
		public override bool IsAccessibleTo( Mobile check )
		{
			return (check.AccessLevel >= m_Accesslevel && check.AccessLevel >= m_GAccessLevel);
		}
		
		public override void OnSpeech( SpeechEventArgs e )
		{
			if ( !SpeechTriggered || !Active || m_End > DateTime.Now )
				return;
			if ( m_Keyword == null || m_Keyword == "" )
				return;
			if ( e.Mobile.InRange( this, m_SPTriggerRange ) && ((!IsFull() && !Group) || (Group && m_SpawnedList.Count == 0)) )
			{
				if ( CaseSensitive && e.Speech.IndexOf( m_Keyword ) != -1 )
				{
					if( Group )
						FullSpawn();
					else
						Spawn();
					DoTimer();
				}
				else if ( !CaseSensitive && e.Speech.ToLower().IndexOf( m_Keyword.ToLower() ) != -1 )
				{
					if( Group )
						FullSpawn();
					else
						Spawn();
					DoTimer();
				}
			}
		}
		
		public override void OnMovement( Mobile m, Point3D oldLocation )
		{
			if ( !RangeTriggered || !Active || !(m is PlayerMobile) || m_End > DateTime.Now )
				return;
			
			if ( m.InRange( this, m_WalkTriggerRange ) && ((!IsFull() && !Group) || (Group && m_SpawnedList.Count == 0)) )
			{
				if( Group )
					FullSpawn();
				else
					Spawn();
				DoTimer();
			}
		}
		
		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );
			
			if ( Active )
			{
				list.Add( 1060742 ); // active
				
				list.Add( 1060656, TotalAmount.ToString() ); // amount to make: ~1_val~
				list.Add( 1061169, m_HomeRange.ToString() ); // range ~1_val~
				if( GlobalSpawn )
					list.Add( 1060658, "Global Spawn\t{0}", GlobalSpawn ); // ~1_val~: ~2_val~
				if( FacetSpawn )
					list.Add( 1060658, "Facet Spawn\t{0}", FacetSpawn ); // ~1_val~: ~2_val~
				list.Add( 1060659, "team\t{0}", m_Team ); // ~1_val~: ~2_val~
				list.Add( 1060660, "speed\t{0} to {1}", m_MinDelay, m_MaxDelay ); // ~1_val~: ~2_val~
				list.Add( 1060847, "Next Spawn In:\t{0:00}:{1:00}:{2:00}", NextSpawn.Hours, NextSpawn.Minutes, NextSpawn.Seconds ); // ~1_val~ ~2_val~
				if( IsExcluded )
					list.Add( 1060661, "IsExcluded\t {0}", IsExcluded ); // ~1_val~: ~2_val~
				if( SpeechTriggered )
					list.Add( 1060662, "Keyword\t\"{0}\" Range: {1}", Keyword, SPTriggerRange.ToString() ); // ~1_val~: ~2_val~
				if( RangeTriggered )
					list.Add( 1060663, "Walk Range\t {0}", WalkTriggerRange ); // ~1_val~: ~2_val~
			}
			else
			{
				list.Add( 1060743 ); // inactive
			}
		}
		
		public override void OnSingleClick( Mobile from )
		{
			base.OnSingleClick( from );
			
			if ( Active )
				LabelTo( from, "[Running]" );
			else
				LabelTo( from, "[Off]" );
		}
		
		public override void OnDoubleClick( Mobile from )
		{
			if ( m_Creator == null )
			{
				m_Creator = from.Name;// Creator is the first to double click it
				AddModifiedBy(from);
			}
			else
			{
				CleanHashTables(m_ModifiedBy); //Get rid of entries by deleted mobiles to avoid problems and keep the hash small
			}
			GSMain.GigaGumpClose( from );
			from.SendGump( new GigaMainGump(this,from) );
		}
		
		public override void OnDelete()
		{
			DeSpawn();
			GSMain.Remove(this);
			if ( m_Timer != null )
			{
				m_Timer.Stop();
				m_Timer = null;
			}
			DeleteBoundry();
			base.OnDelete();
		}
		
		public override bool VerifyMove( Mobile from )
		{
			return IsAccessibleTo(from);
		}
		
		public override void OnMapChange()
		{
			base.OnMapChange();
			if( RangeOrArea && Map != Map.Internal )
				FixBoundries();
		}
		#endregion
		
		#region Timer Related
		public void Start()
		{
			if ( !Active )
			{
				if ( m_SpawnList[0] != "" )
				{
					Active = true;
					DoTimer();
				}
			}
		}
		
		public void Stop()
		{
			if( Active )
			{
				m_Timer.Stop();
				m_Timer = null;
				Active = false;
				m_spawnprogress = 0;
				DeSpawn();
				m_End = DateTime.Now;
			}
		}
		
		public void DoTimer()
		{
			if ( !Active )
				return;
			
			int minSeconds = (int)m_MinDelay.TotalSeconds;
			int maxSeconds = (int)m_MaxDelay.TotalSeconds;
			
			TimeSpan delay = TimeSpan.FromSeconds( Utility.RandomMinMax( minSeconds, maxSeconds ) );
			m_End = DateTime.Now + delay;
			
			DoTimer( delay );
		}
		
		public void DoTimer( TimeSpan delay )
		{
			if ( m_SpawnList[0] == "" )
			{
				Stop();
				return;
			}
			if ( m_Timer != null )
				m_Timer.Stop();
			
			m_Timer = new InternalTimer( this, delay );
			m_Timer.Start();
		}
		
		private class InternalTimer : Timer
		{
			private GigaSpawner m_Spawner;
			
			public InternalTimer( GigaSpawner spawner, TimeSpan delay ) : base( delay )
			{
				Priority = Timer.ComputePriority( delay + TimeSpan.FromSeconds(1));
				
				m_Spawner = spawner;
			}
			
			protected override void OnTick()
			{
				if ( m_Spawner != null )
					if ( !m_Spawner.Deleted )
					m_Spawner.OnTick();
			}
		}
		
		public void OnTick()
		{
			CleanHashTables(m_SpawnedList);
			if( m_InActivityCount != ActLimit && IsFull() )
				m_InActivityCount++;
			else if ( m_InActivityCount == ActLimit && IsFull() )
			{
				if( m_spawnprogress != 0 )
				{
					m_spawnprogress--;
					m_InActivityCount = 0;
					FixAmount();
				}
			}
			else
				m_InActivityCount = 0;
			if( SpeechTriggered || RangeTriggered )
				return;
			if( (Group || InContainer) && SpawnedAmount == 0 )
				FullSpawn();
			else
				Spawn();
			DoTimer();
		}
		#endregion
		
		#region Hashtables methods
		public void CleanHashTables( Hashtable hash )
		{
			if ( hash == null )
				return;
			if ( hash.Count == 0 )
				return;
			
			ArrayList list = new ArrayList(hash.Keys);
			for (int i=0; i<list.Count;i++)
			{
				if (list[i] is Mobile)
				{
					if ( ((Mobile)list[i]).Deleted )
					{
						if ( hash == m_SpawnedList )
							m_SpawnedAmount[(int)hash[list[i]]]--;
						hash.Remove(list[i]);
					}
					else if ( list[i] is BaseCreature )
					{
						if ( ((BaseCreature)list[i]).Controlled || ((BaseCreature)list[i]).IsStabled )
						{
							if ( hash == m_SpawnedList )
							{
								m_SpawnedAmount[(int)hash[list[i]]]--;
								hash.Remove(list[i]);
							}
						}
					}
				}
				else if (list[i] is Item )
				{
					
					if ( ((Item)list[i]).Deleted || ((Item)list[i]).Parent != Parent )
					{
						if ( hash == m_SpawnedList )
							m_SpawnedAmount[(int)hash[list[i]]]--;
						hash.Remove(list[i]);
					}
				}
			}
			
			if( hash == m_SpawnedList && (LinearProgression || ActivityScaled) && m_SpawnedList.Count == 0 )
			{
				if ( m_spawnprogress >= m_SpawnList.Length - 1 && LinearProgression )
					m_spawnprogress = 0;
				else if ( m_spawnprogress < m_SpawnList.Length - 1 )
				{
					m_spawnprogress++;
					m_InActivityCount = 0;
				}
			}
			if( hash == m_SpawnedList )
			{
				for( int i=0; i < m_SpawnedAmount.Length; i++ )
				{
					if( m_SpawnedAmount[i] > SpawnedAmount )
						m_SpawnedAmount[i] = SpawnedAmount;
				}
			}
			InvalidateProperties();
		}
		
		public void AddModifiedBy( Mobile from )
		{
			if ( from == null || !Modified )
				return;
			
			if ( m_ModifiedBy == null )
			{
				m_ModifiedBy = new Hashtable();
				m_ModifiedBy.Add(from, "1," + DateTime.Now.ToString());
			}
			else
			{
				string[] temp;
				int i;
				if (m_ModifiedBy.ContainsKey(from))
				{
					try
					{
						temp = ((string)m_ModifiedBy[from]).Split(',');
						i = Utility.ToInt32(temp[0]) + 1;
						m_ModifiedBy[from] = i.ToString() + "," + DateTime.Now.ToString();
					}
					catch (Exception err)
					{
						GSMain.LogWrite(err.Message,true);
					}
				}
				else
					m_ModifiedBy.Add(from, "1," + DateTime.Now.ToString());
			}
			Modified = false;
		}
		
		public void AddToSpawned( object spawned, int index )
		{
			if ( spawned == null )
				return;
			if ( m_SpawnedList == null )
			{
				m_SpawnedList = new Hashtable();
				m_SpawnedAmount[index]++;
				m_SpawnedList.Add(spawned, index);
			}
			else
			{
				m_SpawnedAmount[index]++;
				m_SpawnedList.Add(spawned, index);
			}
		}
		#endregion
		
		#region Spawn Lists methods
		public void AddItemEntry( string se )
		{
			if( se == "" )
				return;
			string[] nse = new string[m_ItemsList.Length + 1];
			for( int i = 0; i < m_ItemsList.Length; i++ )
				nse[i] = m_ItemsList[i];
			nse[m_ItemsList.Length] = se;
			NewItemsList(nse);
		}
		
		public void NewItemsList( string[] se )
		{
			ArrayList tempst = new ArrayList();
			int i = 0;
			for( i = 0; i < se.Length; i++ )
			{
				if ( VerifyEntries( ref se[i], i, true ) )
					tempst.Add(se[i]);
				else
					tempst.Add("");
			}
			
			if ( tempst.Count > 0 )
			{
				int len = m_ItemsList.Length;
				if ( len > m_SpawnList.Length )
					len = m_SpawnList.Length;
				m_ItemsList = new string[tempst.Count];
				for( i = 0; i < len; i++ )
				{
					m_ItemsList[i] = (string)tempst[i];
				}
			}
			tempst.Clear();
		}
		
		public void AddSpawnEntry( string se, int amount )
		{
			if( se == "" )
				return;
			string[] nse = new string[m_SpawnList.Length + 1];
			int[] namount = new int[m_SpawnAmount.Length + 1];
			string[] nie = new string[m_ItemsList.Length + 1];
			
			for( int i = 0; i < m_SpawnList.Length; i++ )
			{
				nse[i] = m_SpawnList[i];
				namount[i] = m_SpawnAmount[i];
				nie[i] = m_ItemsList[i];
			}
			nse[m_SpawnList.Length] = se;
			namount[m_SpawnAmount.Length] = amount;
			nie[m_ItemsList.Length] = "";
			NewSpawnList(nse,namount,nie);
		}
		
		public void NewSpawnList( string[] se, int[] amount, string[] ie )
		{
			NewSpawnList( se, amount, ie, null );
		}
		
		public void NewSpawnList( string[] se, int[] amount, string[] ie, Mobile from )
		{
			int i = 0;
			string[] buffer = new string[0];
			if( from != null && GSMain.Mods )
			{
				buffer = new string[m_SpawnList.Length];
				for( i = 0;i < m_SpawnList.Length; i++ )
					buffer[i] = string.Format("SpawnLine {0}: {1} Amount: {2} Backpack Items: {3}", i.ToString(), m_SpawnList[i], m_SpawnAmount[i], m_ItemsList[i] );
			}
			ArrayList tempst = new ArrayList();
			ArrayList tempam = new ArrayList();
			ArrayList tempie = new ArrayList();
			for( i = 0; i < se.Length; i++ )
			{
				if ( VerifyEntries( ref se[i], i, InContainer ) )
				{
					tempst.Add(se[i].ToLower());
					tempam.Add(amount[i]);
					if ( VerifyEntries( ref ie[i], i, true ) )
						tempie.Add(ie[i]);
					else
						tempie.Add("");
				}
			}
			if ( tempst.Count > 0 )
			{
				m_SpawnList = new string[tempst.Count];
				m_SpawnAmount = new int[tempam.Count];
				m_ItemsList = new string[tempie.Count];
				for( i = 0; i < m_SpawnList.Length; i++ )
				{
					m_SpawnList[i] = (string)tempst[i];
					m_SpawnAmount[i] = (int)tempam[i];
					m_ItemsList[i] = (string)tempie[i];
				}
			}
			else
			{
				m_SpawnList = new string[]{ "" };
				m_SpawnAmount = new int[]{ 0 };
				m_ItemsList = new string[]{ "" };
			}
			if( from != null && GSMain.Mods && Modified )
			{
				GSMain.LogWrite( string.Format("Account: {0} Character: {1} has made the following changes to '{2}' at {3} Map: {4}", from.Account,from,Name,Location,this.Map.ToString()), true, 2);
				GSMain.LogWrite( "Old:", false, 2 );
				for( i = 0;i < buffer.Length; i++ )
				{
					GSMain.LogWrite( buffer[i], false, 2);
				}
				GSMain.LogWrite( "", false, 2 );
				GSMain.LogWrite( "New:", false, 2 );
				for( i = 0; i < m_SpawnList.Length; i++ )
				{
					GSMain.LogWrite( string.Format("SpawnLine {0}: {1} Amount: {2} Backpack Items: {3}", i.ToString(), m_SpawnList[i], m_SpawnAmount[i], m_ItemsList[i] ), false, 2);
				}
			}
			tempst.Clear();
			tempam.Clear();
			tempie.Clear();
			m_SpawnedAmount = new int[m_SpawnList.Length];
			ReCalculateSA();
		}
		
		private static bool VerifyEntry( ref string entry, bool OnlyItems, out string errormsg )
		{
			string[] args, param, chance;
			param = new string[0];
			Type type;
			string name = "";
			errormsg = "";
			object built = null;
			double dc = 0;
			StringBuilder sb = new StringBuilder();
			chance = entry.Split('?');
			#region Check spawn groups
			if ( MobileSpawnGroups.ContainsKey(chance[0]) )
			{
				if( OnlyItems )
				{
					errormsg = String.Format("Can't add mobiles to container remove spawngroup {0} from entry", chance[0] );
					return false;
				}
				if ( chance.Length > 1 )
				{
					try
					{
						dc = Utility.ToDouble(chance[1]);
						if( dc != 0 && dc < 1 )
						{
							entry = chance[0] + "?" + dc.ToString();
							return true;
						}
						else
						{
							entry = chance[0];
							errormsg = String.Format("Invalid entry ({0}) for chance, must be of type double, greater then 0.0 and less then 1.0", chance[1]);
							return false;
						}
					}
					catch
					{
						entry = chance[0];
						errormsg = String.Format("Invalid entry ({0}) for chance, must be of type double, greater then 0.0 and less then 1.0", chance[1]);;
						return false;
					}
				}
				entry = chance[0];
				return true;
			}
			if ( ItemSpawnGroups.ContainsKey(chance[0]) )
			{
				if ( chance.Length > 1 )
				{
					try
					{
						dc = Utility.ToDouble(chance[1]);
						if( dc != 0 && dc < 1 )
						{
							entry = chance[0] + "?" + dc.ToString();
							return true;
						}
						else
						{
							entry = chance[0];
							errormsg = String.Format("Invalid entry ({0}) for chance, must be of type double, greater then 0.0 and less then 1.0", chance[1]);
							return false;
						}
					}
					catch
					{
						entry = chance[0];
						errormsg = String.Format("Invalid entry ({0}) for chance, must be of type double, greater then 0.0 and less then 1.0", chance[1]);;
						return false;
					}
				}
				entry = chance[0];
				return true;
			}
			#endregion
			args = chance[0].Split(':');
			name = args[0];
			type = ScriptCompiler.FindTypeByName( name );
			if ( type == null )
			{
				errormsg = String.Format("Type {0} doesn't exist.",name);
				entry = "";
				return false;
			}
			
			if ( args.Length > 1 )
			{
				Add.FixArgs( ref args );
				param = args[0].Split(' ');
			}


			ConstructorInfo[] ctors = type.GetConstructors();
			bool ok = false;
			bool constructable = false;
			for ( int i = 0; i < ctors.Length; ++i )
			{
				ConstructorInfo ctor = ctors[i];

        #region No Overload For Method 'IsConstructable' Takes '1' Arguments

                //if ( !Add.IsConstructable(ctor))
                //continue;

        #endregion No Overload For Method 'IsConstructable' Takes '1' Arguments

				ParameterInfo[] paramList = ctor.GetParameters();
				if ( param.Length == paramList.Length )
				{
					constructable = true;
					object[] paramValues = Add.ParseValues( paramList, param );
					
					if ( paramValues == null )
						continue;
					built = ctor.Invoke( paramValues );
					if ( built != null )
					{
						if ( built is Mobile && OnlyItems )
						{
							((Mobile)built).Delete();
							errormsg = String.Format("Can't add mobiles to container remove entry: {0}", name );
							entry = "";
							return false;
						}
						else if (built is Mobile)
							((Mobile)built).Delete();
						else if (built is Item)
							((Item)built).Delete();
						sb.Append( name + ":" );
						foreach( string st in param )
							sb.Append( st + " " );
						sb = new StringBuilder(sb.ToString().TrimEnd(new char[]{';',' ',':'}));
						if ( chance.Length > 1 )
						{
							try
							{
								dc = Utility.ToDouble(chance[1]);
								if( dc != 0 && dc < 1)
									sb.Append( "?" + dc.ToString() );
								else
									errormsg = String.Format("Invalid entry ({0}) for chance, must be of type double, greater then 0.0 and less then 1.0", chance[1]);
							}
							catch
							{
								errormsg = String.Format("Invalid entry ({0}) for chance, must be of type double", chance[1]);
							}
						}
						ok = true;
						entry = sb.ToString();
						return errormsg == "";
					}
				}
			}
			if( !ok && param.Length > 0 )
				errormsg = String.Format("No constructor for entry {0}:{1} takes that amount of arguments. Or invalid args for entry ",name,args[0]);
			if( !constructable )
				errormsg = String.Format("Type {0} is not marked constructable.",name);
			
			return false;
		}
		
		private bool VerifyEntries( ref string spawnline, int index, bool OnlyItems )
		{
			if( spawnline == "" || spawnline == null )
				return false;
			string[] errors;
			VerifyEntries( ref spawnline, index, OnlyItems, out errors );
			return spawnline.Trim() != "";
		}
		
		public bool VerifyEntries( ref string spawnline, int index, bool OnlyItems, out string[] errors )
		{
			if( index < m_SpawnList.Length )
				Modified = ( spawnline != m_SpawnList[index] );
			InvalidEntries = false;
			errors = new string[0];
			if (spawnline == null || spawnline == "" )
				return false;
			
			StringBuilder sb = new StringBuilder();
			spawnline = spawnline.ToLower().TrimEnd(new char[]{';',' ',':'});
			string[] temp = spawnline.Split(';');
			string entry;
			string errmsg;
			ArrayList errmsgs = new ArrayList();
			bool errfree = true;
			for ( int i = 0; i < temp.Length; i++ )
			{
				entry = temp[i].ToLower();
				if( VerifyEntry( ref entry, OnlyItems, out errmsg ) )
					sb.Append( entry + ";" );
				else if ( errfree )
					errfree = false;
				errmsgs.Add(errmsg);
			}
			errors = (string[])errmsgs.ToArray(typeof(string));
			spawnline = sb.ToString().TrimEnd(';');
			return errfree;
			
		}
		#endregion
		
		public void BringToHome()
		{
			CleanHashTables(m_SpawnedList);
			foreach( object o in m_SpawnedList.Keys )
			{
				if( o is Mobile )
					((Mobile)o).MoveToWorld(Location,Map);
				if( o is Item )
					((Item)o).MoveToWorld(Location,Map);
			}
		}
		
		#region Spawn methods
		public void ReSpawn()
		{
			DeSpawn();
			FullSpawn();
			DoTimer();
			InvalidateProperties();
		}
		
		public void DeSpawn()
		{
			CleanHashTables(m_SpawnedList);
			ArrayList list = new ArrayList(m_SpawnedList.Keys);
			for (int i=0; i<list.Count;i++)
			{
				//Console.Write("Trying to delete {0} in position {1}...", list[i].GetType().ToString(), i.ToString());
				if (list[i] is Mobile)
				{
					((Mobile)list[i]).Delete();
					//Console.WriteLine("...Deleted");
				}
				else if (list[i] is Item )
				{
					((Item)list[i]).Delete();
					//Console.WriteLine("...Deleted");
				}
				else
					GSMain.LogWrite(String.Format("Not of mobile or item type. Object of type {0}", list[i].GetType().ToString()),true ) ;
			}
			m_SpawnedList.Clear();
			m_SpawnedAmount = new int[m_SpawnList.Length];
		}
		
		public void FullSpawn()
		{
			if ( LinearProgression || ActivityScaled )
			{
				for ( int i = 0; i < m_SpawnAmount[m_spawnprogress]; i++ )
					Spawn(m_spawnprogress);
				return;
			}
			for ( int i=0; i < m_SpawnList.Length; i++ )
			{
				for ( int j = 0; j < m_SpawnAmount[i]; j++ )
					Spawn(i);
			}
		}
		
		public void Spawn()
		{
			if  ( m_SpawnList == null )
				return;
			if ( LinearProgression || ActivityScaled )
			{
				Spawn(m_spawnprogress);
				return;
			}
			if ( m_SpawnList.Length > 0 )
			{
				for ( int i=0; i < m_SpawnList.Length; i++ )
					Spawn( i );
			}
		}
		
		public void Spawn( int index )
		{
			if ( m_SpawnList[index] != null || m_SpawnList[index] != "" )
			{
				string[] se = m_SpawnList[index].Split(';');
				string[] chance;
				ArrayList chances = new ArrayList();
				foreach( string entry in se )
				{
					chance = entry.Split('?');
					if ( chance.Length < 2 )
						continue;
					try
					{
						chances.Add( Utility.ToDouble( chance[1] ) );
					}
					catch
					{
						if ( chances.Count > 0 )
							chances.Add( 0.1 );//if chance invalid default at 10%
					}
				}
				
				if ( chances.Count > 0 )
				{
					double weight = Utility.RandomDouble();
					double count = 0;
					double upperlimit = 1.0;
					for( int i = 0; i < chances.Count; i++ )
					{
						count += (double)chances[i];
						if ( i < (chances.Count - 1) )
							upperlimit = count + (double)chances[i+1];
						else
							upperlimit = 1.0;
						if ( count >= weight && weight <= upperlimit )
						{
							chance = se[i].Split('?');
							Spawn( index, chance[0] );
							return;
						}
					}
				}
				Spawn( index, se[Utility.Random( se.Length )] );
			}
		}
		
		public void Spawn( int index, string spawnentry )
		{
			if ( m_SpawnList[0] == "" || TotalAmount == 0 )
			{
				InvalidEntries = true;
				GSMain.LogWrite(String.Format("Spawner {0} is empty! Location {1} Map: {2}",Name, Location.ToString(), this.Map.ToString()),true);
				Stop();
				return;
			}
			if( m_SpawnedAmount.Length != m_SpawnList.Length )
			{
				m_SpawnedAmount = new int[m_SpawnList.Length];
				ReCalculateSA();
			}
			
			Map map = Map;
			
			if( map == null || map == Map.Internal || spawnentry == "" || index >= m_SpawnList.Length || !Active )
				return;
			
			CleanHashTables(m_SpawnedList);
			if( SpawnedAmount >= TotalAmount )
				return;
			if ( m_SpawnedAmount[index] >= m_SpawnAmount[index] )
				return;
			
			string[] temp;
			if ( MobileSpawnGroups.ContainsKey(spawnentry.ToLower()) )
			{
				temp = ((string)MobileSpawnGroups[spawnentry]).Split(';');
				spawnentry = temp[Utility.Random( temp.Length )];
			}
			if ( ItemSpawnGroups.ContainsKey(spawnentry.ToLower()) )
			{
				temp = ((string)ItemSpawnGroups[spawnentry]).Split(';');
				spawnentry = temp[Utility.Random( temp.Length )];
			}
			object o = null;
			CreateObject(spawnentry, out o);
			if ( o == null )
			{
				temp = spawnentry.Split(':');
				InvalidEntries = true;
				GSMain.LogWrite(String.Format("Error creating spawn entry number {0}, type constructer modified for {1}", index.ToString(), temp[0]), true);
				GSMain.LogWrite(String.Format("Verify spawner entries for {0} at {1} Map: {2}",Name, Location.ToString(), this.Map.ToString()), false );
				return;
			}
			
			if ( InContainer )
			{
				if( o is Item )
					SpawnInContainer( o, index );
				else
				{
					InvalidEntries = true;
					GSMain.LogWrite( "Mobile entry found in a container spawner.", true);
					GSMain.LogWrite(String.Format("Verify spawner entries for {0} at {1} Map: {2}",Name, Location.ToString(), this.Map.ToString()), false );
				}
				return;
			}
			
			if ( o is Mobile )
			{
				Mobile m = (Mobile)o;
				Point3D loc = Point3D.Zero;
				bool home = false;
				if ( FacetSpawn )
					loc = GSMain.RandomFacetLoc( map );
				else if ( GlobalSpawn )
					loc = GSMain.RandomGlobalLoc( ref map );
				if ( loc == Point3D.Zero )
				{
					home = true;
					loc = ( m is BaseVendor ? this.Location : GetSpawnPosition() );
				}
				if ( loc == Point3D.Zero ) //Couldn't find valid spawn location delete mobile
				{
					m.Delete();
					return;
				}
				m.OnBeforeSpawn( loc, map );
				InvalidateProperties();
				m.MoveToWorld( loc, map );
				if ( m is BaseCreature )
				{
					BaseCreature c = (BaseCreature)m;
					if( m_HomeRange < 0 )
					{
						c.RangeHome = 10;
						home = false;
						loc = Point3D.Zero;
					}
					else
						c.RangeHome = m_HomeRange;
					c.CurrentWayPoint = m_WayPoint;
					if ( m_Team > 0 )
						c.Team = m_Team;
					if ( home )
						c.Home = Location;
					else
						c.Home = loc;
				}
				m.OnAfterSpawn();
				string itemstospawn = "";
				try
				{
					itemstospawn = m_ItemsList[index];
					if ( itemstospawn != "" )
						SpawnItems( itemstospawn, m );
				}
				catch{}
			}
			else if ( o is Item )
			{
				Item item = (Item)o;
				Point3D loc = Point3D.Zero;
				if ( FacetSpawn )
					loc = GSMain.RandomFacetLoc( map );
				else if ( GlobalSpawn )
					loc = GSMain.RandomGlobalLoc( ref map);
				if ( loc == Point3D.Zero )
					loc = GetSpawnPosition();
				if ( loc == Point3D.Zero ) //Couldn't find valid spawn location delete item
				{
					item.Delete();
					return;
				}
				item.OnBeforeSpawn( loc, map );
				InvalidateProperties();
				item.MoveToWorld( loc, map );
				item.OnAfterSpawn();
			}
			AddToSpawned(o,index);
		}
		
		public void SpawnItems( string spawnentry, Mobile to )
		{
			if ( to == null || spawnentry == "" || !Active || to.Deleted )
				return;
			string[] entry = spawnentry.Split(';');
			string[] chances;
			double chance = 1.0;
			for ( int j=0; j < entry.Length; j++ )
			{
				chances = entry[j].Split('?');
				if ( chances.Length > 1 )
				{
					try
					{
						chance = Utility.ToDouble(chances[1]);
					}
					catch{}
				}
				
				if ( Utility.RandomDouble() > chance )
					continue;
				
				object o = null;
				string[] temp;
				string tospawn = chances[0];
				if ( ItemSpawnGroups.ContainsKey(tospawn.ToLower()) )
				{
					temp = ((string)ItemSpawnGroups[tospawn]).Split(';');
					tospawn = temp[Utility.Random( temp.Length )];
				}
				CreateObject(tospawn, out o );
				if ( o == null )
				{
					InvalidEntries = true;
					GSMain.LogWrite(String.Format("Error creating item for backpack!" ), true);
					GSMain.LogWrite(String.Format("Verify spawner's item entries at {0} Map: {1}",Location.ToString(), this.Map.ToString() ),false);
					return;
				}
				
				if ( o is Mobile )
				{
					InvalidEntries = true;
					GSMain.LogWrite( "Mobile type found as an item for backpack entry.", true);
					GSMain.LogWrite(String.Format("Verify spawner item for backpack entries for {0} at {1} Map: {2}",Name, Location.ToString(), this.Map.ToString()), false );
					return;
				}
				else if ( o is Item )
				{
					Item item = (Item)o;
					SpawnInBackpack( item, to );
				}
				if( OnlyOne )
					return;
			}
		}
		
		private static void CreateObject(string spawnentry, out object o)
		{
			
			string[] args, param;
			args = spawnentry.Split(':');
			param = new string[0];
			Type type = ScriptCompiler.FindTypeByName( args[0] );
			o = null;
			
			if ( type != null )
			{
				if ( args.Length > 1 )
					param = args[1].Split(' ');
				ConstructorInfo[] ctors = type.GetConstructors();
				for ( int i = 0; i < ctors.Length; ++i )
				{
					ConstructorInfo ctor = ctors[i];
					
					ParameterInfo[] paramList = ctor.GetParameters();
					if ( param.Length == paramList.Length )
					{
						object[] paramValues = Add.ParseValues( paramList, param );
						
						if ( paramValues == null )
							continue;
						
						o = ctor.Invoke( paramValues );
						if( o != null )
							return;
					}
				}
			}
		}
		
		public void SpawnInContainer( object o, int index )
		{
			if ( o == null )
				return;
			Item item = (Item)o;
			if (!(Parent is Container))
				return;
			((Container)Parent).DropItem(item);
			item.OnAfterSpawn();
			AddToSpawned(o,index);
			m_SpawnedAmount[index]++;
		}
		
		public void SpawnInBackpack( Item item, Mobile to )
		{
			if ( item == null || to == null )
				return;
			if ( to.Backpack == null )
				return;
			to.AddToBackpack(item);
		}
		
		public Point3D GetSpawnPosition()
		{
			Map map = Map;
			
			if ( map == null )
				return Location;
			
			// Try 10 times to find a Spawnable location.
			for ( int i = 0; i < 10; i++ )
			{
				int x = m_SpawnArea.X + (Utility.Random( m_SpawnArea.Width ) );
				int y = m_SpawnArea.Y + (Utility.Random( m_SpawnArea.Height ) );
				int z = Map.GetAverageZ( x, y );
				
				if ( Map.CanSpawnMobile( new Point2D( x, y ), this.Z ) )
					return new Point3D( x, y, this.Z );
				else if ( Map.CanSpawnMobile( new Point2D( x, y ), z ) )
					return new Point3D( x, y, z );
			}
			
			return this.Location;
		}
		#endregion
		
		public GigaSpawner( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 1 ); // version
			#region Version 1
			writer.Write((int)m_GAccessLevel );
			writer.Write((int)m_SpawnRange);
			writer.Write((string)m_UID);
			#endregion
			#region Version 0
			writer.Write((int)m_Accesslevel);
			writer.Write((int)m_HomeRange);
			writer.Write((int)m_SPTriggerRange);
			writer.Write((int)m_WalkTriggerRange);
			writer.Write((int)m_Team);
			writer.Write( m_WayPoint );
			writer.Write((Rectangle2D)m_SpawnArea);
			writer.Write((string)m_Creator);
			writer.Write((string)m_Keyword);
			writer.Write( m_MinDelay );
			writer.Write( m_MaxDelay );
			//Spawn Amount Array
			int i = 0;
			writer.Write((int)m_SpawnedAmount.Length);
			for( i=0; i < m_SpawnedAmount.Length; i++ )
				writer.Write((int)m_SpawnedAmount[i]);
			//Spawn List and items for backpack Array
			writer.Write( (int)m_SpawnList.Length );
			for ( i=0; i < m_SpawnList.Length; i++ )
			{
				writer.Write( (string)m_SpawnList[i] );
				writer.Write( (int)m_SpawnAmount[i] );
				writer.Write( (string)m_ItemsList[i] );
			}
			//ModifiedBy hashtable
			CleanHashTables(m_ModifiedBy);
			writer.Write((int)m_ModifiedBy.Count);
			foreach( DictionaryEntry de in m_ModifiedBy )
			{
				writer.Write((Mobile)de.Key);
				writer.Write((string)de.Value);
			}
			//Spawned List Hashtable
			ArrayList SpawnedMobiles = new ArrayList();
			ArrayList MobIndexes = new ArrayList();
			ArrayList SpawnedItems = new ArrayList();
			ArrayList ItemIndexes = new ArrayList();
			CleanHashTables(m_SpawnedList);
			foreach(DictionaryEntry de in m_SpawnedList)
			{
				if ( de.Key is Mobile)
				{
					SpawnedMobiles.Add(de.Key);
					MobIndexes.Add(de.Value);
				}
				else //if ( de.Key is Item)
				{
					SpawnedItems.Add(de.Key);
					ItemIndexes.Add(de.Value);
				}
			}
			writer.Write((int)SpawnedMobiles.Count);
			i = 0;
			foreach(Mobile mob in SpawnedMobiles)
			{
				writer.Write((Mobile)mob);
				writer.Write((int)MobIndexes[i]);
				i++;
			}
			writer.Write((int)SpawnedItems.Count);
			i = 0;
			foreach(Item item in SpawnedItems)
			{
				writer.Write((Item)item);
				writer.Write((int)ItemIndexes[i]);
				i++;
			}
			writer.Write( (int) m_Flags );
			if ( Active )
				writer.WriteDeltaTime( m_End );
			#endregion
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
			#region Version 1
			if( version > 0 )
			{
				m_GAccessLevel = (AccessLevel)reader.ReadInt();
				m_SpawnRange = reader.ReadInt();
				m_UID = reader.ReadString();
			}
			#endregion
			#region Version 0
			m_Accesslevel = (AccessLevel)reader.ReadInt();
			m_HomeRange = reader.ReadInt();
			m_SPTriggerRange = reader.ReadInt();
			m_WalkTriggerRange = reader.ReadInt();
			m_Team = reader.ReadInt();
			m_WayPoint = reader.ReadItem() as WayPoint;
			m_SpawnArea = reader.ReadRect2D();
			m_Creator = reader.ReadString();
			m_Keyword = reader.ReadString();
			m_MinDelay = reader.ReadTimeSpan();
			m_MaxDelay = reader.ReadTimeSpan();
			//Spawn Amount Array
			int len = reader.ReadInt();
			m_SpawnedAmount = new int[len];
			for( int i=0; i<len; i++ )
				m_SpawnedAmount[i] = reader.ReadInt();
			//Spawn List and Items for backpack Array
			len = reader.ReadInt();
			m_SpawnList = new string[len];
			m_SpawnAmount = new int[len];
			m_ItemsList = new string[len];
			for ( int i=0; i < m_SpawnList.Length; i++ )
			{
				m_SpawnList[i] = reader.ReadString();
				m_SpawnAmount[i] = reader.ReadInt();
				m_ItemsList[i] = reader.ReadString();
			}
			
			//ModifiedBy hashtable
			len = reader.ReadInt();
			m_ModifiedBy = new Hashtable();
			for ( int i = 0; i < len; i++ )
				m_ModifiedBy.Add( reader.ReadMobile(), reader.ReadString() );
			//Spawned List Hashtable
			m_SpawnedList = new Hashtable();
			len = reader.ReadInt();
			for( int i = 0; i < len; i++ )
				AddToSpawned(reader.ReadMobile(), reader.ReadInt());
			len = reader.ReadInt();
			for( int i = 0; i < len; i++ )
				AddToSpawned(reader.ReadItem(), reader.ReadInt());
			m_Flags = (SpawnerFlags)reader.ReadInt();
			
			TimeSpan ts = TimeSpan.Zero;
			
			if ( Active )
			{
				ts = reader.ReadDeltaTime() - DateTime.Now;
				DoTimer( ts );
			}
			GSMain.Add(this);
			if ( ShowSpawnRange == true )
				ResetBoundry();
			#endregion
		}
		
	}
}
