﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Server.Engines.Static_Serialization
{
    class StaticSerialization
    {
        private static string Index { get { return Path.Combine( "Saves/StaticSerialization/", "index.idx" ); } }
        private static string Types { get { return Path.Combine( "Saves/StaticSerialization/", "types.tdb" ); } }
        private static string Binary { get { return Path.Combine( "Saves/StaticSerialization/", "binary.bin" ); } }

        public static void Configure()
        {
            EventSink.WorldSave += new WorldSaveEventHandler( EventSink_WorldSave );
            EventSink.WorldLoad += new WorldLoadEventHandler( EventSink_WorldLoad );
            //EventSink_WorldLoad();
        }

        static void EventSink_WorldSave( WorldSaveEventArgs e )
        {
            if ( !Directory.Exists( "Saves/StaticSerialization/" ) )
                Directory.CreateDirectory( "Saves/StaticSerialization/" );

            Dictionary<Type,MethodInfo> classes = new Dictionary<Type,MethodInfo>();

            GenericWriter idx;
            GenericWriter tdb;
            GenericWriter bin;

            idx = new BinaryFileWriter( Index, false );
            tdb = new BinaryFileWriter( Types, false );
            bin = new BinaryFileWriter( Binary, true );

            foreach ( Assembly asm in ScriptCompiler.Assemblies )
            {
                foreach ( Type type in asm.GetTypes() )
                {
                    MethodInfo m = type.GetMethod( "StaticSerialize", new Type[] { typeof( GenericWriter ) } );
                    if ( m != null && m.IsStatic && m.IsPublic )
                    {
                        classes.Add( type, m );
                    }
                }
            }

            idx.Write( (int) classes.Count );
            foreach ( KeyValuePair<Type,MethodInfo> type in classes )
            {
                long start = bin.Position;
                tdb.Write( type.Key.FullName );
                idx.Write( ( long ) start );
                type.Value.Invoke( null, new object[] { bin } );
                idx.Write( (int)( bin.Position - start ) );
            }

            idx.Close();
            tdb.Close();
            bin.Close();
        }

        static void EventSink_WorldLoad()
        {
            if ( !Directory.Exists( "Saves/StaticSerialization/" ) )
                Directory.CreateDirectory( "Saves/StaticSerialization/" );

            List<LoadEntry> loadentries = new List<LoadEntry>();
            if ( File.Exists( Index ) && File.Exists( Types ) )
            {
                using ( FileStream idx = new FileStream( Index, FileMode.Open, FileAccess.Read, FileShare.Read ) )
                {
                    BinaryReader idxReader = new BinaryReader( idx );

                    using ( FileStream tdb = new FileStream( Types, FileMode.Open, FileAccess.Read, FileShare.Read ) )
                    {
                        BinaryReader tdbReader = new BinaryReader( tdb );

                        int count = idxReader.ReadInt32();

                        for ( int i = 0; i < count; i++ )
                        {
                            long start = idxReader.ReadInt64();
                            int length = idxReader.ReadInt32();
                            string typename = tdbReader.ReadString();

                            Type type = null;

                            foreach ( Assembly asm in ScriptCompiler.Assemblies )
                            {
                                type = asm.GetType( typename, false );
                                if ( type != null )
                                    break;
                            }

                            if ( type == null )
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine( "-- Class '{0}' not found, Static Serialization will skip this class, its data will be lost", typename );
                                Console.ResetColor();
                                continue;
                            }

                            loadentries.Add( new LoadEntry( type, start, length ) );
                        }
                        tdbReader.Close();
                    }
                    idxReader.Close();
                }
            }

            if ( File.Exists( Binary ) )
            {
                using ( FileStream bin = new FileStream( Binary, FileMode.Open, FileAccess.Read, FileShare.Read ) )
                {
                    BinaryFileReader reader = new BinaryFileReader( new BinaryReader( bin ) );

                    foreach ( LoadEntry entry in loadentries )
                    {
                        MethodInfo m = entry.Type.GetMethod( "StaticDeserialize", BindingFlags.Static | BindingFlags.Public );
                        if ( m == null )
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine( "-- Bad serialization on '{0}' class (no StaticDeserialize method), skipping...", entry.Type.FullName );
                            Console.ResetColor();
                            continue;
                        }
                        reader.Seek( entry.Start, SeekOrigin.Begin );
                        m.Invoke( null, new object[] { reader } );

                        if ( reader.Position != ( entry.Start + entry.Length ) )
                        {
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine( "-- Bad serialization on '{0}' class (inconsistent read), data may be corrupt, skipping...", entry.Type.FullName );
                                Console.ResetColor();
                                continue;
                            }
                        }
                    }
                    reader.Close();
                }
            }
        }

        class LoadEntry
        {
            Type m_Type;
            long m_Start;
            int m_Length;

            public LoadEntry( Type type, long start, int length )
            {
                m_Type = type;
                m_Start = start;
                m_Length = length;
            }

            public Type Type
            {
                get
                {
                    return m_Type;
                }
            }

            public long Start
            {
                get
                {
                    return m_Start;
                }
            }

            public int Length
            {
                get
                {
                    return m_Length;
                }
            }
        }
    }
}
