﻿using System.Collections;
using Server.Mobiles;
using Server.Items;
using System;
using Server;
using Server.Targeting;
using Server.Network;

namespace Server.Items
{
    public class Bucket : BaseWaterContainer
    {
        public override int voidItem_ID { get { return vItemID; } }
        public override int fullItem_ID { get { return fItemID; } }
        public override int MaxQuantity { get { return 25; } }

        private static int vItemID = 0x14e0;
        private static int fItemID = 0x2004;

        [Constructable]
        public Bucket(): this(false)
        {
        }

        [Constructable]
        public Bucket(bool filled): base((filled) ? Bucket.fItemID : Bucket.vItemID, filled)
        {
            Name = "a bucket";
        }

        public Bucket(Serial serial): base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }

    public class Pail : Item, IWaterSource
    {
        [Constructable]
        public Pail(): this(0x14E0) // 0xFFA
        {
        }

        protected Pail(int itemID): base(itemID)
        {
            Name = "a bucket";
            Movable = true;
            Weight = 10;
        }

        public int Quantity
        {
            get { return 10; }
            set { }
        }

        public override void OnDoubleClick(Mobile m)
        {
            if (!IsChildOf(m.Backpack))
            {
                m.LocalOverheadMessage(MessageType.Regular, 0x3B2, 1019045); // I can't reach that.
            }
            else
            {
                if (ItemID != 0xFFA)
                {
                    m.SendMessage("What do you want to fill this with?");
                    m.Target = new InternalTarget(this);
                }
                if (ItemID != 0x14E0)
                {
                    ItemID = 0x14E0;
                    m.PlaySound(0x4E);
                    m.SendMessage("You smell much better after washing yourself.");
                }
                else
                    return;
            }
        }

        private class InternalTarget : Target
        {
            private Pail m_Pail;

            public InternalTarget(Pail pail): base(1, true, TargetFlags.Beneficial)
            {
                m_Pail = pail;
            }

            protected override void OnTarget(Mobile m, object targeted)
            {
                if (m_Pail.Deleted) return;

                if (targeted is AddonComponent)
                {
                    AddonComponent well = targeted as AddonComponent;

                    if (well.Addon is WaterTroughEastAddon || well.Addon is WaterTroughSouthAddon)
                    {
                        m_Pail.ItemID = 0xFFA;
                        m.PlaySound(0x4E);
                        m.SendMessage("You fill the bucket with water.");
                    }
                    else
                    {
                        m.SendMessage("You can only fill the bucket from a bigger water container.");
                    }
                }
            }
        }

        public Pail(Serial serial): base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}