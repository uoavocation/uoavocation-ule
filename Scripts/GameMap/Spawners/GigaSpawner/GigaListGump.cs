using System;
using System.Collections;
using Server;
using Server.Items;
using Server.Network;
using Server.Targeting;
using Server.GigaSpawners;

namespace Server.Gumps
{
	public class GigaListGump : Server.Gumps.Gump
	{
		private GigaSpawner m_spawner;
		private AccessLevel m_al;
		private ArrayList m_List = new ArrayList(GSMain.GSHash.Values);
		private Mobile m_Owner;
		private int m_Page;
		private const int EntryCount = 15;
		private int i_SearchOp;
		private static string s_errMsg = "";
		private int i_LastPage;
		private bool b_Ascending = false;

		public int GetButtonID( int type, int index )
		{
			return 1 + (index * 15) + type;
		}

		public GigaListGump( Mobile owner, ArrayList list, int page ) : base(0, 0)
		{
			owner.CloseGump( typeof( GigaListGump ) );

			i_SearchOp = 1;
			b_Ascending = true;
			s_errMsg = "";
			m_Owner = owner;
			m_List = list;

			Initialize( page );
		}
		
		public GigaListGump( Mobile owner, ArrayList list, int page, int SearchOp, bool SortOrder, string errMsg ) : base(0, 0)
		{
			owner.CloseGump( typeof( GigaListGump ) );

			i_SearchOp = SearchOp;
			b_Ascending = SortOrder;
			s_errMsg = errMsg;
			m_Owner = owner;
			m_List = list;

			Initialize( page );
		}

		public void Initialize( int page )
		{
			m_Page = page;
			i_LastPage = (m_List.Count + EntryCount - 1) / EntryCount;
			int count = m_List.Count - (page * EntryCount);
			if ( count < 0 )
			count = 0;
			else if ( count > EntryCount )
			count = EntryCount;
			Point3D loc;

			AddPage( 0 );
			//Hidden Buttons
			AddButton(  90,  60, 4006, 4006, GetButtonID( 0, 3 ), GumpButtonType.Reply, 0 );//Goto Page
			AddButton( 575,  60, 5214, 5214, GetButtonID( 4, 3 ), GumpButtonType.Reply, 0 );//New Spawner
			AddButton( 170,  85, 2443, 2443, GetButtonID( 1, 1 ), GumpButtonType.Reply, 0 );//Sort Name
			AddButton( 450,  85, 2443, 2443, GetButtonID( 1, 2 ), GumpButtonType.Reply, 0 );//Sort Facet
			AddButton( 520,  85, 2472, 2472, GetButtonID( 1, 3 ), GumpButtonType.Reply, 0 );//Sort Status
			AddButton( 555,  85, 2472, 2472, GetButtonID( 1, 4 ), GumpButtonType.Reply, 0 );//Sort AccessLevel
			AddButton( 462, 504, 2446, 2446, GetButtonID( 5, 1 ), GumpButtonType.Reply, 0 );//Search Criteria Loop

			// Backgrounds
			AddBackground(50,  50, 615, 500, 3000); //Grey
			AddBackground(55, 100, 605, 405, 3500);//Border

			AddHtml( 50, 60, 615, 20, string.Format( "<center>GigaSpawner List - {0} Spawners</center>", m_List.Count ), false, false );

			if ( page > 0 )
			{
				AddButton( 60, 55, 9770, 9772, GetButtonID( 0, 1 ), GumpButtonType.Reply, 0 ); //Previous
			}
			if ( (page + 1) * EntryCount < m_List.Count )
			{
				AddButton( 60, 80, 9771, 9773, GetButtonID( 0, 2 ), GumpButtonType.Reply, 0 );//Next
			}

			AddHtml( 90, 60, 35, 20, "<basefont color=#FFFFFF>Page</basefont>" , false, false );//Page
			AddBackground( 125, 60, 40, 20, 9300 );
			AddTextEntry(  128, 60, 37, 20, 0, 1, string.Format( "{0}", page + 1 ) );//Goto Page
			AddLabel( 108, 80, 0, string.Format( "of {0}", i_LastPage ) );

			AddHtml( 575, 60, 90, 20, "<basefont color=#FFFFFF>New Spawner</basefont>" , false, false );//New Spawner

			AddHtml(  90, 85, 225, 20, "<basefont color=#FFFFFF><center>Name</center></basefont>" , false, false );//Name
			AddHtml( 320, 85, 120, 20, "<center>Location</center>" , false, false );//Location
			AddHtml( 445, 85,  75, 20, "<basefont color=#FFFFFF><center>Facet</center></basefont>" , false, false );//Facet
			AddHtml( 525, 85,  20, 20, "<basefont color=#FFFFFF><center>A/I</center></basefont>" , false, false );//Active
			AddHtml( 550, 85,  30, 20, "<basefont color=#FFFFFF><center>AL</center></basefont>" , false, false );//AccessLevel
			AddHtml( 585, 85,  55, 20, "<center>Spawned</center>" , false, false );//Spawned

			for ( int i = 0, index = page * EntryCount; i < EntryCount && index < m_List.Count; ++i, ++index )
			{
				GigaSpawner gs = (GigaSpawner)m_List[index];

				AddButton(  70, 122 + (i * 25), 2224, 1210, GetButtonID( 2, i ), GumpButtonType.Reply, 0 );//Open
				AddButton( 325, 119 + (i * 25), 2062, 2062, GetButtonID( 3, i ), GumpButtonType.Reply, 0 ); //Goto
				AddButton( 527, 120 + (i * 25), gs.Active ? 11400 : 11410, gs.Active ? 11401 : 11411, GetButtonID( 6, i ), GumpButtonType.Reply, 0 ); //Status Toggle

				AddBackground(  90, 118 + (i * 25), 225, 20, 9200 );//Name
				AddBackground( 320, 118 + (i * 25), 120, 20, 9200 );//Location
				AddBackground( 445, 118 + (i * 25),  75, 20, 9200 );//Facet
				AddBackground( 550, 118 + (i * 25),  30, 20, 9200 );//AccessLevel
				AddBackground( 585, 118 + (i * 25),  55, 20, 9200 );//Spawned

				if ( gs.RootParent is Container )
					loc = ( (Container)gs.RootParent ).Location;
				else if ( gs.RootParent is Mobile )
					loc = ( (Mobile)gs.RootParent ).Location;
				else
					loc = gs.Location;

					AddHtml(  95, 118 + (i * 25), 220, 20, string.Format( gs.Deleted ? "<basefont color=#FFFFFF><center>(DELETED)</center></basefont>" : gs.InvalidEntries ? "<basefont color=#FF0000>{0}</basefont>" : "{0}", gs.Name ), false, false );//Name
					AddHtml( 320, 118 + (i * 25), 120, 20, string.Format( gs.Deleted ? "<basefont color=#FFFFFF><center>(DELETED)</center></basefont>" : "<center>{0}</center>", loc ), false, false );//Location
					AddHtml( 445, 118 + (i * 25),  75, 20, string.Format( gs.Deleted ? "<basefont color=#FFFFFF><center>(DELETED)</center></basefont>" : "<center>{0}</center>", gs.Map ), false, false );//Facet
					AddHtml( 550, 118 + (i * 25),  30, 20, gs.Deleted ? "" : ( String.Format( "<center>{0}</center>",gs.Accesslevel.ToString().Substring(0,1) ) ), false, false );//Access Level
					AddHtml( 585, 118 + (i * 25),  55, 20, gs.Deleted ? "" : ( String.Format( "<center>{0}/{1}</center>", gs.SpawnedAmount.ToString(), gs.TotalAmount.ToString() ) ), false, false );//Spawned
			}

			AddButton(55, 500, 4030, 4031, GetButtonID( 4, 1 ), GumpButtonType.Reply, 0);//Reset List
			AddLabel( 90, 500, 0, "Reset");

			AddButton(55, 525, 4006, 4007, GetButtonID( 4, 2 ), GumpButtonType.Reply, 0);//Load/Save Menu
			AddLabel( 90, 525, 0, "Load/Save");

			AddHtml(160, 515, 260, 30, string.Format( "<basefont color=#FF0000>{0}</basefont>", s_errMsg ) , true, false );//Error Messages

			AddButton(425, 525, 4006, 4007, GetButtonID( 5, 2 ), GumpButtonType.Reply, 0);//Search Button
			AddBackground( 460, 525, 200, 20, i_SearchOp <= 2 ? 9300 : 9350 );
			switch( i_SearchOp )
			{
				case 1: default:
				{
					AddHtml( 465, 505, 200, 20, "<basefont color=#FFFFFF><center>Search by Name</center></basefont>", false, false );
					AddTextEntry(468, 522, 197, 20, 0, 0, "" );
					break;
				}
				case 2:
				{
					AddHtml( 465, 505, 200, 20, "<basefont color=#FFFFFF><center>Search by Facet</center></basefont>", false, false );
					AddTextEntry(468, 522, 197, 20, 0, 0, "" );
					break;
				}
				case 3: 
				{
					AddHtml( 465, 505, 200, 20, "<basefont color=#FFFFFF><center>Search for Active Spawners</center></basefont>", false, false );
					break;
				}
				case 4:
				{
					AddHtml( 465, 505, 200, 20, "<basefont color=#FFFFFF><center>Search for Inactive Spawners</center></basefont>", false, false );
					break;
				}
				case 5:
				{
					AddHtml( 465, 505, 200, 20, "<basefont color=#FFFFFF><center>Search for Invalid Spawners</center></basefont>", false, false );
					break;
					
				}
			}
		}

		public override void OnResponse( NetState sender, RelayInfo info )
		{
			int val = info.ButtonID - 1;
			int HashPages = ( GSMain.GSHash.Count + EntryCount - 1) / EntryCount;

			if ( val < 0 )
			return;

			int type = val % 15;
			int index = val / 15;
			
			Mobile from = sender.Mobile;
			Point3D loc = new Point3D();;
			ArrayList results = new ArrayList();
			GSMain.GigaGumpClose( from );
			
			TextRelay SearchRelay;
			string SearchString = null;
			if ( i_SearchOp == 1 || i_SearchOp == 2 )
			{
				SearchRelay = info.GetTextEntry( 0 );
				SearchString = SearchRelay.Text.ToLower();
			}

			switch ( type )
			{
				case 0: //Page Navigation
				{
					switch ( index )
					{
						case 1: //Previous
						{
							if ( m_Page > 0 )
								from.SendGump( new GigaListGump( from, m_List, m_Page - 1 ) );
							break;
						}
						case 2: //Next
						{
							if ( (m_Page + 1) * EntryCount < m_List.Count )
								from.SendGump( new GigaListGump( from, m_List, m_Page + 1 ) );
							break;
						}
						case 3://Goto Page
						{
							string GotoPage = Convert.ToString( info.GetTextEntry( 1 ).Text ).ToLower();
							int GotoInt = 0;
							try
							{
								GotoInt = Convert.ToInt32( GotoPage ) - 1;
							}
							catch
							{
								GotoInt = m_Page;
								s_errMsg = string.Format("Page Number must be between 1 and {0}", i_LastPage );
								from.SendGump( new GigaListGump( from, m_List, GotoInt, i_SearchOp, b_Ascending, s_errMsg ) );
								break;
							}

							if ( GotoInt >= 0 && GotoInt <= i_LastPage - 1 )
							{
								s_errMsg = "";
							}
							else
							{
								GotoInt = m_Page;
								s_errMsg = string.Format("Page Number must be between 1 and {0}", i_LastPage );
							}
							from.SendGump( new GigaListGump( from, m_List, GotoInt, i_SearchOp, b_Ascending, s_errMsg ) );
							break;
						}
					}
					break;
				}

				case 1: //Sort
				{
					switch ( index )
					{
						default:
						{
							b_Ascending = !b_Ascending;
							m_List.Sort( new GSSorter( index, b_Ascending ) );
							from.SendGump( new GigaListGump( from, m_List, 0, i_SearchOp, b_Ascending, s_errMsg ) );
							break;
						}
					}
					break;
				}

				case 2: //Open
				{
					index = ( m_Page * EntryCount ) + index;
					GigaSpawner gs = (GigaSpawner)m_List[index];
					if ( gs.Deleted )
					{
						m_List.Remove( gs );
						s_errMsg = "That Spawner has been deleted.";
						from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp, b_Ascending, s_errMsg ) );
						return;
					}
					switch ( index )
					{
						default:
						{
							try
							{
								gs.OnDoubleClick( from );
							}
							catch
							{
								s_errMsg = "Unknow error occured on Spawner open.";
								from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp, b_Ascending, s_errMsg ) );
							}
							break;
						}
					}
					break;
				}

				case 3: //Goto
				{
					index = ( m_Page * EntryCount ) + index;
					GigaSpawner gs = (GigaSpawner)m_List[index];
					if ( gs.Deleted )
					{
						m_List.Remove( gs );
						s_errMsg = "That Spawner has been deleted.";
						from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp, b_Ascending, s_errMsg ) );
						return;
					}
					switch ( index )
					{
						default:
						{
							try
							{
								if ( gs.RootParent is Container )
								loc = ( (Container)gs.RootParent ).Location;
								else if ( gs.RootParent is Mobile )
								loc = ( (Mobile)gs.RootParent ).Location;
								else
								loc = gs.Location;

								Map map = gs.Map;

								if ( map == null || map == Map.Internal )
								{
									s_errMsg = "That spawner is in an inavlid location.";
									from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp, b_Ascending, s_errMsg ) );
								}

								if ( map != null && map != Map.Internal )
								{
									s_errMsg = "You have been teleported to the spawner.";
									from.MoveToWorld( loc, map );
									from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp, b_Ascending, s_errMsg ) );
								}
							}
							catch
							{
								s_errMsg = "Unknow error occured on Goto.";
								from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp, b_Ascending, s_errMsg ) );
							}
							break;
						}
					}
					break;
				}

				case 4: //Reset, Main Menu and Add
				{
					switch ( index )
					{
						case 1: //Reset
						{
							s_errMsg = "List has been reset.";
							ArrayList All_List = new ArrayList(GSMain.GSHash.Values);
							from.SendGump( new GigaListGump( from, All_List, m_Page, 1, true, s_errMsg ) );
							break;
						}
						case 2: //Main Menu
						{
							from.SendGump(new GigaSaveGump(from) );
							break;
						}
						case 3: //New Spawner
						{
							from.Target = new NewGSTarget( );
							break;
						}
						
					}
					break;
				}

				case 5: //Search
				{
					switch ( index )
					{
						case 1://Looping thru Search Operators
						{
							if ( i_SearchOp < 5 )
								from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp + 1, b_Ascending, "" ) );
							else
								from.SendGump( new GigaListGump( from, m_List, m_Page, 1, b_Ascending, "" ) );
							break;
						}
						case 2://Search
						{
							if ( i_SearchOp == 1 )//Name Search
							{
								foreach( GigaSpawner m_gs in m_List )
								{
									if ( m_gs != null && m_gs.Name != null && m_gs.Name.ToLower().IndexOf( SearchString ) >= 0 )
									results.Add( m_gs );
								}
							}
							else if ( i_SearchOp == 2 )//Facet Search
							{
								foreach( GigaSpawner m_gs in m_List )
								{
									if ( m_gs != null && m_gs.Map != null && m_gs.Map.ToString().ToLower().IndexOf( SearchString ) >= 0 )
									results.Add( m_gs );
								}
							}
							else if ( i_SearchOp == 3 )//Active Search
							{
								foreach( GigaSpawner m_gs in m_List )
								{
									if ( m_gs != null && m_gs.Active )
									results.Add( m_gs );
								}
							}
							else if ( i_SearchOp == 4 )//Inactive Search
							{
								foreach( GigaSpawner m_gs in m_List )
								{
									if ( m_gs != null && !m_gs.Active )
									results.Add( m_gs );
								}
							}
							else if ( i_SearchOp == 5 )//Invalid Search
							{
								foreach( GigaSpawner m_gs in m_List )
								{
									if ( m_gs != null && m_gs.InvalidEntries == true )
									results.Add( m_gs );
								}
							}
							if ( results.Count == 0 )
								s_errMsg = "There are no spawners to display.";

							from.SendGump( new GigaListGump( from, results, 0, i_SearchOp, b_Ascending, s_errMsg ) );
							break;
						}
					}
					break;
				}
				

				case 6: //Status Toggle
				{
					index = ( m_Page * EntryCount ) + index;
					GigaSpawner gs = (GigaSpawner)m_List[index];
					if ( gs.Deleted )
					{
						m_List.Remove( gs );
						s_errMsg = "That Spawner has been deleted.";
						from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp, b_Ascending, s_errMsg ) );
						return;
					}
					switch ( index )
					{
						default:
						{
							try
							{
								if ( !gs.Active )
									gs.Start();
								else
									gs.Stop();
							}
							catch
							{
								s_errMsg = "Unknow error occured on Status Toggle.";
							}
							from.SendGump( new GigaListGump( from, m_List, m_Page, i_SearchOp, b_Ascending, s_errMsg ) );
							break;
						}
					}
					break;
				}
			}
		}

		private class NewGSTarget : Target
		{
			public NewGSTarget( ) : base( -1, true, TargetFlags.None )
			{
			}

			protected override void OnTarget( Mobile from, object targeted )
			{
				GigaSpawner gs = new GigaSpawner();
				if ( targeted is Container )
				{
					Container cont = (Container)targeted;
					cont.DropItem( gs );
					cont.OnDoubleClick( from );
				}
				else
				{
					IPoint3D p = targeted as IPoint3D;

					if ( p == null )
					{
						gs.Delete();
						return;
					}

					if ( p is Item )
						p = ((Item)p).GetWorldTop();

					gs.MoveToWorld( new Point3D(p), from.Map );
				}
				from.SendGump(new GigaMainGump( gs, from ) );
			}
		}

		private class GSSorter : IComparer
		{
			private int i_FieldToSort;
			private bool b_SortDir;
			private string s_SortDir;

			public GSSorter( int FieldToSort, bool SortDir )
			{
				i_FieldToSort = FieldToSort;
				b_SortDir = SortDir;
				s_SortDir = SortDir ? "Ascending" : "Descending";
			}

			public int Compare( object x, object y )
			{
				int compare;
				Item a = x as Item;
				Item b = y as Item;

				switch( i_FieldToSort )
				{
					case 1: default: //Sort Name
					{
						s_errMsg = "Sorted by Name, " + s_SortDir;
						if ( b_SortDir )
						return a.Name.CompareTo( b.Name );
						else
						return b.Name.CompareTo( a.Name );
					}
					case 2: //Sort Facet
					{
						s_errMsg = "Sorted by Facet, " + s_SortDir;
						if ( b_SortDir )
						compare = a.Map.ToString().CompareTo( b.Map.ToString() );
						else
						compare = b.Map.ToString().CompareTo( a.Map.ToString() );

						if ( compare == 0 )
						return a.Name.CompareTo( b.Name );
						else
						return compare;
					}
					case 3: //Sort Active, Sorting inverse to the order b/c 0 = Inactive, 1 = Active
					{
						s_errMsg = "Sorted by Active, " + s_SortDir;
						if ( !b_SortDir )
						compare = ((GigaSpawner)a).Active.CompareTo( ((GigaSpawner)b).Active );
						else
						compare = ((GigaSpawner)b).Active.CompareTo( ((GigaSpawner)a).Active );

						if ( compare == 0 )
						return a.Name.CompareTo( b.Name );
						else
						return compare;
					}
					case 4: //Sort Active, Sorting inverse to the order b/c 0 = Inactive, 1 = Active
					{
						s_errMsg = "Sorted by AccessLevel, " + s_SortDir;
						if ( !b_SortDir )
							compare = ((GigaSpawner)a).Accesslevel.ToString().CompareTo( ((GigaSpawner)b).Accesslevel.ToString() );
						else
							compare = ((GigaSpawner)b).Accesslevel.ToString().CompareTo( ((GigaSpawner)a).Accesslevel.ToString() );

						if ( compare == 0 )
							return a.Name.CompareTo( b.Name );
						else
							return compare;
					}
				}
			}
		}
	}
}
