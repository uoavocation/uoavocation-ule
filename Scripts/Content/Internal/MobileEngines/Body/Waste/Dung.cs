using System;
using Server;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;

namespace Server.Items
{
    public class Dung : Item
	{
		[Constructable]
		public Dung() : this( 1 )
		{
		}

		[Constructable]
		public Dung( int amount ) : base( 0xF3C )
		{
			Weight = 4.0;
			Stackable = true;
			Amount = amount;
            Name = "A Pile of Dung";
		}

		public override bool CheckItemUse( Mobile from, Item item ) 
		{ 
			if ( item != this )
				return base.CheckItemUse( from, item );

			if ( from != this.RootParent )
			{
				from.SendLocalizedMessage( 1042038 );
				return false;
			}

			return base.CheckItemUse( from, item );
		}


		public override void OnDoubleClick( Mobile from )
		{
			if ( !IsChildOf( from.Backpack ) )
			{
                from.SendMessage("The pile of dung must be in your pack to use it.");
			}
			else if ( !from.CanBeginAction( typeof( Bola ) ) )
			{
				from.SendMessage( "You have to wait a few moments before you can use another pile of dung!" );
			}
            else if (from.Target is DungTarget)
			{
                from.SendMessage("This pile of dung is currently being used.");
			}
			else if ( from.FindItemOnLayer( Layer.OneHanded ) != null || from.FindItemOnLayer( Layer.TwoHanded ) != null )
			{
				from.SendLocalizedMessage( 1040015 );
			}
			else if ( from.Mounted )
			{
				from.SendLocalizedMessage( 1040016 );
			}
			else
			{
				from.Target = new DungTarget( this );
				from.SendMessage( "You've balled up the pile of dung in your hand." );
				//from.NonlocalOverheadMessage( MessageType.Emote, 0x3B2, 1049633, from.Name );
			}
		}

        private static void ReleaseDungLock(object state)
		{
			((Mobile)state).EndAction( typeof( Dung ) );
		}

		private static void ReleaseMountLock( object state )
		{
			((Mobile)state).EndAction( typeof( BaseMount ) );
		}

		private static void FinishThrow( object state )
		{
			object[] states = (object[])state;

			Mobile from = (Mobile)states[0];
			Mobile to = (Mobile)states[1];

			if ( Core.AOS )
				new Bola().MoveToWorld( to.Location, to.Map );

			to.Damage( 1, from );

			to.BeginAction( typeof( BaseMount ) );

			to.SendLocalizedMessage( 1040023 );

			Timer.DelayCall( TimeSpan.FromSeconds( 3.0 ), new TimerStateCallback( ReleaseMountLock ), to );
			Timer.DelayCall( TimeSpan.FromSeconds( 2.0 ), new TimerStateCallback( ReleaseDungLock ), from );
		}

		private class DungTarget : Target
		{
            private Dung m_Dung;

			public DungTarget( Dung dung ) : base( 8, false, TargetFlags.Harmful )
			{
                m_Dung = dung;
			}

			protected override void OnTarget( Mobile from, object obj )
			{
                if (m_Dung.Deleted)
					return;

				if ( obj is Mobile )
				{
					Mobile to = (Mobile)obj;

                    if (!m_Dung.IsChildOf(from.Backpack))
					{
						from.SendMessage( "The pile of dung must be your pack to use it." );
					}
					else if ( from.FindItemOnLayer( Layer.OneHanded ) != null || from.FindItemOnLayer( Layer.TwoHanded ) != null )
					{
						from.SendLocalizedMessage( 1040015 );
					}
					else if ( from.Mounted )
					{
						from.SendLocalizedMessage( 1040016 );
					}
					else if ( !to.Mounted )
					{
						from.SendMessage( "You have no reason to throw dung at that." );
					}
					else if ( !from.CanBeHarmful( to ) )
					{
					}
					else if ( from.BeginAction( typeof( Dung ) ) )
					{
						from.DoHarmful( to );

                        m_Dung.Consume();

						from.Direction = from.GetDirectionTo( to );
						from.Animate( 11, 5, 1, true, false, 0 );
						from.MovingEffect( to, 0x26AC, 10, 0, false, false );

						Timer.DelayCall( TimeSpan.FromSeconds( 0.5 ), new TimerStateCallback( FinishThrow ), new object[]{ from, to } );
					}
					else
					{
						from.SendMessage( "You have to wait a few moments before you can use another pile of dung." );
					}
				}
				else
				{
					from.SendMessage( "You cannot throw dung at that." );
				}
			}
		}

		public Dung( Serial serial ) : base( serial )
		{
        }

        public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
