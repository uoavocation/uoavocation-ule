using System;
using System.IO;
using System.Collections;
using Server;
using Server.Gumps;
using Server.Items;
using Server.Network;
using Server.Targeting;

namespace Server.GigaSpawners
{
	public class GSTemplate
	{
		public static void GSTempLoad( GSTemplateBook gstb )
		{
			if( gstb.FileName == null )
				return;
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			string path = @"GigaSpawner\" + gstb.FileName + ".gst";
			string idxpath = @"GigaSpawner\" + gstb.FileName + ".idx";
			gstb.Templates.Clear();
			try
			{
				if ( !File.Exists( path ) )
				{
					File.Delete(idxpath);
					return;
				}
				if ( File.Exists( idxpath ) )
				{
					using ( FileStream m_FileStream = new FileStream( idxpath, FileMode.Open, FileAccess.Read) )
					{
						BinaryReader m_BinaryReader = new BinaryReader(m_FileStream);
						BinaryFileReader reader = new BinaryFileReader(m_BinaryReader);
						while( reader.Position != m_FileStream.Length )
							gstb.Templates.Add(reader.ReadString(),reader.ReadLong());
						using ( FileStream m_FileStream1 = new FileStream( path, FileMode.Open, FileAccess.Read) )
						{
							gstb.NextIDX = m_FileStream1.Length;
							m_FileStream1.Close();
						}
						if( GSMain.Debug )
						{
							GSMain.LogWrite("GSTempLoad",true,3);
							foreach( DictionaryEntry de in gstb.Templates )
								GSMain.LogWrite(String.Format("Template Name = {0} Index = {1}", de.Key, de.Value),false,3);
							GSMain.LogWrite(String.Format("Template Book NextIDX = {0}", gstb.NextIDX),false,3);
						}
						reader.Close();
						m_BinaryReader.Close();
						m_FileStream.Close();
					}
				}
				else
				{
					File.Delete(path);
					if( GSMain.Debug )
						GSMain.LogWrite(String.Format("File {0} deleted, no idx found!",gstb.FileName),true,3);
				}
			}
			catch(Exception err)
			{
				GSMain.LogWrite("Template File: " + gstb.FileName, true);
				GSMain.LogWrite("Template Load error: " + err.Message,false);
				return;
			}
		}
		
		public static void GSTempRead( GSTemplateBook gstb, Mobile from )
		{
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			string path = @"GigaSpawner\" + gstb.FileName + ".gst";
			GSTempLoad(gstb);
			if( !gstb.Templates.ContainsKey(gstb.TemplateName) )
			{
				from.SendMessage("That template was deleted from the file.");
				gstb.TemplateName = null;
				return;
			}
			long idx = (long)gstb.Templates[gstb.TemplateName];
			GigaSpawner gs = new GigaSpawner();
			try
			{
				if ( File.Exists( path ) )
				{
					using ( FileStream m_FileStream = new FileStream( path, FileMode.Open, FileAccess.Read) )
					{
						BinaryReader m_BinaryReader = new BinaryReader(m_FileStream);
						BinaryFileReader reader = new BinaryFileReader(m_BinaryReader);
						m_FileStream.Position = idx;
						GSTempHolder gst = new GSTempHolder();
						Read(reader,ref gst);
						gs.Name = gst.Name;//Name
						gs.SpawnRange = gst.SpawnRange;//SpawnRange
						gs.HomeRange = gst.HomeRange;//HomeRange
						gs.SPTriggerRange = gst.SPTriggerRange;//Speech trigger Range
						gs.WalkTriggerRange = gst.WalkTriggerRange;//Walk trigger Range
						gs.Team = gst.Team;//Team
						gs.WayPoint = gst.WayPoint;//Waypoint
						gs.SpawnArea = gst.SpawnArea;//Spawnarea
						gs.Keyword = gst.Keyword;//Keyword
						gs.MinDelay = gst.MinDelay;//Min delay
						gs.MaxDelay = gst.MaxDelay;//Max delay
						gs.SpawnList = gst.SpawnList;
						gs.SpawnAmount = gst.SpawnAmount;
						gs.ItemsList = gst.ItemsList;
						gs.Flags = gst.Flags;//Flags
						gst = null;
						if( GSMain.Debug )
						{
							GSMain.LogWrite("GSTempRead: " + gstb.TemplateName,true,3);
							GSMain.LogWrite(String.Format("Name = {0} Spawn Range = {1} Home Range = {2}", gs.Name, gs.SpawnRange, gs.HomeRange ),false,3);
							GSMain.LogWrite(String.Format("Speech trigger Range = {0} Walk trigger Range = {1} Team = {2} Waypoint = {3}", gs.SPTriggerRange, gs.WalkTriggerRange, gs.Team, gs.WayPoint ),false,3);
							GSMain.LogWrite(String.Format("Spaw Area = {0} Keyword = {1} Min Delay = {2} Max Delay = {3}", gs.SpawnArea, gs.Keyword, gs.MinDelay, gs.MaxDelay ),false,3);
							GSMain.LogWrite(String.Format("Flags = {0} Len = {1}", gs.Flags, gs.SpawnList.Length ),false,3);
							for(int i=0; i<gs.SpawnList.Length;i++)
								GSMain.LogWrite(String.Format("Spawn Line = {0} Amount = {1} Item Line = {2}", gs.SpawnList[i], gs.SpawnAmount[i], gs.ItemsList[i] ),false,3);
							GSMain.LogWrite(String.Format("IDX = {0} NextIDX = {1}", idx, gstb.NextIDX ),false,3);
						}
						reader.Close();
						m_BinaryReader.Close();
						m_FileStream.Close();
						from.Target = new InternalTarget( gs, gstb );
					}
				}
			}
			catch(Exception err)
			{
				gs.Delete();
				GSMain.LogWrite(String.Format("Template Load error: File name {0} Template name: {1}", gstb.FileName, gstb.TemplateName),true);
				GSMain.LogWrite("Load error: " + err.Message,false);
				return;
			}
		}
		
		private class InternalTarget : Target
		{
			private GigaSpawner m_gs;
			private GSTemplateBook gstb;
			
			public InternalTarget(GigaSpawner gs, GSTemplateBook gst) : base( 15, true, TargetFlags.None )
			{
				m_gs = gs;
				gstb = gst;
			}
			
			protected override void OnTarget( Mobile from, object targeted )
			{
				if( targeted is Container )
				{
					Container cont = (Container)targeted;
					cont.DropItem(m_gs);
				}
				else
				{
					Point3D p = new Point3D((IPoint3D)targeted);
					m_gs.MoveToWorld(p,from.Map);
				}
				from.SendGump(new GSTemplateGump(gstb,from));
			}
			
			protected override void OnTargetCancel(Mobile from, TargetCancelType type)
			{
				m_gs.Delete();
				from.SendGump(new GSTemplateGump(gstb,from));
			}
		}
		
		public static void GSTempWrite( GigaSpawner spawner, GSTemplateBook gstb )
		{
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			string path = @"GigaSpawner\" + gstb.FileName + ".gst";
			string idxpath = @"GigaSpawner\" + gstb.FileName + ".idx";
			if( gstb.Templates.ContainsKey(gstb.TemplateName) )
			{
				string tname = gstb.TemplateName;
				GSTempDelete(gstb);
				gstb.TemplateName = tname;
				if( GSMain.Debug )
					GSMain.LogWrite(String.Format("Overwriting Template Name: {0}.",gstb.TemplateName),true,3);
			}
			try
			{
				using(FileStream m_FileStream = new FileStream( path, FileMode.OpenOrCreate, FileAccess.Write ))
				{
					BinaryFileWriter writer = new BinaryFileWriter( m_FileStream,true);
					m_FileStream.Position = gstb.NextIDX;
					gstb.Templates.Add(gstb.TemplateName,writer.Position);
					try
					{
						using ( FileStream m_FileStream1 = new FileStream( idxpath, FileMode.Append, FileAccess.Write) )
						{
							BinaryFileWriter writer1 = new BinaryFileWriter( m_FileStream1,true );
							writer1.Write((string)gstb.TemplateName);
							writer1.Write((long)writer.Position);
							writer1.Close();
							m_FileStream1.Close();
						}
					}
					catch(Exception err)
					{
						Console.WriteLine( err.Message );
					}
					Write(writer,spawner);
					gstb.NextIDX = writer.Position;
					if( GSMain.Debug )
					{
						GSMain.LogWrite(String.Format("GSTempWrite: {0}.",gstb.TemplateName),true,3);
						GSMain.LogWrite(String.Format("Version = 0 Name = {0} Spawn Range = {1} Home Range = {2}", spawner.Name, spawner.SpawnRange, spawner.HomeRange ),false,3);
						GSMain.LogWrite(String.Format("Speech trigger Range = {0} Walk trigger Range = {1} Team = {2} Waypoint = {3}", spawner.SPTriggerRange, spawner.WalkTriggerRange, spawner.Team, spawner.WayPoint ),false,3);
						GSMain.LogWrite(String.Format("Spaw Area = {0} Keyword = {1} Min Delay = {2} Max Delay = {3}", spawner.SpawnArea, spawner.Keyword, spawner.MinDelay, spawner.MaxDelay ),false,3);
						GSMain.LogWrite(String.Format("Flags = {0} Len = {1}", spawner.Flags, spawner.SpawnList.Length ),false,3);
						for(int i=0; i<spawner.SpawnList.Length;i++)
							GSMain.LogWrite(String.Format("Spawn Line = {0} Amount = {1} Item Line = {2}", spawner.SpawnList[i], spawner.SpawnAmount[i], spawner.ItemsList[i] ),false,3);
						GSMain.LogWrite(String.Format("IDX = {0} NextIDX = {1}", gstb.Templates[gstb.TemplateName], gstb.NextIDX ),false,3);
					}
					writer.Close();
					m_FileStream.Close();
				}
			}
			catch(Exception err)
			{
				GSMain.LogWrite(String.Format("Template Save error: File name {0} Template name {1}", gstb.FileName,gstb.TemplateName),true);
				GSMain.LogWrite("Save error: " + err.Message,false);
				return;
			}
		}
		
		public static void GSTempDelete( GSTemplateBook gstb )
		{
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			string path = @"GigaSpawner\" + gstb.FileName + ".gst";
			string idxpath = @"GigaSpawner\" + gstb.FileName + ".idx";
			if ( !File.Exists( path ) || !File.Exists( idxpath ) )
				return;
			if( !gstb.Templates.ContainsKey(gstb.TemplateName) )
				return;
			if( GSMain.Debug )
				GSMain.LogWrite("GSTempDelete: " + gstb.TemplateName,true,3);
			try
			{
				Hashtable temp = new Hashtable();
				#region Move to cache
				using ( FileStream m_FileStream = new FileStream( path, FileMode.Open, FileAccess.Read) )
				{
					BinaryReader m_BinaryReader = new BinaryReader(m_FileStream);
					BinaryFileReader reader = new BinaryFileReader(m_BinaryReader);
					foreach( DictionaryEntry de in gstb.Templates )
                    {

    #region Possible Unintended Reference Comparison; To Get A Value Comparison, Cast The Left Hand Side To Type 'String' //Console Warning

                        //if ( de.Key == gstb.TemplateName)
                          if (de.Key is string && (string) de.Key == gstb.TemplateName)

    #endregion Possible Unintended Reference Comparison; To Get A Value Comparison, Cast The Left Hand Side To Type 'String' //Console Warning

                            continue;


						if( GSMain.Debug )
							GSMain.LogWrite(String.Format("Keeping: Template Name = {0} IDX = {1}", de.Key, de.Value ),false,3);
						m_FileStream.Position = (long)de.Value;
						GSTempHolder gst = new GSTempHolder();
						Read(reader,ref gst);
						temp.Add(de.Key,gst);
					}
					if( temp.ContainsKey(gstb.TemplateName) )
					{
						temp.Remove(gstb.TemplateName);
						if( GSMain.Debug )
							GSMain.LogWrite(String.Format("Removed on 2nd sweep: Template Name = {0}", gstb.TemplateName ),false,3);
					}
					reader.Close();
					m_BinaryReader.Close();
					m_FileStream.Close();
				}
				#endregion
				
				File.Delete( path );
				File.Delete( idxpath );
				gstb.Templates.Clear();
				
				#region Write from cache
				using(FileStream m_FileStream = new FileStream( path, FileMode.OpenOrCreate, FileAccess.Write ))
				{
					BinaryFileWriter writer = new BinaryFileWriter( m_FileStream,true);
					foreach( DictionaryEntry de in temp )
					{
						using ( FileStream m_FileStream1 = new FileStream( idxpath, FileMode.Append, FileAccess.Write) )
						{
							BinaryFileWriter writer1 = new BinaryFileWriter( m_FileStream1,true );
							writer1.Write((string)de.Key);
							writer1.Write((long)writer.Position);
							writer1.Close();
							m_FileStream1.Close();
						}
						Write(writer, (GSTempHolder)de.Value);
					}
					gstb.NextIDX = writer.Position;
					writer.Close();
					m_FileStream.Close();
				}
				#endregion
				
				gstb.TemplateName = null;
				GSTempLoad(gstb);
			}
			catch(Exception err)
			{
				GSMain.LogWrite(String.Format("Template deletion error: File name {0} Template name {1}", gstb.FileName, gstb.TemplateName),true);
				GSMain.LogWrite("Delete error: " + err.Message,false);
				return;
			}
		}
		
		private static void Write(BinaryFileWriter writer, GSTempHolder gst)
		{
			GigaSpawner gs = new GigaSpawner();
			gs.Name = gst.Name;//Name
			gs.SpawnRange = gst.SpawnRange;//SpawnRange
			gs.HomeRange = gst.HomeRange;//HomeRange
			gs.SPTriggerRange = gst.SPTriggerRange;//Speech trigger Range
			gs.WalkTriggerRange = gst.WalkTriggerRange;//Walk trigger Range
			gs.Team = gst.Team;//Team
			gs.WayPoint = gst.WayPoint;//Waypoint
			gs.SpawnArea = gst.SpawnArea;//Spawnarea
			gs.Keyword = gst.Keyword;//Keyword
			gs.MinDelay = gst.MinDelay;//Min delay
			gs.MaxDelay = gst.MaxDelay;//Max delay
			gs.SpawnList = gst.SpawnList;
			gs.SpawnAmount = gst.SpawnAmount;
			gs.ItemsList = gst.ItemsList;
			gs.Flags = gst.Flags;//Flags
			Write(writer,gs);
			gs.Delete();
		}
		
		private static void Write(BinaryFileWriter writer, GigaSpawner spawner)
		{
			writer.Write((int)0);//Version
			writer.Write((string)spawner.Name);
			writer.Write((int)spawner.SpawnRange);
			writer.Write((int)spawner.HomeRange);
			writer.Write((int)spawner.SPTriggerRange);
			writer.Write((int)spawner.WalkTriggerRange);
			writer.Write((int)spawner.Team);
			writer.Write( spawner.WayPoint );
			writer.Write((Rectangle2D)spawner.SpawnArea);
			writer.Write((string)spawner.Keyword);
			writer.Write( spawner.MinDelay );
			writer.Write( spawner.MaxDelay );
			writer.Write( (int)spawner.SpawnList.Length );
			for ( int i=0; i < spawner.SpawnList.Length; i++ )
			{
				writer.Write( (string)spawner.SpawnList[i] );
				writer.Write( (int)spawner.SpawnAmount[i] );
				writer.Write( (string)spawner.ItemsList[i] );
			}
			writer.Write( (int) spawner.Flags );
		}
		
		private static void Read(BinaryFileReader reader, ref GSTempHolder gs )
		{
			int version = reader.ReadInt();//Version
			gs.Name = reader.ReadString();//Name
			gs.SpawnRange = reader.ReadInt();//SpawnRange
			gs.HomeRange = reader.ReadInt();//HomeRange
			gs.SPTriggerRange = reader.ReadInt();//Speech trigger Range
			gs.WalkTriggerRange = reader.ReadInt();//Walk trigger Range
			gs.Team = reader.ReadInt();//Team
			gs.WayPoint = reader.ReadItem() as WayPoint;//Waypoint
			gs.SpawnArea = reader.ReadRect2D();//Spawnarea
			gs.Keyword = reader.ReadString();//Keyword
			gs.MinDelay = reader.ReadTimeSpan();//Min delay
			gs.MaxDelay = reader.ReadTimeSpan();//Max delay
			//Spawn Amount Array
			int len = reader.ReadInt();
			gs.SpawnList = new string[len];
			gs.SpawnAmount = new int[len];
			gs.ItemsList = new string[len];
			for ( int i=0; i < len; i++ )
			{
				gs.SpawnList[i] = reader.ReadString();//SpawnList
				gs.SpawnAmount[i] = reader.ReadInt();//SpawnAmount
				gs.ItemsList[i] = reader.ReadString();//ItemsList
			}
			gs.Flags = (SpawnerFlags)reader.ReadInt();//Flags
		}
		
	}
	
	public class GSTempHolder
	{
		public string Name;
		public int SpawnRange;
		public int HomeRange;
		public int SPTriggerRange;
		public int WalkTriggerRange;
		public int Team;
		public WayPoint WayPoint;
		public Rectangle2D SpawnArea;
		public string Keyword;
		public TimeSpan MinDelay;
		public TimeSpan MaxDelay;
		public string[] SpawnList;
		public int[] SpawnAmount;
		public string[]ItemsList;
		public SpawnerFlags Flags;
		
		public GSTempHolder()
		{
		}
		
	}
	
	public class GSTemplateBook : Item
	{
		private string m_TemplateName;
		private string m_FileName;
		private AccessLevel m_AccessLevel;
		
		public long NextIDX = 0;
		public Hashtable Templates = new Hashtable();
		
		[CommandProperty( AccessLevel.GameMaster )]
		public AccessLevel AccessLevel{ get{ return m_AccessLevel;} set{ m_AccessLevel=value;} }
		
		public string TemplateName { get{ return m_TemplateName; } set { m_TemplateName = value; } }
		
		public string FileName
		{
			get{ return m_FileName; }
			set
			{
				if( m_FileName != value )
					Templates.Clear();
				m_FileName = value;
			}
		}
		
		[Constructable]
		public GSTemplateBook() : base (0x1C13)
		{
			Name = "GS Template Book";
			m_AccessLevel = AccessLevel.GameMaster;
		}
		
		public void Clear()
		{
			NextIDX = 0;
			TemplateName = null;
			FileName = null;
			Templates.Clear();
		}
		
		public override void OnDoubleClick( Mobile from )
		{
			if( AccessLevel > from.AccessLevel )
			{
				Delete();
				return;
			}
			if( from.HasGump( typeof( GSTemplateGump ) ) )
				from.CloseGump( typeof( GSTemplateGump ) );
			from.SendGump( new GSTemplateGump(this, from) );
		}
		
		public GSTemplateBook ( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize(writer);
			writer.Write((int)1);//version
			writer.Write((int)m_AccessLevel);
			writer.Write((string)m_TemplateName);
			writer.Write((string)m_FileName);
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize(reader);
			int version = reader.ReadInt();
			if( version == 1 )
				m_AccessLevel = (AccessLevel)reader.ReadInt();
			m_TemplateName = reader.ReadString();
			m_FileName = reader.ReadString();
			GSTemplate.GSTempLoad(this);
		}
	}
	
	public class GSTemplateGump : Gump
	{
		private GSTemplateBook gstb;
		private ArrayList templates;
		
		public GSTemplateGump(GSTemplateBook gs, Mobile from) : base(50,50)
		{
			gstb = gs;
			templates = new ArrayList(gs.Templates.Keys);
			if( templates.Count > 0 )
				templates.Sort();
			int i = 0;
			AddPage(1);
			AddImage(0, 0, 500);
			AddBackground( 30, 45, 160, 20, 9350 );
			AddBackground( 225, 95, 160, 20, 9350 );
			AddLabel(35, 105, 1302, gstb.FileName == null ? "No File Loaded" : gstb.FileName );
			AddLabel(35, 80, 0, "Currently Loaded File:");
			AddLabel(50, 190, 0, "Create GigaSpawner");
			AddLabel(50, 20, 0, "Load template file:");
			AddLabel(225, 20, 0, "Current Template:");
			AddLabel(225, 40, 0, gstb.TemplateName == null ? "No Template Selected" : gstb.TemplateName );
			AddLabel(245, 70, 0, "Create Template");
			AddLabel(245, 120, 0, "Overwrite Template");
			if( from.AccessLevel > AccessLevel.Seer )
			{
				AddLabel(50, 165, 0, "Delete Template");
				AddLabel(50, 140, 0, "Delete File");
			}
			AddHtml( 225, 150, 155, 50, "Template list for current file on following pages",false, false);
			AddButton(356, 0, 502, 502, 0, GumpButtonType.Page, 2);
			AddButton(401, 33, 2225, 2225, 0, GumpButtonType.Page, 2);
			AddButton(401, 55, 2226, 2226, 0, GumpButtonType.Page, 3);
			AddButton(400, 3, 42, 42, 0, GumpButtonType.Page, 4);// Minimize
			AddButton(30, 23, 1209, 1210, 1, GumpButtonType.Reply, 0);//Load Template
			AddButton(225, 73, 1209, 1210, 2, GumpButtonType.Reply, 0);//Create Template
			AddButton(225, 123, 1209, 1210, 3, GumpButtonType.Reply, 0);//Overwrite Template
			AddButton(30, 193, 1209, 1210, 4, GumpButtonType.Reply, 0);// Create GS
			if( from.AccessLevel > AccessLevel.Seer )
			{
				AddButton(30, 168, 1209, 1210, 5, GumpButtonType.Reply, 0);// Delete Template
				AddButton(30, 143, 1209, 1210, 6, GumpButtonType.Reply, 0);// Delete File
			}
			AddTextEntry(35, 45, 150, 20, 0, 0, "");//File Name
			AddTextEntry(230, 95, 150, 20, 0, 1, "");//Template Name
			
			AddPage(2);
			AddImage(0, 0, 500);
			AddLabel(112, 202, 0, "1");
			AddLabel(298, 202, 0, "2");
			AddLabel(80, 10, 0, "Templates");
			AddLabel(270, 10, 0, "Templates");
			AddButton(0, 0, 501, 501, 0, GumpButtonType.Page, 1);
			AddButton(356, 0, 502, 502, 0, GumpButtonType.Page, 3);
			AddButton(6, 36, 2223, 2223, 0, GumpButtonType.Page, 1);
			AddButton(394, 33, 2225, 2225, 0, GumpButtonType.Page, 2);
			AddButton(401, 55, 2226, 2226, 0, GumpButtonType.Page, 3);
			AddButton(400, 3, 42, 42, 0, GumpButtonType.Page, 4);//Minimize
			if( templates.Count == 0 )
			{
				AddLabel(60, 40, 0, "No File Loaded");
				AddLabel(250, 40, 0, "No File Loaded");
			}
			
			while( i < 10 && i < templates.Count )
			{
				AddButton(40, 43 + 15 * i, 1209, 1210, 10 + i, GumpButtonType.Reply, 0);
				AddLabel(60, 40 + 15 * i, 0, (string)templates[i]);
				i++;
			}
			while( i < 20 && i < templates.Count )
			{
				AddButton(230, 43 + 15 * (i - 10), 1209, 1210, 20 + i, GumpButtonType.Reply, 0);
				AddLabel(250, 40 + 15 * (i - 10), 0, (string)templates[i]);
				i++;
			}
			
			AddPage(3);
			AddImage(0, 0, 500);
			AddLabel(112, 202, 0, "3");
			AddLabel(298, 202, 0, "4");
			AddLabel(80, 10, 0, "Templates");
			AddLabel(270, 10, 0, "Templates");
			AddButton(0, 0, 501, 501, 0, GumpButtonType.Page, 2);
			AddButton(6, 36, 2223, 2223, 0, GumpButtonType.Page, 1);
			AddButton(400, 35, 2225, 2225, 0, GumpButtonType.Page, 2);
			AddButton(394, 55, 2226, 2226, 0, GumpButtonType.Page, 3);
			AddButton(400, 3, 42, 42, 0, GumpButtonType.Page, 4);//Minimize
			if( templates.Count == 0 )
			{
				AddLabel(60, 40, 0, "No File Loaded");
				AddLabel(250, 40, 0, "No File Loaded");
			}
			while( i < 30 && i < templates.Count )
			{
				AddButton(40, 43 + 15 * (i - 20), 1209, 1210, 30 + i, GumpButtonType.Reply, 0);
				AddLabel(60, 40 + 15 * (i - 20), 0, (string)templates[i]);
				i++;
			}
			while( i < 40 && i < templates.Count )
			{
				AddButton(230, 43 + 15 * (i - 30), 1209, 1210, 40 + i, GumpButtonType.Reply, 0);
				AddLabel(250, 40 + 15 * (i - 30), 0, (string)templates[i]);
				i++;
			}
			AddPage(4);
			AddBackground(200, 300, 120, 50, 5054); //Gray Background
			AddLabel ( 205, 300, 0, "GS Template Book"); //Title
			AddImageTiled( 210, 320, 100, 20, 3604); //Dark Blue Background
			AddImageTiled( 210, 320, 1, 20, 2624); //Left Verticle Line
			AddImageTiled( 210, 320, 100, 1, 2624); //Top Horizontal Line
			AddImageTiled( 310, 320, 1, 20, 2624); //Right Verticle Line
			AddImageTiled( 210, 340, 101, 1, 2624); //Bottom Horizontal Line
			AddButton ( 235, 323, 5002, 5002, 0, GumpButtonType.Page, 1); //Maximize Button
			AddImageTiled( 238, 325, 1, 10, 2624); //Left Verticle Line Maximize
			AddImageTiled( 238, 325, 10, 2, 2624); //Top Horizontal Line Maximize
			AddImageTiled( 248, 325, 1, 11, 2624); //Right Verticle Line Maximize
			AddImageTiled( 238, 335, 10, 1, 2624); //Bottom Horizontal Line Maximize
			AddButton ( 273, 323, 5003, 5003, 0, GumpButtonType.Reply, 0); //Close Button
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			if( info.ButtonID == 0 )
				return;
			Mobile from = sender.Mobile;
			if( info.ButtonID == 1 )
			{
				if( info.GetTextEntry(0).Text == "" )
				{
					from.SendGump( new GSFileList(from, "*.gst", 2, gstb) );
					return;
				}
				string filename = info.GetTextEntry(0).Text;
				if( filename.IndexOfAny(new char[]{'\\','/',':','*','?','"','<','>','|'}) != -1 )
				{
					from.SendMessage("The filename can not contain the following special characters \\ / : * ? \" < > |");
					from.SendGump(new GSTemplateGump(gstb,from));
					return;
				}
				gstb.FileName = info.GetTextEntry(0).Text;
				GSTemplate.GSTempLoad( gstb );
				from.SendGump(new GSTemplateGump(gstb,from));
			}
			if( info.ButtonID == 2 )
			{
				if( info.GetTextEntry(1).Text == "" || gstb.Templates.ContainsKey(info.GetTextEntry(1).Text) )
				{
					from.SendMessage("You must specify a unique name for each template in this file.");
					from.SendGump(new GSTemplateGump(gstb,from));
					return;
				}
				if( gstb.Templates.Count == 40 )
				{
					from.SendMessage("Only 40 templates allowed per file. Create a new template file, or create room by overwriting or deleting a template and try again.");
					from.SendGump(new GSTemplateGump(gstb,from));
					return;
				}
				gstb.TemplateName = info.GetTextEntry(1).Text;
				from.SendMessage("Select the GigaSpawner you would like to copy.");
				from.Target = new InternalTarget(gstb);
			}
			if( info.ButtonID == 3 )
			{
				
				from.SendMessage("Select the GigaSpawner you would like to copy.");
				from.Target = new InternalTarget(gstb);
			}
			if( info.ButtonID == 4 )
			{
				if( gstb.TemplateName == null )
				{
					from.SendMessage("You must select a template before you can create a GigaSpawner.");
					from.SendGump(new GSTemplateGump(gstb,from));
					return;
				}
				GSTemplate.GSTempRead( gstb, from );
			}
			if( info.ButtonID == 5 )
			{
				if( gstb.TemplateName == null )
				{
					from.SendMessage("You must select the template you wish to delete.");
					from.SendGump(new GSTemplateGump(gstb,from));
					return;
				}
				GSTemplate.GSTempDelete(gstb);
				from.SendGump(new GSTemplateGump(gstb,from));
			}
			if( info.ButtonID == 6 )
			{
				from.SendGump( new GSComfirmGump( 4,0,gstb.FileName, null, gstb ) );
			}
			if( info.ButtonID > 9 )
			{
				gstb.TemplateName = (string)templates[info.ButtonID - 10];
				from.SendGump(new GSTemplateGump(gstb,from));
			}
		}
		
		private class InternalTarget : Target
		{
			private GSTemplateBook gstb;
			public InternalTarget(GSTemplateBook gs) : base( 15, false, TargetFlags.None )
			{
				gstb = gs;
			}
			
			protected override void OnTarget( Mobile from, object targeted )
			{
				if( targeted is GigaSpawner )
				{
					GigaSpawner gs = (GigaSpawner)targeted;
					GSTemplate.GSTempWrite( gs, gstb );
					from.CloseGump( typeof(GSTemplateGump) );
					from.SendGump(new GSTemplateGump(gstb,from));
				}
				else
					from.SendMessage("You can only use GigaSpawners as a template.");
			}
		}
	}
}
