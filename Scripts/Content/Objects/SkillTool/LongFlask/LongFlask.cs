using System;
using Server.Network;

namespace Server.Items
{
    [FlipableAttribute(0x1838, 0x1839, 0x183A)]
    public class LongFlask : Item
    {
        [Constructable]
        public LongFlask()
            : base(0x1838)
        {
            Weight = 1.0;
            Movable = true;
        }

        public LongFlask(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}