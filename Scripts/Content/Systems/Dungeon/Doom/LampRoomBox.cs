using System;
using System.Collections;
using Server;
using Server.Spells;
using Server.Mobiles;
using Server.Network;

namespace Server.Engines.Doom
{
    public class LampRoomBox : Item
    {
        private LeverPuzzleController m_Controller;
        private Mobile m_Wanderer;

        public LampRoomBox(LeverPuzzleController controller)
            : base(0xe80)
        {
            m_Controller = controller;
            ItemID = 0xe80;
            Movable = false;
        }

        public override void OnDoubleClick(Mobile m)
        {
            if (!m.InRange(this.GetWorldLocation(), 3))
                return;
            if (m_Controller.Enabled)
                return;

            if ((m_Wanderer == null || !m_Wanderer.Alive))
            {
                m_Wanderer = new WandererOfTheVoid();
                m_Wanderer.MoveToWorld(LeverPuzzleController.lr_Enter, Map.Malas);
                m_Wanderer.PublicOverheadMessage(MessageType.Regular, 0x3B2, 1060002, ""); // I am the guardian of...
                Timer.DelayCall(TimeSpan.FromSeconds(5.0), new TimerCallback(CallBackMessage));
            }
        }

        public void CallBackMessage()
        {
            PublicOverheadMessage(MessageType.Regular, 0x3B2, 1060003, ""); // You try to pry the box open...
        }
        public override void OnAfterDelete()
        {
            if (m_Controller != null && !m_Controller.Deleted)
                m_Controller.Delete();
        }
        public LampRoomBox(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
            writer.Write(m_Controller);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
            m_Controller = reader.ReadItem() as LeverPuzzleController;
        }
    }
}