using System; 
using Server; 

namespace Server.Items 
{ 

   public class RosePetals : Item 
   { 

      [Constructable] 
      public RosePetals() 
      { 

         Weight= 1.0;
	 Name = "rose petals";
	 Movable = true;

				if( Utility.Random( 100 ) < 50 ) 
				switch ( Utility.Random( 3 )) 
				{ 
          
         				case 0:
         				ItemID=6943; 
					break;

         				case 1:
         				ItemID=6944;
					break;

         				case 2:
         				ItemID=6945;
					break;
 
				}
				else
				{
         				ItemID=6946;
				} 

				if( Utility.Random( 100 ) < 50 ) 
				switch ( Utility.Random( 3 )) 
				{ 
          
         				case 0:
					Hue = 1150;
					break;

         				case 1:
					Hue = 55;
					break;

         				case 2:
					Hue = 30;
					break;
 
				}
				else
				{
					Hue = 38;
				} 
       
       
      }

      public RosePetals( Serial serial ) : base( serial ) 
      { 
      } 

      public override void Serialize( GenericWriter writer ) 
      { 
         base.Serialize( writer ); 

         writer.Write( (int) 0 ); 
      } 
       
      public override void Deserialize(GenericReader reader) 
      { 
         base.Deserialize( reader ); 

         int version = reader.ReadInt(); 
      } 
   }     
} 