using System;
using System.Collections.Generic;
using System.Text;
using Server;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines.MLQuests.Items
{
    public class TinkerSatchel : BaseCraftmansSatchel
    {
        [Constructable]
        public TinkerSatchel()
        {
            AddBaseLoot(Loot.MLArmorTypes, Loot.MLWeaponTypes, Loot.MLRangedWeaponTypes, Loot.JewelryTypes, m_TalismanType);

            if (Utility.RandomDouble() < 0.50)
            {
                // TODO: Add recipe drops (drops include inscription, alchemy and drops from non-artifact recipes for tailoring, blacksmith, carpentry and fletching.
                switch (Utility.Random(6))
                {
                    case 0: AddRecipe(DefInscription.CraftSystem); break;
                    case 1: AddRecipe(DefAlchemy.CraftSystem); break;
                    // TODO
                    //case 2: AddNonArtifactRecipe( DefTailoring.CraftSystem ); break;
                    //case 3: AddNonArtifactRecipe( DefBlacksmithy.CraftSystem ); break;
                    //case 4: AddNonArtifactRecipe( DefCarpentry.CraftSystem ); break;
                    //case 5: AddNonArtifactRecipe( DefBowFletching.CraftSystem ); break;
                }
            }
        }

        public TinkerSatchel(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}