using System;
using Server;

namespace Server.Items
{
    public class DarkChocolate : CandyCane
    {
        public override int LabelNumber { get { return 1079994; } } // Dark chocolate
        public override double DefaultWeight { get { return 1.0; } }

        [Constructable]
        public DarkChocolate()
            : base(0xF10)
        {
            Hue = 0x465;
            LootType = LootType.Regular;
        }

        public DarkChocolate(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}
