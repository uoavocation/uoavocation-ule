using System;
using Server.Items;
using Server.Mobiles;
using System.Collections;
using Server;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using Server.Spells;
using Server.ContextMenus;

namespace Server.Items
{
    [FlipableAttribute(0x13d5, 0x13dd)]
    public class FalconerGloves : BaseArmor
    {
        public override int BasePhysicalResistance { get { return 2; } }
        public override int BaseFireResistance { get { return 4; } }
        public override int BaseColdResistance { get { return 3; } }
        public override int BasePoisonResistance { get { return 3; } }
        public override int BaseEnergyResistance { get { return 4; } }

        public override int InitMinHits { get { return 35; } }
        public override int InitMaxHits { get { return 45; } }

        public override ArmorMaterialType MaterialType { get { return ArmorMaterialType.Studded; } }
        public override CraftResource DefaultResource { get { return CraftResource.RegularLeather; } }

        public override int AosStrReq { get { return 25; } }
        public override int OldStrReq { get { return 25; } }
        public override int ArmorBase { get { return 16; } }

        #region Falcon Integration [01-02]
        //Command Properties

        public Mobile Falconer;
        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public Mobile BoundFalcon
        {
            get
            {
                return Falconer;
            }
            set
            {
                Falconer = value;
                InvalidateProperties();
            }
        }

        public bool Used;
        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public bool falcon_created
        {
            get
            {
                return Used;
            }
            set
            {
                Used = value;
            }
        }

        public bool Active;
        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public bool Status
        {
            get
            {
                return Active;
            }
            set
            {
                Active = value;
            }
        }

        #endregion Edited By: A.A.S.R

        [Constructable]
        public FalconerGloves(): base(0x13D5)
        {
            Weight = 1.0;
            Name = "Falconer's Gloves";

            //Falcon Integration:
            falcon_created = false;
            BoundFalcon = null;
        }

        #region Falcon Integration [02-02]

//------Bonded Falcon Creation Spawn From Item----------//

        public void MakeFalcon(Mobile from)
        {
            Falcon newfalc;
            newfalc = new Falcon();
            newfalc.Map = from.Map;
            newfalc.Location = from.Location;
            newfalc.Controlled = true;
            newfalc.ControlMaster = from;
            this.BoundFalcon = newfalc;
            newfalc.Bound = this;
            newfalc.IsBonded = true;
            newfalc.ControlOrder = OrderType.Follow;
        }

//------Glove Behavior When They're Double-Clicked------//

        public override void OnDoubleClick(Mobile from)
        {
            if (BoundFalcon == null && this.falcon_created == true)
                this.Delete();
            else
            {
                if (Parent != from)
                {
                    from.SendMessage(22, "You should wear the gloves to call a falcon");
                }
                else
                {
                    if (this.falcon_created == false || BoundFalcon.Deleted) //Create Falcon
                    {
                        MakeFalcon(from);
                        this.falcon_created = true;
                        this.Status = false;
                    }
                    else if (this.falcon_created == true && this.Status == false) //Stable Falcon
                    {
                        FalconStable(from);
                        this.Name = "[Away]";
                        this.Status = true;
                    }
                    else if (this.falcon_created == true && this.Status == true && from.Followers + 1 <= from.FollowersMax) //Call Falcon
                    {
                        if (BoundFalcon.CheckAlive())
                        {
                            FalconClaim(from);
                            this.Name = "[Active]";
                        }
                        else
                        {
                            BoundFalcon.Resurrect();
                            BoundFalcon.Hits = BoundFalcon.HitsMax;
                        }
                        this.Status = false;
                    }
                    else if (this.falcon_created == true && this.Status == false)  //Cannot call error message
                        from.SendMessage("You need an available Control Slot for a Falcon");
                }
            }
        }

//------Glove Behavior When Equipped/Unequipped---------//

        public override void OnAdded(IEntity parent)
        {
            base.OnAdded(parent);
        }

        public override void OnRemoved(IEntity parent)
        {
            if (this.falcon_created == true)
            {
                base.OnRemoved(parent);
                if (parent is Mobile)
                    FalconStable((Mobile)parent);
            }
        }

//------Falcon Behavior When Glove Is Double-Clicked----//

        public void FalconStable(Mobile from)
        {
            BaseCreature falcon = BoundFalcon as BaseCreature;
            if (falcon.ControlOrder != OrderType.Attack)
            {
                Container pack = this.BoundFalcon.Backpack;
                BankBox bank = from.BankBox;
                ArrayList senditems = new ArrayList(pack.Items);
                foreach (Item item in senditems)
                {
                    if (item.Movable != false)
                    {
                        bank.DropItem(item);
                    }
                }

                this.BoundFalcon.Internalize();
                from.Stabled.Add(this.BoundFalcon);
                this.Name = this.BoundFalcon.Name;
                this.Status = true;
                from.SendMessage("The bird has flown it's items to the bank for you!");
            }
            else
                from.SendMessage("Your falcon cannot flee from battle");

        }

//------Falcon Behavior When The Bird Is Stabled--------//

        public void FalconClaim(Mobile from)
        {
            if (Deleted || !from.CheckAlive())
                return;


            int stabled = 0;
            for (int i = 0; i < from.Stabled.Count; ++i)
            {
                BaseCreature pet = from.Stabled[i] as BaseCreature;
                if (pet == null || pet.Deleted)
                {
                    pet.IsStabled = false;
                    from.Stabled.RemoveAt(i);
                    --i;
                    continue;
                }
                ++stabled;
                if ((from.Followers + pet.ControlSlots) <= from.FollowersMax && pet == this.BoundFalcon)
                {
                    pet.SetControlMaster(from);
                    if (pet.Summoned)
                        pet.SummonMaster = from;
                    pet.ControlTarget = from;
                    pet.ControlOrder = OrderType.Follow;
                    pet.Location = from.Location;
                    pet.Map = from.Map;
                    pet.IsStabled = false;
                    from.Stabled.RemoveAt(i);
                    --i;
                }
            }
        }

//------Falcon Behavior When The Bird Is Deleted--------//

        public override void OnDelete()
        {
            if (this.falcon_created == true)
            {
                if (this.BoundFalcon != null)
                    this.BoundFalcon.Delete();
            }

        }

        #endregion Edited By: A.A.S.R

        public FalconerGloves(Serial serial): base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)1);//Case Versioning

            //Falcon Integration
            writer.Write((Mobile)Falconer);
            writer.Write((bool)Used);
            writer.Write((bool)Active);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        //Falcon Integration
                        Falconer = reader.ReadMobile();
                        break;
                    }
                case 1:
                    {
                        //Falcon Integration
                        Falconer = reader.ReadMobile();
                        Used = reader.ReadBool();
                        Active = reader.ReadBool();
                        break;
                    }
            }
        }
    }
}