using System;

namespace Server.Items
{
	public class ChampagneGlass : Item
	{
		[Constructable]
		public ChampagneGlass() : base( 0x99a )
		{
			this.Weight = 0.1;
			Name = "a champagne glass";
			Hue = Utility.RandomList( 38, 64);
         		LootType=LootType.Blessed;
		}

		public ChampagneGlass( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}