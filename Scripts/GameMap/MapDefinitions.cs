using System;
using Server;

namespace Server.Misc
{
	public class MapDefinitions
	{
		public static void Configure()
		{
			/* Here we configure all maps. Some notes:
			 * 
			 * 1) The first 32 maps are reserved for core use.
			 * 2) Map 0x7F is reserved for core use.
			 * 3) Map 0xFF is reserved for core use.
			 * 4) Changing or removing any predefined maps may cause server instability.
			 */

			RegisterMap( 0, 0, 0, 7168, 4096, 4, "Felucca",		MapRules.FeluccaRules );
			RegisterMap( 1, 1, 1, 7168, 4096, 0, "Trammel",		MapRules.TrammelRules );
			RegisterMap( 2, 2, 2, 2304, 1600, 1, "Ilshenar",	MapRules.TrammelRules );
			RegisterMap( 3, 3, 3, 2560, 2048, 1, "Malas",		MapRules.TrammelRules );
			RegisterMap( 4, 4, 4, 1448, 1448, 1, "Tokuno",		MapRules.TrammelRules );
			RegisterMap( 5, 5, 5, 1280, 4096, 1, "TerMur",		MapRules.TrammelRules );

            #region UOAvocation - Ultima Live System [01-01]

            #region Map Addition Format
            /*
                (<index>, <mapID>, <fileIndex>, <width>, <height>, <season>, <name>, <rules>)
                      
                RegisterMap ( 32, 32, 32, 7168, 4096, 1, "UOLive32", MapRules.TrammelRules );
                RegisterMap ( 33, 33, 33, 7168, 4096, 0, "UOLive33", MapRules.TrammelRules );
            */
            #endregion


            #endregion Edited By: A.A.S.R

            RegisterMap( 0x7F, 0x7F, 0x7F, Map.SectorSize, Map.SectorSize, 1, "Internal", MapRules.Internal );

			TileMatrixPatch.Enabled = false; // OSI Client Patch 6.0.0.0
			MultiComponentList.PostHSFormat = true; // OSI Client Patch 7.0.9.0
		}

		public static void RegisterMap( int mapIndex, int mapID, int fileIndex, int width, int height, int season, string name, MapRules rules )
		{
			Map newMap = new Map( mapID, mapIndex, fileIndex, width, height, season, name, rules );

			Map.Maps[mapIndex] = newMap;
			Map.AllMaps.Add( newMap );
		}
	}
}