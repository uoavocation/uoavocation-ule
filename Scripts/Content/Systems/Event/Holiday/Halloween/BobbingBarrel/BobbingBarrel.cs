using System;
using System.Collections;
using System.Collections.Generic;
using Server;
using Server.Mobiles;
using Server.Targeting;
using Server.Items;
using Server.Network;

namespace Server.Items
{
    public class BobbingBarrel_Apples : Item
    {
        private Timer m_Timer;

        [Constructable]
        public BobbingBarrel_Apples(): base(0x154D)
        {
            Weight = 2.0;
            Name = "Apple Bobbing Barrel";
        }

        public override bool OnMoveOver(Mobile m)
        {
            return false;
        }

        public override void OnDoubleClick(Mobile from)
        {
            if (from.Mounted)
            {
                from.SendMessage("You cannot bob for apples while mounted.");
                return;
            }

            if (!from.InRange(GetWorldLocation(), 1)) from.LocalOverheadMessage(MessageType.Regular, 0x3B2, 1019045);

            else if (!from.CanBeginAction(typeof(BobbingBarrel_Apples))) 
                from.SendMessage("You all ready have your head under the water");

            else
            {
                from.BeginAction(typeof(BobbingBarrel_Apples));
                Timer.DelayCall(TimeSpan.FromSeconds(6.0), new TimerStateCallback(EndBobbing), from);
                
                from.SendMessage("You dunk your head in the water trying franticly to sink your teeth into an apple!");
                from.CantWalk = true;
                from.Direction = from.GetDirectionTo(this);
                
                from.Animate(32, 5, 1, true, true, 0);
                from.PlaySound(37);
            }
        }

        public static void EndBobbing(object state)
        {
            Mobile m = (Mobile)state;
            PlayerMobile pm = m as PlayerMobile;
            pm.CantWalk = false;

            if (pm != null && Utility.RandomDouble() <= .45)
            {
                switch (Utility.Random(4))
                {
                    case 3:
                    default: break;

                    case 2: pm.AddToBackpack(new BloodRedApples());
                        break;

                    case 1: pm.AddToBackpack(new ShinyRedApples());
                        break;

                    case 0: pm.AddToBackpack(new YummyRedApples());
                        break;
                }

                pm.SendMessage("You bite into an apple and pull your soaking wet head out of the water!");
                pm.PublicOverheadMessage(MessageType.Regular, 0xFE, false, "*" + pm.Name + " victoriously pulls an apple from the barrel using only their teeth!*");

            }
            else if (pm != null)
            {
                pm.SendMessage("You fail to bite into any of the apples in the barrel...");
                pm.PublicOverheadMessage(MessageType.Regular, 0xFE, false, "*" + pm.Name + " is soaking wet without an apple to show for it...*");
            }
                m.EndAction(typeof(BobbingBarrel_Apples));
        }

        public BobbingBarrel_Apples(Serial serial): base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)4); //Version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    #region BobbingBarrel_Apples Contents

    #region Yummy Red Apples

    public class YummyRedApples : Apple
    {
        [Constructable]
        public YummyRedApples() : this(1) 
        { 
        }

        [Constructable]
        public YummyRedApples(int amount): base(amount)
        {
            Name = "A Red Apple";
            Hue = 0x1E;
        }

        public YummyRedApples(Serial serial) : base(serial) 
        { 
        }

        public override void Serialize(GenericWriter writer) 
        { 
            base.Serialize(writer); 
            writer.Write((int)0); 
        }

        public override void Deserialize(GenericReader reader) 
        { 
            base.Deserialize(reader); 
            int version = reader.ReadInt(); 
        }
    }

    #endregion Edited By: A.A.R

    #region Shiny Red Apples

    public class ShinyRedApples : Apple
    {
        [Constructable]
        public ShinyRedApples() : this(1) 
        { 
        }

        [Constructable]
        public ShinyRedApples(int amount): base(amount)
        {
            Name = "A Shiny Red Apple";
            Hue = 0x1E;
        }

        public ShinyRedApples(Serial serial) : base(serial) 
        { 
        }

        public override void Serialize(GenericWriter writer) 
        { 
            base.Serialize(writer); 
            writer.Write((int)0); 
        }

        public override void Deserialize(GenericReader reader) 
        { 
            base.Deserialize(reader); 
            int version = reader.ReadInt(); 
        }
    }

    #endregion Edited By: A.A.R

    #region Blood Red Apples

    public class BloodRedApples : Apple
    {
        [Constructable]
        public BloodRedApples() : this(1) 
        { 
        }

        [Constructable]
        public BloodRedApples(int amount): base(amount)
        {
            Name = "A Red Apple";
            Hue = 0x1E;
        }

        public override void OnDoubleClick(Mobile from)
        {
            if (!from.Female)
            {
                from.SendMessage(89, "Something Sliced ThroughYour Lower Jaw !!!");
                Effects.PlaySound(from.Location, from.Map, 0x44A);
                BeginBleed(from);           
            }
            else
            {         
                from.SendMessage(89, "Something Sliced ThroughYour Lower Jaw !!!");
                Effects.PlaySound(from.Location, from.Map, 0x338);
                BeginBleed(from);
            }
        }

        private void BeginBleed(Mobile m)
        {
            if (m == null) 
                return;

            m.SendLocalizedMessage(1060160);
            BleedAttack.BeginBleed(m, m);

        }

        public BloodRedApples(Serial serial) : base(serial) 
        { 
        }

        public override void Serialize(GenericWriter writer) 
        { 
            base.Serialize(writer); 
            writer.Write((int)0); 
        }

        public override void Deserialize(GenericReader reader) 
        { 
            base.Deserialize(reader); 
            int version = reader.ReadInt(); 
        }
    }

    #endregion Edited By: A.A.R

    #endregion Edited By: A.A.R
}
