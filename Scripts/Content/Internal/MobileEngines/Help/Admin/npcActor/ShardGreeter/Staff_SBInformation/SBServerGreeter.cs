
#region Automated Server Staff Acknowledgements
/*
    This Automated NPC System Idea Originated
    With A Script Coded By: Tresdni from the
    RunUO (www.runuo.com) Forums; It Is Named:
    
       **Completely Automated Staff Team** 
 http://www.runuo.com/community/threads/completely-automated-staff-team-oh-yes-i-did.460720/
 
    This Released Version Of The Script Named
    Above Is My Own Variation On What I Think
    Might Complete The System Tresdni Started.
    
    The Code In This Script File Is Annoted. I
    Have Regioned Out Most Areas And Outlined
    Others So That You Know What Code Can Be
    Copy And Pasted To Other Scripts To Add The
    Same Functionality For Another System. 
 
    The Author Of Each Line Of Code Varies, I
    Got The Shell Of This Script From Tresdni,
    However A Lot Has Come From Many Other 
    Sources Over The Years; I Have A Library
    Of Annotated Methods I've Been Working On,
    That Help Me Build The Scripts I Upload.
 
    A Special Thank You Goes Out To The Following
    People For Helping Me Complete This System
    Addition To The Completely Automated Staff Team,
    Written By: Tresdni:
 
    THANK YOU GUYS!! THE HELP WAS MUCH APPRECIATED
                   -Sythen (A.A.R)-
    ______________________________________________
    ** JamzeMcC | Morexton | Soteric | James420 **
    ----------------------------------------------
 */
#endregion Edited By: A.A.R

using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Server;
using Server.Targeting;
using Server.Items;
using Server.Items.Staff;
using Server.ContextMenus;
using Server.Multis;
using Server.Spells;
using Server.Commands;
using Server.Gumps;
using Server.Mobiles;
using Server.Accounting;
using Server.Misc;
using Server.Network;


namespace Server.Mobiles
{
    public class SBServerGreeter : SBInfo
    {
        private List<GenericBuyInfo> m_BuyInfo = new InternalBuyInfo();
        private IShopSellInfo m_SellInfo = new InternalSellInfo();

        public SBServerGreeter()
        {
        }

        public override IShopSellInfo SellInfo { get { return m_SellInfo; } }
        public override List<GenericBuyInfo> BuyInfo { get { return m_BuyInfo; } }

        public class InternalBuyInfo : List<GenericBuyInfo>
        {
            public InternalBuyInfo()
            {
                Add(new GenericBuyInfo(typeof(RobeOfEntitlement), 0, 20, 0x2683, 0));
                Add(new GenericBuyInfo(typeof(CommUnit), 0, 20, 0x1F07, 0));
                Add(new GenericBuyInfo(typeof(StaffOfAnnulment), 0, 20, 0x13F8, 0));
                Add(new GenericBuyInfo(typeof(LensesOfResist), 0, 20, 0x2FB8, 0));
                Add(new GenericBuyInfo(typeof(CollarOfVisibility), 0, 20, 0x1F08, 0));
                Add(new GenericBuyInfo(typeof(RingOfReduction), 0, 20, 0x1F09, 0));
                Add(new GenericBuyInfo(typeof(BraceletOfEthics), 0, 20, 0x1F06, 0));
            }
        }

        public class InternalSellInfo : GenericSellInfo
        {
            public InternalSellInfo()
            {
                Add(typeof(RobeOfEntitlement), 0);
                Add(typeof(CommUnit), 0);
                Add(typeof(StaffOfAnnulment), 0);
                Add(typeof(LensesOfResist), 0);
                Add(typeof(CollarOfVisibility), 0);
                Add(typeof(RingOfReduction), 0);
                Add(typeof(BraceletOfEthics), 0);
            }
        }
    }
}