using System;
using System.Collections.Generic;
using System.Collections;
using Server;
using Server.ContextMenus;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using Server.Spells;
using Server.Mobiles;

namespace Server.Mobiles
{
    [CorpseName("an falcon corpse")]
    public class Falcon : FlyingCreature
    {
        //Bounding Property
        public Item Bound;

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public Item BoundFalconer
        {
            get { return Bound; }
            set
            {
                Bound = value;
                InvalidateProperties();
            }
        }

        [Constructable]
        public Falcon(): base(AIType.AI_FlyingPredator, FightMode.Aggressor, 10, 1, 0.1, 0.2)
        //public Falcon() : base( AIType.AI_Animal, FightMode.Aggressor, 10, 1, 0.1, 0.2 )
        {
            Name = "a falcon";
            Body = 5;
            Hue = 995;
            BaseSoundID = 0x2EE;

            SetStr(45, 65);
            SetDex(70, 160);
            SetInt(100, 120);

            SetHits(120, 150);
            SetMana(100);

            SetDamage(15, 30);

            SetDamageType(ResistanceType.Physical, 100);

            SetResistance(ResistanceType.Physical, 100, 125);
            SetResistance(ResistanceType.Fire, 10, 15);
            SetResistance(ResistanceType.Cold, 20, 25);
            SetResistance(ResistanceType.Poison, 5, 10);
            SetResistance(ResistanceType.Energy, 5, 10);

            SetSkill(SkillName.MagicResist, 100.1, 125.0);
            SetSkill(SkillName.Tactics, 18.1, 37.0);
            SetSkill(SkillName.Wrestling, 20.1, 60.0);

            Fame = 500;
            Karma = 0;

            VirtualArmor = 50;

            CanFlying = true;
            FlyStamMax = 800;
            FlyStam = 750;

            Tamable = true;
            ControlSlots = 1;
            MinTameSkill = 65.1;

            //Pack Animal's Backpack
            Container pack = Backpack;

            if (pack != null)
                pack.Delete();

            pack = new StrongBackpack();
            pack.Movable = false;
            AddItem(pack);
        }

        public override int Meat { get { return 1; } }
        public override MeatType MeatType { get { return MeatType.Bird; } }
        public override int Feathers { get { return 36; } }
        public override FoodType FavoriteFood { get { return FoodType.Meat | FoodType.Fish; } }

        //Falcon Skinning Method
        private Item m_Item;

        public override void OnThink()
        {
            Corpse toSkin = null;
            base.OnThink();
            Container pack = this.Backpack;
            foreach (Item item in this.GetItemsInRange(2))
            {
                int hideamount;
                hideamount = 0;
                if (item is ICarvable && item is Corpse)
                {
                    toSkin = (Corpse)item;

                    if (toSkin.Carved == false)
                        this.ControlMaster.SendMessage("Your bird rips the flesh from the corpse");

                    ((ICarvable)item).Carve(this, m_Item);
                    toSkin.Carved = true;

                    ArrayList lootitems = new ArrayList(item.Items);
                    foreach (Item xitem in lootitems)
                    {

                        if (xitem is Hides)
                        {
                            hideamount = xitem.Amount;
                            this.AddToBackpack(new Leather(hideamount));
                            xitem.Delete();
                        }
                        else if (xitem is SpinedHides)
                        {
                            hideamount = xitem.Amount;
                            this.AddToBackpack(new SpinedLeather(hideamount));
                            xitem.Delete();
                        }
                        else if (xitem is HornedHides)
                        {
                            hideamount = xitem.Amount;
                            this.AddToBackpack(new HornedLeather(hideamount));
                            xitem.Delete();
                        }
                        else if (xitem is BarbedHides)
                        {
                            hideamount = xitem.Amount;
                            this.AddToBackpack(new BarbedLeather(hideamount));
                            xitem.Delete();
                        }
                        else if (xitem is Gold)
                        {
                            pack.DropItem(xitem);
                        }
                    }
                }
            }
        }

        //Pack Animal Method
        public override bool OnBeforeDeath()
        {
            if (!base.OnBeforeDeath())
                return false;

            PackAnimal.CombineBackpacks(this);

            return true;
        }

        public override DeathMoveResult GetInventoryMoveResultFor(Item item)
        {
            return DeathMoveResult.MoveToCorpse;
        }

        public override bool IsSnoop(Mobile from)
        {
            if (PackAnimal.CheckAccess(this, from))
                return false;

            return base.IsSnoop(from);
        }

        public override bool OnDragDrop(Mobile from, Item item)
        {
            BankBox bank = from.BankBox;
            if (CheckFeed(from, item))
                return true;

            if (PackAnimal.CheckAccess(this, from))
            {
                if (item.Stackable == false && item.PileWeight > 10)
                {
                    from.Backpack.DropItem(item);
                    from.SendMessage("That is too heavy, give the bird less weight to carry.");
                }
                if (this.ControlMaster == from && item.Weight <= 10)
                {
                    bank.DropItem(item);
                    from.SendMessage("The bird has flown your item to the bank.");
                }
                else if (this.ControlMaster == from && item.Weight > 10)
                {
                    from.Backpack.DropItem(item);
                    from.SendMessage("That is too heavy for the bird to carry.");
                }
                else
                {
                    from.Backpack.DropItem(item);
                    from.SendMessage("The bird will only follow it's master's orders.");
                }
                return true;
            }

            return base.OnDragDrop(from, item);
        }

        public override bool CheckNonlocalDrop(Mobile from, Item item, Item target)
        {
            return PackAnimal.CheckAccess(this, from);
        }

        public override bool CheckNonlocalLift(Mobile from, Item item)
        {
            return PackAnimal.CheckAccess(this, from);
        }

        public override void OnDoubleClick(Mobile from)
        {
            PackAnimal.TryPackOpen(this, from);
        }

        public override void GetContextMenuEntries(Mobile from, List<ContextMenuEntry> list) // changes for 2.0
        {
            base.GetContextMenuEntries(from, list);

            PackAnimal.GetContextMenuEntries(this, from, list);
        }

        public Falcon(Serial serial): base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0);

            //Bounding Property
            writer.Write((Item)Bound);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();

            //Bounding Property
            Bound = reader.ReadItem();
        }
    }
}