using System;
using System.Collections.Generic;
using Server;
using Server.Accounting;
using Server.Items;

namespace Server.Misc
{
    #region UOAvocation - GiftGiving.cs Script Format
    /*
        using System;
        using Server;
        using Server.Items;

        namespace Server.Misc
        {
	        public class WinterGiftGiver2004 : GiftGiver
	        {
		        public static void Initialize()
		        {
			        GiftGiving.Register( new WinterGiftGiver2004() );
		        }

		        public override DateTime Start{ get{ return new DateTime( 2004, 12, 24 ); } }
		        public override DateTime Finish{ get{ return new DateTime( 2005, 1, 1 ); } }

		        public override void GiveGift( Mobile mob )
		        {
			        GiftBox box = new GiftBox();

			        box.DropItem( new MistletoeDeed() );
			        box.DropItem( new PileOfGlacialSnow() );
			        box.DropItem( new LightOfTheWinterSolstice() );

			        int random = Utility.Random( 100 );

			        if ( random < 60 )
				        box.DropItem( new DecorativeTopiary() );
			        else if ( random < 84 )
				        box.DropItem( new FestiveCactus() );
			        else
				        box.DropItem( new SnowyTree() );

			        switch ( GiveGift( mob, box ) )
			        {
				        case GiftResult.Backpack:
					        mob.SendMessage( 0x482, "Happy Holidays from the team!  Gift items have been placed in your backpack." );
					        break;
				        case GiftResult.BankBox:
					        mob.SendMessage( 0x482, "Happy Holidays from the team!  Gift items have been placed in your bank box." );
					        break;
			        }
		        }
	        }
        }
    */
    #endregion Edited By: A.A.S.R

    public enum GiftResult
    {
        Backpack,
        BankBox
    }

    public class GiftGiving
    {
        private static List<GiftGiver> m_Givers = new List<GiftGiver>();

        public static void Register(GiftGiver giver)
        {
            m_Givers.Add(giver);
        }

        public static void Initialize()
        {
            EventSink.Login += new LoginEventHandler(EventSink_Login);
        }

        private static void EventSink_Login(LoginEventArgs e)
        {
            Account acct = e.Mobile.Account as Account;

            if (acct == null)
                return;

            DateTime now = DateTime.UtcNow;

            for (int i = 0; i < m_Givers.Count; ++i)
            {
                GiftGiver giver = m_Givers[i];

                if (now < giver.Start || now >= giver.Finish)
                    continue; // not in the correct timefream

                if (acct.Created > (giver.Start - giver.MinimumAge))
                    continue; // newly created account

                if (acct.LastLogin >= giver.Start)
                    continue; // already got one

                giver.DelayGiveGift(TimeSpan.FromSeconds(5.0), e.Mobile);
            }

            acct.LastLogin = now;
        }
    }

    public abstract class GiftGiver
    {
        public virtual TimeSpan MinimumAge { get { return TimeSpan.FromDays(30.0); } }

        public abstract DateTime Start { get; }
        public abstract DateTime Finish { get; }
        public abstract void GiveGift(Mobile mob);

        public virtual void DelayGiveGift(TimeSpan delay, Mobile mob)
        {
            Timer.DelayCall(delay, new TimerStateCallback(DelayGiveGift_Callback), mob);
        }

        protected virtual void DelayGiveGift_Callback(object state)
        {
            GiveGift((Mobile)state);
        }

        public virtual GiftResult GiveGift(Mobile mob, Item item)
        {
            if (mob.PlaceInBackpack(item))
            {
                if (!WeightOverloading.IsOverloaded(mob))
                    return GiftResult.Backpack;
            }

            mob.BankBox.DropItem(item);
            return GiftResult.BankBox;
        }
    }
}