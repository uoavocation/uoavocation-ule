using System;
using System.Collections.Generic;
using System.Text;
using Server;
using Server.Items;

namespace Server.Engines.MLQuests.Items
{
    public class LargeBagOfTreasure : Bag
    {
        [Constructable]
        public LargeBagOfTreasure()
        {
            RewardBag.Fill(this, 4, 0.50);
        }

        public LargeBagOfTreasure(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}