using System;
using Server.Network;

namespace Server.Items
{
    public class YellowBeaker : Item
    {
        [Constructable]
        public YellowBeaker()
            : base(0x182E)
        {
            Weight = 1.0;
            Movable = true;
        }

        public YellowBeaker(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    } 
}