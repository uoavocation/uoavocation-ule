using System;
using System.Collections.Generic;
using Server.ContextMenus;
using Server.Regions;
using Server.Items;
using BunnyHole = Server.Mobiles.VorpalBunny.BunnyHole;

namespace Server.Mobiles
{
    public class BaseTalismanSummon : BaseCreature
    {
        public override bool Commandable { get { return false; } }
        public override bool InitialInnocent { get { return true; } }
        //public override bool IsInvulnerable{ get{ return true; } } // TODO: Wailing banshees are NOT invulnerable, are any of the others?

        public BaseTalismanSummon()
            : base(AIType.AI_Melee, FightMode.None, 10, 1, 0.2, 0.4)
        {
            // TODO: Stats/skills
        }

        public BaseTalismanSummon(Serial serial)
            : base(serial)
        {
        }

        public override void AddCustomContextEntries(Mobile from, List<ContextMenuEntry> list)
        {
            if (from.Alive && ControlMaster == from)
                list.Add(new TalismanReleaseEntry(this));
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.WriteEncodedInt(0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadEncodedInt();
        }

        private class TalismanReleaseEntry : ContextMenuEntry
        {
            private Mobile m_Mobile;

            public TalismanReleaseEntry(Mobile m)
                : base(6118, 3)
            {
                m_Mobile = m;
            }

            public override void OnClick()
            {
                Effects.SendLocationParticles(EffectItem.Create(m_Mobile.Location, m_Mobile.Map, EffectItem.DefaultDuration), 0x3728, 8, 20, 5042);
                Effects.PlaySound(m_Mobile, m_Mobile.Map, 0x201);

                m_Mobile.Delete();
            }
        }
    }
}