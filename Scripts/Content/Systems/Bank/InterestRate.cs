using System;
using System.Collections;
using System.Collections.Generic;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Accounting;

namespace Server.Misc
{
    public class interestTimer : Timer
    {
        public static void Initialize()
        {
            new interestTimer();
        }

        public interestTimer(): base(TimeSpan.FromMinutes(1), TimeSpan.FromHours(1))
        {
            this.Start();
        }

        protected override void OnTick()
        {
            //Grants Interest At 11:00 PM Server Time
            if (DateTime.Now.Hour == 23)
            {
                Console.WriteLine("Interest On All Currency Has Been Distributed To All Active Accounts");

                World.Broadcast(0x22, true, "Your bank has just given you interest on all of your gold!");

                ArrayList bankBoxes = new ArrayList();

                #region Do Not Allow Interest If Account Has Not Logged In 30+ Days

                TimeSpan inttime = TimeSpan.FromDays(30);

                #endregion

                foreach (Mobile mob in World.Mobiles.Values)
                {
                    if (mob is PlayerMobile)
                    {
                        Account acct = mob.Account as Account;

                        //Checks To See If Players Have A Bankbox, Whether Or Not Their Banned, And Checks Their Last Login Date/Time
                        if (mob.BankBox != null && (((PlayerMobile)mob).LastOnline + inttime) > DateTime.UtcNow && !acct.Banned && mob.AccessLevel == AccessLevel.Player)                     
                        {
                            bankBoxes.Add(mob.BankBox);
                        }
                    }
                }

                foreach (BankBox ibb in bankBoxes)
                {
                    int totalGold = 0;
                    int interest = 0;

                    #region Currency Object Type That Gains Interest

                    totalGold += ibb.GetAmount(typeof(Gold));

                    #endregion

                    ArrayList bankchecks = new ArrayList(ibb.FindItemsByType(typeof(BankCheck)));

                    foreach (BankCheck bcint in bankchecks)
                    {
                        totalGold += bcint.Worth;
                    }

                    if (totalGold > 0)

                        //Interest = 5 Coins Per Every 1000
                        interest = (int)(totalGold * 0.005);

                    Item interestitem = null;

                    if (interest > 0)
                    {
                        //If Interest Given Is 60,000 Coins Or Less, Give A Coin Pile
                        if (interest <= 60000)
                        {
                            interestitem = (new Gold(interest));
                        }

                        //If Interest Given Is 60,001+ Coins Or More, Give A Bankcheck
                        else 
                        {
                            interestitem = (new BankCheck(interest));
                        }
                    }

                    if (interestitem != null)
                    ((Container)ibb).DropItem(interestitem);

                    bankchecks.Clear();
                }

                bankBoxes.Clear();
            }
        }
    }
}