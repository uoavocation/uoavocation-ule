using System;
using Server;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
	public enum BirthdayBellType
	{
		Adrick,
		Alai,
		Bulldoz,
		Evocare,
		FierYiCe,
		Greyburn,
		Hanse,
		Ignatz,
		Jelek,
		LadyMOl,
		LordKrum,
		Melantus,
		Nimrond,
		Oaks,
		Prophet,
		Runesabre,
		Sage,
		Stellerex,
		TBone,
		Tajima,
		Tyrant,
		Vex
	}

	public enum BirthdayBellSound
	{
		Sound1,
		Sound2,
		Sound3,
		Sound4,
		Sound5,
		Sound6
	}

	public class BirthdayBell : Item
	{
		private BirthdayBellType m_Type;
		private BirthdayBellSound m_Sound;

		[CommandProperty( AccessLevel.GameMaster )]
		public BirthdayBellType Type
		{
			get{ return m_Type; }
			set{ m_Type = value; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public BirthdayBellSound Sound
		{
			get{ return m_Sound; }
			set{ m_Sound = value; }
		}

		[Constructable]
		public BirthdayBell() : base( 0x1C12 )
		{

			BirthdayBellType randomtype = (BirthdayBellType)Utility.Random((int)BirthdayBellType.Vex+1);

			m_Type = randomtype;

			BirthdayBellSound randomsound = (BirthdayBellSound)Utility.Random((int)BirthdayBellSound.Sound6+1);

			m_Sound = randomsound;

			LootType = LootType.Blessed;

			Hue = Utility.RandomList( 1150 , 55, 65, 75, 85, 95, 105, 115, 125, 135, 145, 30, 35, 37 );
		}

      		public override void OnDoubleClick( Mobile from ) 
      		{  
			if ( m_Sound == BirthdayBellSound.Sound1 )
			{
				from.PlaySound( 0x100 );
			}
			else if ( m_Sound == BirthdayBellSound.Sound2 )
			{
				from.PlaySound( 0x101 );
			}
			else if ( m_Sound == BirthdayBellSound.Sound3 )
			{
				from.PlaySound( 0x103 );
			}
			else if ( m_Sound == BirthdayBellSound.Sound4 )
			{
				from.PlaySound( 0x104 );
			}
			else if ( m_Sound == BirthdayBellSound.Sound5 )
			{
				from.PlaySound( 0x16 );
			}
			else if ( m_Sound == BirthdayBellSound.Sound6 )
			{
				from.PlaySound( 0x428 );
			}
			else
			{
				from.SendMessage( "INTERNAL ERROR: Please Contact A Gamemaster About Your Bell." );
			}
      		}

		public BirthdayBell( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version

			writer.WriteEncodedInt( (int) m_Type );

			writer.WriteEncodedInt( (int) m_Sound );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
			
			m_Type = (BirthdayBellType)reader.ReadEncodedInt();

			m_Sound = (BirthdayBellSound)reader.ReadEncodedInt();
		}

		public override void AddNameProperty(ObjectPropertyList list)
		{
			if ( m_Type == BirthdayBellType.Adrick )
			{
				list.Add( "A Birthday Bell from Adrick" );
			}
			else if ( m_Type == BirthdayBellType.Alai )
			{
				list.Add( "A Birthday Bell from Alai" );
			}
			else if ( m_Type == BirthdayBellType.Bulldoz )
			{
				list.Add( "A Birthday Bell from Bulldoz" );
			}
			else if ( m_Type == BirthdayBellType.Evocare )
			{
				list.Add( "A Birthday Bell from Evocare" );
			}
			else if ( m_Type == BirthdayBellType.FierYiCe )
			{
				list.Add( "A Birthday Bell from FierY iCe" );
			}
			else if ( m_Type == BirthdayBellType.Greyburn )
			{
				list.Add( "A Birthday Bell from Greyburn" );
			}
			else if ( m_Type == BirthdayBellType.Hanse )
			{
				list.Add( "A Birthday Bell from Hanse" );
			}
			else if ( m_Type == BirthdayBellType.Ignatz )
			{
				list.Add( "A Birthday Bell from Ignatz" );
			}
			else if ( m_Type == BirthdayBellType.Jelek )
			{
				list.Add( "A Birthday Bell from Jelek" );
			}
			else if ( m_Type == BirthdayBellType.LadyMOl )
			{
				list.Add( "A Birthday Bell from LadyMOl" );
			}
			else if ( m_Type == BirthdayBellType.LordKrum )
			{
				list.Add( "A Birthday Bell from Lord Krum" );
			}
			else if ( m_Type == BirthdayBellType.Melantus )
			{
				list.Add( "A Birthday Bell from Melantus" );
			}
			else if ( m_Type == BirthdayBellType.Nimrond )
			{
				list.Add( "A Birthday Bell from Nimrond" );
			}
			else if ( m_Type == BirthdayBellType.Oaks )
			{
				list.Add( "A Birthday Bell from Oaks" );
			}
			else if ( m_Type == BirthdayBellType.Prophet )
			{
				list.Add( "A Birthday Bell from Prophet" );
			}
			else if ( m_Type == BirthdayBellType.Runesabre )
			{
				list.Add( "A Birthday Bell from Runesabre" );
			}
			else if ( m_Type == BirthdayBellType.Sage )
			{
				list.Add( "A Birthday Bell from Sage" );
			}
			else if ( m_Type == BirthdayBellType.Stellerex )
			{
				list.Add( "A Birthday Bell from Stellerex" );
			}
			else if ( m_Type == BirthdayBellType.TBone )
			{
				list.Add( "A Birthday Bell from TBone" );
			}
			else if ( m_Type == BirthdayBellType.Tajima )
			{
				list.Add( "A Birthday Bell from Tajima" );
			}
			else if ( m_Type == BirthdayBellType.Tyrant )
			{
				list.Add( "A Birthday Bell from Tyrant" );
			}
			else if ( m_Type == BirthdayBellType.Vex )
			{
				list.Add( "A Birthday Bell from Vex" );
			}
			else
			{
				list.Add( "A Birthday Bell" );
			}
		}
	}
}