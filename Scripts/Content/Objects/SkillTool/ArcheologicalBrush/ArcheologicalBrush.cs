/* Created by Hammerhand*/

using System;
using Server;
using Server.Items;
using Server.Targeting;
using Server.Network;
using Server.Commands;
using Server.Engines.Harvest;
using Server.Mobiles;
using Server.Regions;
using Server.Gumps;

namespace Server.Items
{
    public class ArcheologistsBrush : BaseHarvestTool, IUsesRemaining
	{
        public override HarvestSystem HarvestSystem { get { return Archeology.System; } }

		[Constructable]
		public ArcheologistsBrush() : this( 10 )
		{
		}

		[Constructable]
        public ArcheologistsBrush(int uses) : base(4978)
        {
            Name = "an Archeologists Brush";
            UsesRemaining = uses;
            ShowUsesRemaining = true;
        }
        public override void OnDoubleClick(Mobile from)
        {
            #region Archeological Region Toggle [01-02]
            /*
                if (from.Region.IsPartOf(typeof(Regions.ArcheologicalDigRegion)))
                {
            */
            #endregion Edited By: Hammerhand

                if (IsChildOf(from.Backpack) || Parent == from)
                    HarvestSystem.BeginHarvesting(from, this);
                else
                    from.SendLocalizedMessage(1042001); // That must be in your pack for you to use it.

            #region Archeological Region Toggle [02-02]
            /*
                }  
                else 
                    from.SendMessage("You cant dig here."); // You cant dig here.
            */
            #endregion Edited By: Hammerhand
        } 
        public ArcheologistsBrush(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
			ShowUsesRemaining = true;
		}
	}
}