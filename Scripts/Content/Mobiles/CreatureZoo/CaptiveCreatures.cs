#region Script Acknowledgements
/*
    Passive zoo animals by Alari (alarihyena@gmail.com)
    a Zoo<MOB> constructor for each zoo critter that sets 
    them to passive and makes them non-tameable.
*/
#endregion Edited By: A.A.R

using System;
using Server;
using Server.Mobiles;

namespace Server.Mobiles.Zoo
{
    #region Aquatic Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Arctic Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Champion Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Desert Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Forest Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Grassland Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Grave Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Jungle Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Mountain Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Peerless Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Urban Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Volcano Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R

    #region Wetland Regions

    #region Zoo Creature List
    /*
        Sample    
    */
    #endregion

    #region Sample_Exhibition
    /*
    public class Zoo_Creature : Creature
	{
		[Constructable]
		public Zoo_Creature() : base()
		{
			FightMode = FightMode.None;
			Tamable = false; Karma = 0; Fame = 0; Blessed = true;
		}

		public Zoo_Creature( Serial serial ) : base ( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
    */
    #endregion

    #endregion Edited By: A.A.R
}