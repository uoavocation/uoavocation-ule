using System;
using Server.Items;

namespace Server.Items
{
	public class RegStone_Necromancy : Item
	{
		public override string DefaultName
		{
			get { return "necromancy regeant stone"; }
		}

		[Constructable]
		public RegStone_Necromancy() : base( 0xED4 )
		{
			Movable = false;
			Hue = 0x2D1;
		}

		public override void OnDoubleClick( Mobile from )
		{
            BagOfNecroReagents regBag = new BagOfNecroReagents(50);

			if ( !from.AddToBackpack( regBag ) )
				regBag.Delete();
		}

		public RegStone_Necromancy( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}