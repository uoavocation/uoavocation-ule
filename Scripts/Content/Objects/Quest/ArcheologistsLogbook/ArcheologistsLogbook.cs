/* Created by Hammerhand*/

using System;
using Server;
using Server.Items;
using Server.Targeting;
using Server.Network;
using Server.Commands;
using Server.Engines.Harvest;
using Server.Mobiles;
using Server.Regions;
using Server.Gumps;

namespace Server.Items
{
	public class ArcheologistsLogbook : Item
	{
		[Constructable]
        public ArcheologistsLogbook()
            : base(0xFF4)
		{
            Name = "The Logbook of John Aubrey";
			Weight = 1.0;
            Hue = 1169;
		}

        public ArcheologistsLogbook(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}