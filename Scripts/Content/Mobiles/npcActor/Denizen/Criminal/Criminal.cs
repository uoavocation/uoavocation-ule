using System;
using Server;
using Server.Items;
using System.Collections;

namespace Server.Mobiles
{
    [CorpseName("a criminal's corpse")]
    public class Criminal : BaseCreature
    {
        private static bool m_Talked; // flag to prevent spam 

        string[] kfcsay = new string[] // things to say while greating 
      { 
         "Thou froward beef-witted codpiece!", 
         "No word to save thee.",   
         "How now, wool-sack, what mutter you?",
         "If the cook help to make the gluttony, you help to make the diseases.",
         "Thou elvish-mark'd, abortive, rooting hog!",
         "Thou wilt be as valiant as the wrathful dove, or most magnanimous mouse.",
         "Thou mangled boil-brained foot-licker!",
         "Thou art the rudeliest welcome to this world.",
         "God hath given you one face and you make yourselves another.",
         "Thou wimpled whoreson joithead!",
         "Thou art some fool, I am loath to beat thee.",
         "Thou venomed spur-galled knave!",
         "Thou saucy clay-brained hedge-pig!",
         "Your bait of falsehood takes this carp of truth.",
         "I throw thy name against the bruising stones.",
         "I'll pray a thousand prayers for thy death.",
         "Thou mangled milk-livered scut!",
         "Your means are very slender, and your waste is great.",
         "In civility thou seem'st so empty.",
         "Thou pribbling hasty-witted knave!",
         "You are a shallow cowardly hind, and you lie.",
         "What a drunken knave was the sea to cast thee in our way!",
         "Come, come, you talk greasily; your lips grow foul.",
         "Thou bootless milk-livered flax-wench!",
         "Hence rotten thing! Or I shall shake thy bones out of thy garments.",  
      };

        [Constructable]
        public Criminal()
            : base(AIType.AI_Predator, FightMode.Closest, 10, 1, 0.2, 0.4)
        {
            Body = 400;
            Hue = Utility.RandomSkinHue();

            if (Female = Utility.RandomBool())
            {
                Body = 401;
                Name = NameList.RandomName("female");
            }
            else
            {
                Name = NameList.RandomName("male");
            }

            ActiveSpeed = 0.4;

            SetDamage(10, 23);

            SetStr(80, 100);
            SetDex(80, 100);
            SetInt(80, 100);

            SetHits(3000);

            Fame = 0;
            Karma = -1000;

            SetSkill(SkillName.Anatomy, 50.1, 100.0);
            SetSkill(SkillName.Healing, 50.1, 100.0);
            SetSkill(SkillName.Tactics, 50.1, 100.0);
            SetSkill(SkillName.MagicResist, 50.1, 100.0);
            SetSkill(SkillName.Macing, 50.1, 100.0);
            SetSkill(SkillName.Wrestling, 50.1, 100.0);

            SetResistance(ResistanceType.Physical, 80, 90);
            SetResistance(ResistanceType.Fire, 40, 50);
            SetResistance(ResistanceType.Cold, 40, 50);
            SetResistance(ResistanceType.Poison, 40, 50);
            SetResistance(ResistanceType.Energy, 40, 50);

            Item sandals = new Sandals(702);
            sandals.Movable = false;
            AddItem(sandals);

            AddItem(new ShortPants(702));
            AddItem(new SkullCap(702));

            Item BlackStaff = new BlackStaff();
            BlackStaff.Movable = false;
            AddItem(BlackStaff);

            Utility.AssignRandomHair(this);

            Item Vandyke = new Vandyke(2125);
            Vandyke.Movable = false;
            AddItem(Vandyke);

            VirtualArmor = 100;

            Tamable = false; // You Can't Tame A Troublemaker!		 
        }

        public override bool CanRummageCorpses { get { return true; } }
        public override Poison PoisonImmune { get { return Poison.Lethal; } }
        public override int TreasureMapLevel { get { return 3; } }

        public override void GenerateLoot()
        {
            Container pack = new Backpack();
            pack.DropItem(new Gold(1000, 2000));
            pack.DropItem(new Bandage(Utility.RandomMinMax(50, 100)));
            pack.Movable = false;
            AddItem(pack);

            AddLoot(LootPack.Rich, 3);
            AddLoot(LootPack.Average);
            AddLoot(LootPack.MedScrolls, 2);
        }

        public override void OnMovement(Mobile m, Point3D oldLocation)
        {
            if (m_Talked == false)
            {
                if (m.InRange(this, 4))
                {
                    m_Talked = true;
                    SayRandom(kfcsay, this);
                    this.Move(GetDirectionTo(m.Location));

                    // Start timer to prevent spam 
                    SpamTimer t = new SpamTimer();
                    t.Start();
                }
            }
        }

        #region Troublemaker SpamTimer

        private class SpamTimer : Timer
        {
            public SpamTimer()
                : base(TimeSpan.FromSeconds(8))
            {
                Priority = TimerPriority.OneSecond;
            }

            protected override void OnTick()
            {
                m_Talked = false;
            }
        }

        #endregion

        private static void SayRandom(string[] say, Mobile m)
        {
            m.Say(say[Utility.Random(say.Length)]);
        }

        public Criminal(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}
