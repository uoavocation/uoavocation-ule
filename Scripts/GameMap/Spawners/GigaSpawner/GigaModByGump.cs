using System;
using System.Collections;
using Server;
using Server.Gumps;
using Server.GigaSpawners;
using Server.Network;

namespace Server.Gumps
{
	public class GigaModByGump : Gump
	{
		private GigaSpawner m_spawner;
		public GigaModByGump(GigaSpawner spawner) : base( 0, 0 )
		{
			int mod = spawner.ModifiedBy.Count;
			m_spawner = spawner;
			
			AddBackground(50, 50, 390, 90 + 30 * mod, 3000);
			AddBackground(55, 75, 380, 25 + 30 * mod, 3500);
			AddLabel(100, 55, 0, "Name");
			AddLabel(175, 55, 0, "Times");
			AddLabel(280, 55, 0, "Last Time");
			mod = 0;
			string[] temp;
			foreach (DictionaryEntry de in spawner.ModifiedBy)
			{
				temp = ((string)de.Value).Split(',');
				AddLabelCropped( 85, 95 + 20 * mod, 120, 20, 0, ((Mobile)de.Key).Name );
				AddLabel(185, 95 + 20 * mod, 0, temp[0]);
				AddLabel(240, 95 + 20 * mod, 0, temp[1]);
				mod++;
			}
			
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			from.SendGump( new GigaMainGump(m_spawner,from) );
		}
	}
}
