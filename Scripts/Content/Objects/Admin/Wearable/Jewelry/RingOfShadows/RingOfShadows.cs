using System;
using Server;
using Server.Items;
using System.Collections.Generic;
using Server.Network;
using Server.ContextMenus;

namespace Server.Items.Staff
{

	public class RingOfShadows : BaseRing
	{

		private Mobile m_Owner;
		private AccessLevel m_StaffLevel;
		public Point3D m_HomeLocation;
		public Map m_HomeMap;

		[Constructable]
		public RingOfShadows() : base( 0x108a )
		{
			
			LootType = LootType.Blessed;
			Weight = 0;
			Hue = 2406;
			Name = "An Unassigned Ring of Shadows";
			Attributes.NightSight = 1;
			Attributes.LowerRegCost = 100;
			Attributes.LowerManaCost = 100;
			Attributes.RegenHits = 100;
			Attributes.RegenStam = 100;
			Attributes.RegenMana = 100;
			Attributes.CastRecovery = 10;
			Attributes.CastSpeed = 10;

		}

		public RingOfShadows( Serial serial ) : base( serial )
		{
		}


		[CommandProperty( AccessLevel.Counselor, AccessLevel.GameMaster )]
		public Point3D HomeLocation
		{
			get
			{
				return m_HomeLocation;
			}
			set
			{
				m_HomeLocation = value;
			}
		}

		[CommandProperty( AccessLevel.Counselor, AccessLevel.GameMaster )]
		public Map HomeMap
		{
			get
			{
				return m_HomeMap;
			}
			set
			{
				m_HomeMap = value;
			}
		}

		private class GoHomeEntry : ContextMenuEntry
		{
			private RingOfShadows m_Item;
			private Mobile m_Mobile;

			public GoHomeEntry( Mobile from, Item item ) : base( 5134 ) // uses "Goto Loc" entry
			{
				m_Item = (RingOfShadows)item;
				m_Mobile = from;
			}

			public override void OnClick()
			{
				// go to home location
				m_Mobile.Location = m_Item.HomeLocation;
				if ( m_Item.HomeMap != null )
					m_Mobile.Map = m_Item.HomeMap;
			}
		}

		private class SetHomeEntry : ContextMenuEntry
		{
			private RingOfShadows m_Item;
			private Mobile m_Mobile;

			public SetHomeEntry( Mobile from, Item item ) : base( 2055 ) // uses "Mark" entry
			{
				m_Item = (RingOfShadows)item;
				m_Mobile = from;
			}

			public override void OnClick()
			{
				// set home location
				m_Item.HomeLocation = m_Mobile.Location;
				m_Item.HomeMap = m_Mobile.Map;
				m_Mobile.SendMessage( "The home location on your ring has been set to your current position." );
			}
		}
		 
		public static void GetContextMenuEntries( Mobile from, Item item, List<ContextMenuEntry> list ) 
		  
		//public static void GetContextMenuEntries( Mobile from, Item item, ArrayList list ) //Replaced With Above Line

		{
			list.Add( new GoHomeEntry( from, item ) );
			list.Add( new SetHomeEntry( from, item ) );
		}

		public override void GetContextMenuEntries( Mobile from, List<ContextMenuEntry> list )

		//public override void GetContextMenuEntries(Mobile from, ArrayList list) //Replaced With Above Line

		{
			if ( m_Owner == null )
			{
				return;
			}
			else
			{
				if ( m_Owner != from )
				{
					from.SendMessage( "This is not yours to use." );
					return;
				}
				else
				{
					base.GetContextMenuEntries( from, list );
					RingOfShadows.GetContextMenuEntries( from, this, list );
				}
			}
		}

		public override void OnDoubleClick(Mobile from)
		{
			// set owner if not already set -- this is only done the first time.
			if ( m_Owner == null )
			{
				m_Owner = from;
				this.Name = m_Owner.Name.ToString() + "'s Ring of Shadows";
				this.HomeLocation = from.Location;
				this.HomeMap = from.Map;
				from.SendMessage( "This ring has been assigned to you." );
			}
			else
			{
				if ( m_Owner != from )
				{
					from.SendMessage( "This is not yours to use." );
					return;
				}
				else
				{
					SwitchAccessLevels( from );
				}
					
			}
		}

		private void SwitchAccessLevels( Mobile from )
		{
			// check current access level
			if ( from.AccessLevel == AccessLevel.Player )
			{
				// return to staff status
				from.AccessLevel = m_StaffLevel;
				from.Blessed = true;
			}
			else
			{
				m_StaffLevel = from.AccessLevel;
				from.AccessLevel = AccessLevel.Player;
				from.Blessed = false;
			}
		}

		public override bool OnEquip( Mobile from )
		{
			if ( from.AccessLevel < AccessLevel.Counselor )
			{
				from.SendMessage ( "Your not a Staff member, you may not wear this Item..." ); 
				this.Delete();
			}
			return true;
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			// version 2
			writer.Write( (int) 2 );
			writer.Write( m_HomeLocation );
			writer.Write( m_HomeMap );
			writer.Write( m_Owner );
			writer.WriteEncodedInt( (int)m_StaffLevel );	

		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
			switch ( version )
			{

				case 2:
				{
					m_HomeLocation = reader.ReadPoint3D();
					m_HomeMap = reader.ReadMap();
					goto case 1;
				}
				case 1:
				{
					m_Owner = reader.ReadMobile();
					m_StaffLevel = (AccessLevel)reader.ReadEncodedInt();
					break;

				}
			}
		}
	}
}