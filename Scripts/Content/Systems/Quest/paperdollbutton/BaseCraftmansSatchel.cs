using System;
using System.Collections.Generic;
using System.Text;
using Server;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines.MLQuests.Items
{
    public abstract class BaseCraftmansSatchel : Backpack
    {
        protected static readonly Type[] m_TalismanType = new Type[] { typeof(RandomTalisman) };

        public BaseCraftmansSatchel()
        {
            Hue = Utility.RandomBrightHue();
        }

        protected void AddBaseLoot(params Type[][] lootSets)
        {
            Item loot = Loot.Construct(lootSets[Utility.Random(lootSets.Length)]);

            if (loot == null)
                return;

            RewardBag.Enhance(loot);
            DropItem(loot);
        }

        protected void AddRecipe(CraftSystem system)
        {
            // TODO: change craftable artifact recipes to a rarer drop
            int recipeID = system.RandomRecipe();

            if (recipeID != -1)
                DropItem(new RecipeScroll(recipeID));
        }

        public BaseCraftmansSatchel(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}